package de.tu_bs.cs.isf.temporalregulator3000.viewer;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipFile;

import org.eclipse.core.internal.utils.FileUtil;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;
import org.osgi.framework.Bundle;

import de.tu_bs.cs.isf.temporalregulator3000.core.ExtensionPointWriter;
import de.tu_bs.cs.isf.temporalregulator3000.core.ProjectCreator;
import de.tu_bs.cs.isf.temporalregulator3000.core.UnZipper;

public class ViewerProjectCreator extends ProjectCreator {
	
	
	
	private static String SRC_FOLDER_NAME = "src";
	
	

	public ViewerProjectCreator(IProject inputProject) {
		super(inputProject);

		this.srcFolders = new ArrayList<>();
		this.srcFolders.add(SRC_FOLDER_NAME);

		this.requiredBundles = new ArrayList<>();
		this.requiredBundles.add(inputProject.getName() + ".temporal");
		this.requiredBundles.add("org.eclipse.gef");
		this.requiredBundles.add("org.eclipse.ui");
		this.requiredBundles.add("org.eclipse.core.runtime");
		this.requiredBundles.add("org.eclipse.ui.ide");
		this.requiredBundles.add("org.eclipse.core.resources");
		this.requiredBundles.add("de.tu_bs.cs.isf.temporalregulator3000.core");

		String projectName = this.inputProject.getName() + ".temporal.viewer";
		generatedProject = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
	}
	
	
	
	public void createViewerCode(String basePackageName, String fullyQualifiedGatewayName, String fileExtension) throws IOException, CoreException {
		IPackageFragment basePackage = generatedJavaProject.getPackageFragmentRoot(generatedProject.getFolder(SRC_FOLDER_NAME)).createPackageFragment(basePackageName, true, null);

		extractStaticContent(generatedProject, basePackage);
		generatedProject.refreshLocal(IProject.DEPTH_INFINITE, null);
		
		Set<IFile> containedFiles = new HashSet<>();
		collectContainedFiles(containedFiles, (IContainer) basePackage.getCorrespondingResource());
		for(IFile file : containedFiles)
			fixBadPackages(file, basePackage.getElementName(), fullyQualifiedGatewayName);
		
		writeExtensionToPluginXML(basePackageName, fileExtension);
	}

	private void extractStaticContent(IProject project, IPackageFragment basePackage) throws IOException, JavaModelException {
		UnZipper unZipper = new UnZipper();
		Bundle bundle = Platform.getBundle("de.tu_bs.cs.isf.temporalregulator3000.viewer");
		
		{
//				String projectFolder = project.getParent().getLocation() + File.separator + project.getName() + File.separator + srcFolderName + File.separator + basePackage;
			String basePackageFolder = basePackage.getCorrespondingResource().getLocation().toString();
			File unzipDestinationFile = new File(basePackageFolder);
			
			URL url = bundle.getEntry("static_classes.zip");
			URL urlZipLocal = FileLocator.toFileURL(url);
			ZipFile zipFile = new ZipFile(urlZipLocal.getPath());
			
			unZipper.unzip(zipFile, unzipDestinationFile);
			zipFile.close();
		}
		
		{
			String projectFolder = project.getParent().getLocation() + File.separator + project.getName();
			File unzipDestinationFile = new File(projectFolder);
			
			URL url = bundle.getEntry("icons.zip");
			URL urlZipLocal = FileLocator.toFileURL(url);
			ZipFile zipFile = new ZipFile(urlZipLocal.getPath());
			
			unZipper.unzip(zipFile, unzipDestinationFile);
			zipFile.close();
		}
	}



	private void collectContainedFiles(Set<IFile> containedFiles, IContainer container) throws CoreException {
		for(IResource child : container.members()) {
			if(child instanceof IFile)
				containedFiles.add((IFile) child);
			else if(child instanceof IContainer)
				collectContainedFiles(containedFiles, (IContainer) child);
		}
	}
	
	private void fixBadPackages(IFile file, String basePackageName, String fullyQualifiedGatewayName) throws IOException, CoreException {
		String badPackage = "eu.vicci.ecosystem.temporal";
		String badGateway = "eu.vicci.ecosystem.temporal.SftEvolutionGateway";
		String badViewerPlugin = "eu.vicci.ecosystem.sft.temporal.viewer";
		
		String fileEnding = FileUtil.getLineSeparator(file);
		BufferedReader reader = new BufferedReader(new InputStreamReader(file.getContents(true), file.getCharset()));
		String line = reader.readLine();
		
		String content = "";
		while (line != null) {
			if(line.contains(badGateway))
				line = line.replaceAll(badGateway, fullyQualifiedGatewayName);
			else if(line.contains(badPackage))
				line = line.replaceAll(badPackage.replaceAll("\\\\.", "\\\\\\\\."), basePackageName);
			else if(line.contains(badViewerPlugin))
				line = line.replaceAll(badViewerPlugin, generatedProject.getName());
			
			content += line + fileEnding;
			
			line = reader.readLine();
		}
		reader.close();
		
		file.setContents(new ByteArrayInputStream(content.getBytes()), IFile.KEEP_HISTORY, null);
	}
	
	
	
	private void writeExtensionToPluginXML(String basePackageName, String fileExtension) throws CoreException, IOException {
		IFile pluginXmlFile = generatedProject.getFile("plugin.xml");
		if(!pluginXmlFile.exists()) {
			String pluginXmlStub =
					"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + System.lineSeparator() + 
					"<?eclipse version=\"3.4\"?>" + System.lineSeparator() + 
					"<plugin>" + System.lineSeparator() + 
					"</plugin>" + System.lineSeparator() + 
					"";
			
			pluginXmlFile.create(new ByteArrayInputStream(pluginXmlStub.getBytes()), true, null);
		}
		
		String lineEnding = FileUtil.getLineSeparator(pluginXmlFile);
		String extensionToInsert =
				"	<extension" + System.lineSeparator() + 
				"         point=\"org.eclipse.ui.editors\">" + System.lineSeparator() + 
				"      <editor" + System.lineSeparator() + 
				"            class=\"" + basePackageName + ".gef.editor.EvolutionEditorImpl\"" + System.lineSeparator() + 
				"            contributorClass=\"" + basePackageName + ".gef.editor.EditorActionBarContributorImpl\"" + System.lineSeparator() + 
				"            default=\"false\"" + System.lineSeparator() + 
				"            extensions=\"" + fileExtension + "\"" + System.lineSeparator() + 
				"            icon=\"icons/temporal.png\"" + System.lineSeparator() + 
				"            id=\"" + basePackageName + ".viewer\"" + System.lineSeparator() + 
				"            name=\"Temporal Regulator Evolution Viewer\">" + System.lineSeparator() + 
				"      </editor>" + System.lineSeparator() + 
				"   </extension>" + System.lineSeparator() + 
				"   <extension" + System.lineSeparator() + 
				"         point=\"de.tu_bs.cs.isf.temporalregulator3000.core.clocklistener\">" + System.lineSeparator() + 
				"      <clock_listener" + System.lineSeparator() + 
				"            class=\"" + basePackageName + ".gef.util.RegulatorClockListener\">" + System.lineSeparator() + 
				"      </clock_listener>" + System.lineSeparator() + 
				"   </extension>";
		
		ExtensionPointWriter.writeExtension(pluginXmlFile, extensionToInsert);
	}
	
}
