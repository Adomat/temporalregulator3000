package de.tu_bs.cs.isf.temporalregulator3000.api.generation;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.codegen.ecore.genmodel.GenClassifier;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;

import de.tu_bs.cs.isf.temporalregulator3000.api.CodeGenerator;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.util.CounterPartFinder;
import de.tu_bs.cs.isf.temporalregulator3000.core.StringUtil;

public class AdapterElementDiffCodeGenerator extends BaseCodeGenerator {
	
	
	
	private List<GenClassifier> handledGenClasses = new ArrayList<>();

	
	
	public AdapterElementDiffCodeGenerator(IPackageFragment javaPackage, GenPackage temporalGenPackage, GenPackage nonTemporalGenPackage) {
		super(javaPackage, temporalGenPackage, nonTemporalGenPackage);
	}
	
	
	
	public void generateBaseCode() throws JavaModelException {
		String resourceName = CodeGenerator.instance.getNamePrefix() + "AdapterElementDifferenceCalculator";
		
		ICompilationUnit cu = javaPackage.createCompilationUnit(resourceName + ".java", "", true, null);
		CodeGenerator.instance.registerGeneratedSourceFile(cu.getCorrespondingResource());
		
		cu.createPackageDeclaration(javaPackage.getElementName(), null);
		cu.createImport("java.util.List", null, null);
		cu.createImport("java.util.ArrayList", null, null);
		cu.createImport("org.eclipse.emf.ecore.EObject", null, null);
		
		javaClass = cu.createType("public class " + resourceName + " {" + System.lineSeparator() + System.lineSeparator() + "}", null, false, null);
		
		javaClass.createField("private Comparator comparatorInstance;", null, false, null);
		
		javaClass.createType(
				"	public interface Comparator {" + System.lineSeparator() + 
				"		public boolean equal(Object arg1, Object arg2);" + System.lineSeparator() + 
				"	}", null, false, null);
		
		javaClass.createMethod(
				"	public void setComparator(Comparator instance) {" + System.lineSeparator() + 
				"		this.comparatorInstance = instance;" + System.lineSeparator() + 
				"	}", null, false, null);
		
		javaClass.createMethod(
				"	private boolean equal(Object e1, Object e2) {" + System.lineSeparator() + 
				"		return comparatorInstance.equal(e1, e2);" + System.lineSeparator() + 
				"	}", null, false, null);
		
		javaClass.createMethod(
				"	/**" + System.lineSeparator() + 
				"	 * Check which argument is null. returns as follows:<br>" + System.lineSeparator() + 
				"	 * <br>" + System.lineSeparator() + 
				"	 * none is null: 0<br>" + System.lineSeparator() + 
				"	 * only o1 is null: 1<br>" + System.lineSeparator() + 
				"	 * only o2 is null: -1<br>" + System.lineSeparator() + 
				"	 * both are null: 2" + System.lineSeparator() + 
				"	 */" + System.lineSeparator() + 
				"	private int nullcheck(Object o1, Object o2) {" + System.lineSeparator() + 
				"		if(o1 == null)" + System.lineSeparator() + 
				"			return (o2 == null) ? 2 : 1;" + System.lineSeparator() + 
				"		return (o2 == null) ? -1 : 0;" + System.lineSeparator() + 
				"	}", null, false, null);
		
		javaClass.createType(
				"	private class SnapshotNoActionRequiredExcpeption extends Exception {" + System.lineSeparator() + 
				"		private static final long serialVersionUID = 1L;" + System.lineSeparator() + 
				"	}", null, false, null);
		
		javaClass.createMethod(
				"	private <T> T getNewValueForAttributeOrSingleReference(T combinedAdapterObject, T newOriginalObject) throws SnapshotNoActionRequiredExcpeption {" + System.lineSeparator() + 
				"		switch(nullcheck(combinedAdapterObject, newOriginalObject)) {" + System.lineSeparator() + 
				"			case 0:" + System.lineSeparator() + 
				"				// both are set" + System.lineSeparator() + 
				"				if (!equal(combinedAdapterObject, newOriginalObject)) {" + System.lineSeparator() + 
				"					if (combinedAdapterObject instanceof EObject && newOriginalObject instanceof EObject)" + System.lineSeparator() + 
				"						applyDifferences((EObject) combinedAdapterObject, (EObject) newOriginalObject);" + System.lineSeparator() + 
				"					return newOriginalObject;" + System.lineSeparator() + 
				"				}" + System.lineSeparator() + 
				"				break;" + System.lineSeparator() + 
				"			case 1:" + System.lineSeparator() + 
				"				// create" + System.lineSeparator() + 
				"				if(combinedAdapterObject instanceof EObject && newOriginalObject instanceof EObject)" + System.lineSeparator() + 
				"					applyDifferences((EObject) combinedAdapterObject, (EObject) newOriginalObject);" + System.lineSeparator() + 
				"				return newOriginalObject;" + System.lineSeparator() + 
				"			case -1:" + System.lineSeparator() + 
				"				// delete" + System.lineSeparator() + 
				"				return null;" + System.lineSeparator() + 
				"			case 2:" + System.lineSeparator() + 
				"				// both elements are unset, no action needed..." + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		throw new SnapshotNoActionRequiredExcpeption();" + System.lineSeparator() + 
				"	}", null, false, null);
		
		javaClass.createMethod(
				"	private <T> List<T> retrieveNewAndRemoveDeletedEntries(List<T> existingAdapterElements, List<T> newOriginalElements) {" + System.lineSeparator() + 
				"		List<T> matchedElements = new ArrayList<>();" + System.lineSeparator() + 
				"		outer: for (T newOriginalElement : newOriginalElements) {" + System.lineSeparator() + 
				"			for (T existingAdapterElement : existingAdapterElements) {" + System.lineSeparator() + 
				"				if (equal(newOriginalElement, existingAdapterElement)) {" + System.lineSeparator() + 
				"					matchedElements.add(newOriginalElement);" + System.lineSeparator() + 
				"					matchedElements.add(existingAdapterElement);" + System.lineSeparator() + 
				"					" + System.lineSeparator() + 
				"					if(existingAdapterElement instanceof EObject && newOriginalElement instanceof EObject)" + System.lineSeparator() + 
				"						applyDifferences((EObject) existingAdapterElement, (EObject) newOriginalElement);" + System.lineSeparator() + 
				"					" + System.lineSeparator() + 
				"					continue outer;" + System.lineSeparator() + 
				"				}" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		List<T> existingAdapterElementsToDelete = new ArrayList<>();" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		outer: for (T existingAdapterElement : existingAdapterElements) {" + System.lineSeparator() + 
				"			for (T matchedElement : matchedElements) {" + System.lineSeparator() + 
				"				if (equal(matchedElement, existingAdapterElement)) {" + System.lineSeparator() + 
				"					// \"existingAdapter\" could be matched" + System.lineSeparator() + 
				"					continue outer;" + System.lineSeparator() + 
				"				}" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"			// an \"existingAdapter\" was not found in the temporal model" + System.lineSeparator() + 
				"			// Delete!" + System.lineSeparator() + 
				"			existingAdapterElementsToDelete.add(existingAdapterElement);" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		for(T existingAdapterElementToDelete : existingAdapterElementsToDelete) {" + System.lineSeparator() + 
				"			if(existingAdapterElementToDelete instanceof EObject)" + System.lineSeparator() + 
				"				" + CodeGenerator.instance.getQualifiedFacadeName() + ".instance.deleteElement((EObject) existingAdapterElementToDelete);" + System.lineSeparator() + 
				"			else" + System.lineSeparator() + 
				"				existingAdapterElements.remove(existingAdapterElementToDelete);" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		List<T> result = new ArrayList<>();" + System.lineSeparator() + 
				"		outer: for (T newOriginalElement : newOriginalElements) {" + System.lineSeparator() + 
				"			for (T matchedElement : matchedElements) {" + System.lineSeparator() + 
				"				if (equal(matchedElement, newOriginalElement)) {" + System.lineSeparator() + 
				"					// newOriginalElement could be matched" + System.lineSeparator() + 
				"					continue outer;" + System.lineSeparator() + 
				"				}" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"			// a \"newOriginalElement\" was not found in the list of matched entries" + System.lineSeparator() + 
				"			// Create a new entry" + System.lineSeparator() + 
				"			// TODO what about the order !?" + System.lineSeparator() + 
				"			if (newOriginalElement != null)" + System.lineSeparator() + 
				"				result.add(newOriginalElement);" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		return result;" + System.lineSeparator() + 
				"	}", null, false, null);

		this.generateSingletonCode("");
	}



	public void generateDiffingCodeFor(EClass nonTemporalEClass, GenClassifier nonTemporalGenClassifier) throws JavaModelException {
		handledGenClasses.add(nonTemporalGenClassifier);
		
		String qualifiedClassName = nonTemporalGenClassifier.getRawInstanceClassName();

		String contents =
				"	public void applyDifferences(" + qualifiedClassName + " existingConcreteAdapterElement, " + qualifiedClassName + " newConcreteOriginalEntity) {" + System.lineSeparator();
		
		for(EStructuralFeature containedFeature : nonTemporalEClass.getEAllStructuralFeatures()) {
			String featureName = StringUtil.firstLetterToUpper(containedFeature.getName());
			String featureTypeName = CounterPartFinder.getQualifiedNonTemporalTypeName(containedFeature);
			String keyword = featureTypeName.equals("boolean") ? "is" : "get";
			
			if(containedFeature.isMany()) {
				contents +=
						"		{" + System.lineSeparator() + 
						"			List<" + featureTypeName + "> newOriginalEntriesToAdd = retrieveNewAndRemoveDeletedEntries(existingConcreteAdapterElement.get" + featureName + "(), newConcreteOriginalEntity.get" + featureName + "());" + System.lineSeparator() + 
						"			for(" + featureTypeName + " newOriginalEntryToAdd : newOriginalEntriesToAdd)" + System.lineSeparator();
				
				if(containedFeature instanceof EReference)
					contents +=
						"				existingConcreteAdapterElement.get" + featureName + "().add(" + CodeGenerator.instance.getQualifiedAdapterInitiliazerName() + ".instance.createAdapterElementFor(newOriginalEntryToAdd));" + System.lineSeparator();
				else
					contents +=
						"				existingConcreteAdapterElement.get" + featureName + "().add(newOriginalEntryToAdd);" + System.lineSeparator();
					
				contents +=
						"		}" + System.lineSeparator() + 
						"		" + System.lineSeparator();
			}
			else {
				contents +=
						"		try {" + System.lineSeparator() + 
						"			" + featureTypeName + " newValue = getNewValueForAttributeOrSingleReference(existingConcreteAdapterElement." + keyword + featureName + "(), newConcreteOriginalEntity." + keyword + featureName + "());" + System.lineSeparator() + 
						"			existingConcreteAdapterElement.set" + featureName + "(newValue);" + System.lineSeparator() + 
						"		} catch(SnapshotNoActionRequiredExcpeption e) {" + System.lineSeparator();
				
				if(containedFeature instanceof EReference)
					contents += 
						"			applyDifferences(existingConcreteAdapterElement." + keyword + featureName + "(), newConcreteOriginalEntity." + keyword + featureName + "());";
				else
					contents +=
						"			// No action required" + System.lineSeparator();
					
				contents +=
						"		}" + System.lineSeparator() + 
						"		" + System.lineSeparator();
				
			}
		}
		
		contents += "	}";
		
		javaClass.createMethod(contents, null, false, null);
	}
	
	
	
	public void generateRootDiffingCode() throws JavaModelException {
		String contents = "	public void applyDifferences(EObject existingAdapterElement, EObject newOriginalElement) {" + System.lineSeparator();
				
		for(GenClassifier handledGenClass : handledGenClasses) {
			String qualifiedClassName = handledGenClass.getRawInstanceClassName();
			contents += 
					"		if(existingAdapterElement instanceof " + qualifiedClassName + " && newOriginalElement instanceof " + qualifiedClassName + ")" + System.lineSeparator() + 
					"			applyDifferences((" + qualifiedClassName + ") existingAdapterElement, (" + qualifiedClassName + ") newOriginalElement);" + System.lineSeparator() + 
					"		" + System.lineSeparator();
		}
				
		contents += "	}";
		
		javaClass.createMethod(contents, null, false, null);
	}



}





















