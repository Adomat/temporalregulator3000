package de.tu_bs.cs.isf.temporalregulator3000.api.generation;

import java.io.IOException;

import org.eclipse.core.internal.utils.FileUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;

import de.tu_bs.cs.isf.temporalregulator3000.api.CodeGenerator;
import de.tu_bs.cs.isf.temporalregulator3000.core.ExtensionPointWriter;

public class ResourceCodeGenerator extends BaseCodeGenerator {
	
	private String RESOURCE_NAME;
	private String RESORCE_FACTORY_NAME;
	
	
	
	public ResourceCodeGenerator(IPackageFragment javaPackage, GenPackage temporalGenPackage, GenPackage nonTemporalGenPackage) {
		super(javaPackage, temporalGenPackage, nonTemporalGenPackage);
	}



	public void generateResource(GenModel nonTemporalGenModel) throws JavaModelException {
		RESOURCE_NAME = CodeGenerator.instance.getNamePrefix() +  "TemporalResource";
		
		String qualifiedGatewayName = CodeGenerator.instance.getQualifiedGatewayName();
		
		ICompilationUnit cu = javaPackage.createCompilationUnit(RESOURCE_NAME + ".java", "", true, null);
		CodeGenerator.instance.registerGeneratedSourceFile(cu.getCorrespondingResource());
		
		cu.createPackageDeclaration(javaPackage.getElementName(), null);

		cu.createImport("org.eclipse.emf.common.util.URI", null, null);
		cu.createImport("org.eclipse.emf.ecore.EObject", null, null);
		cu.createImport("org.eclipse.emf.ecore.EPackage", null, null);
		cu.createImport("org.eclipse.emf.ecore.EFactory", null, null);
		cu.createImport("org.eclipse.emf.ecore.resource.Resource", null, null);
		cu.createImport("org.eclipse.emf.ecore.xmi.XMLResource", null, null);
		cu.createImport("org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl", null, null);
		cu.createImport("org.eclipse.emf.ecore.util.ExtendedMetaData", null, null);
		cu.createImport("org.eclipse.emf.ecore.util.BasicExtendedMetaData", null, null);
		
		javaClass = cu.createType("public class " + RESOURCE_NAME + " extends org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl {" + System.lineSeparator() + System.lineSeparator() + "}", null, false, null);
		
		javaClass.createMethod(
				"	public " + RESOURCE_NAME + "() {" + System.lineSeparator() + 
				"		super();" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	public " + RESOURCE_NAME + "(org.eclipse.emf.common.util.URI uri) {" + System.lineSeparator() + 
				"		super(uri);" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	@Override" + System.lineSeparator() + 
				"	public void save(java.util.Map<?, ?> options) throws java.io.IOException {" + System.lineSeparator() + 
				"		java.util.List<org.eclipse.emf.ecore.EObject> newContents = new java.util.ArrayList<>(this.contents.size());" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		for (org.eclipse.emf.ecore.EObject potentialAdapterObject : this.contents) {" + System.lineSeparator() + 
				"			org.eclipse.emf.ecore.EObject temporalObject = " + qualifiedGatewayName + ".instance.getTemporalObjectFromCacheMap(potentialAdapterObject);" + System.lineSeparator() + 
				"			if (temporalObject != null) {" + System.lineSeparator() + 
				"				newContents.add(temporalObject);" + System.lineSeparator() + 
				"			} else {" + System.lineSeparator() + 
				"				newContents.add(potentialAdapterObject);" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		this.contents.clear();" + System.lineSeparator() + 
				"		this.contents.addAll(newContents);" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		super.save(options);" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		String loadContent = 
				"	@Override" + System.lineSeparator() + 
				"	public void load(java.util.Map<?, ?> options) throws java.io.IOException {" + System.lineSeparator() + 
				"		try {" + System.lineSeparator() + 
				"			super.load(options);" + System.lineSeparator() + 
				"		} catch(Exception e) {" + System.lineSeparator() + 
				"			// Loading failed, maybe a non-temporalized resource is tried to be loaded, register the old factory and work with this resource in its original notation." + System.lineSeparator() + 
				"			this.unload();" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			{" + System.lineSeparator() + 
				"				// This block does not have any effects unfortunately" + System.lineSeparator() + 
				"				// TODO how do we make the XML files use the original factory?" + System.lineSeparator() + 
				"				Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(\"ecore\", new EcoreResourceFactoryImpl());" + System.lineSeparator() + 
				"				" + System.lineSeparator() + 
				"				final ExtendedMetaData extendedMetaData = new BasicExtendedMetaData(this.resourceSet.getPackageRegistry());" + System.lineSeparator() + 
				"				this.resourceSet.getLoadOptions().put(XMLResource.OPTION_EXTENDED_META_DATA, extendedMetaData);" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"			" + System.lineSeparator();
		
		int count = 0;
		for(GenPackage genPackage : nonTemporalGenModel.getAllGenAndUsedGenPackagesWithClassifiers()) {
			String factoryImplName = genPackage.getQualifiedFactoryClassName();
			String genModelURI = genPackage.eResource().getURI().toString().replace("platform:/resource/", "platform:/plugin/");
			String ecoreModelURI = genModelURI.substring(0, genModelURI.length() - "genmodel".length()) + "ecore";
			
			loadContent +=
				"			Resource modelRes" + count + " = this.resourceSet.getResource(URI.createURI(\"" + ecoreModelURI + "\"), true);" + System.lineSeparator() + 
				"			EPackage ecorePackage" + count + " = this.findPackage(modelRes" + count + ", \"" + genPackage.getNSName() + "\");" + System.lineSeparator() + 
				"			EFactory oldFactory" + count + " = null;" + System.lineSeparator() + 
				"			this.resourceSet.getPackageRegistry().put(ecorePackage" + count + ".getNsURI(), ecorePackage" + count + ");" + System.lineSeparator() + 
				"			oldFactory" + count + " = ecorePackage" + count + ".getEFactoryInstance();" + System.lineSeparator() + 
				"			ecorePackage" + count + ".setEFactoryInstance(new " + factoryImplName + "());" + System.lineSeparator() + 
				"			" + System.lineSeparator();
			
			count++;
		}
		
		loadContent +=
				"			try {" + System.lineSeparator() + 
				"				super.load(options);" + System.lineSeparator() + 
				"			} catch (Exception e2) {" + System.lineSeparator() + 
				"				e2.printStackTrace();" + System.lineSeparator() + 
				"				throw e2;" + System.lineSeparator() + 
				"			} finally {" + System.lineSeparator();

		for(int i=0; i<count; i++)
			loadContent +=
				"				ecorePackage" + i + ".setEFactoryInstance(oldFactory" + i + ");" + System.lineSeparator();
				
		loadContent +=
				"			}" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if (!this.contents.get(0).getClass().getSimpleName().startsWith(\"TemporalAwareAdapterFor\")) {" + System.lineSeparator() + 
				"			org.eclipse.emf.ecore.EObject nonTemporalRoot = " + qualifiedGatewayName + ".instance.getNonTemporalObjectFromCacheMap(this.contents.get(0));" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			if(nonTemporalRoot != null) {" + System.lineSeparator() + 
				"				this.contents.clear();" + System.lineSeparator() + 
				"				this.contents.add(nonTemporalRoot);" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"	}";
		
		javaClass.createMethod(loadContent, null, false, null);
		
		javaClass.createMethod(
				"	private EPackage findPackage(Resource res, String name) {" + System.lineSeparator() + 
				"		for(EObject child : res.getContents()) {" + System.lineSeparator() + 
				"			if(child instanceof EPackage) {" + System.lineSeparator() + 
				"				EPackage result = findPackage((EPackage) child, name);" + System.lineSeparator() + 
				"				" + System.lineSeparator() + 
				"				if(result != null)" + System.lineSeparator() + 
				"					return result;" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		throw new RuntimeException(\"Could not find an EPackage named \\\"\" + name + \"\\\" in resource \\\"\" + res.getURI() + \"\\\".\");" + System.lineSeparator() + 
				"	}", null, false, null);
		
		javaClass.createMethod(
				"	private EPackage findPackage(EPackage pack, String name) {" + System.lineSeparator() + 
				"		if(pack.getName().equals(name))" + System.lineSeparator() + 
				"			return pack;" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		for(EPackage subPack : pack.getESubpackages()) {" + System.lineSeparator() + 
				"			EPackage result = findPackage(subPack, name);" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			if(result != null)" + System.lineSeparator() + 
				"				return result;" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		return null;" + System.lineSeparator() + 
				"	}", null, false, null);
	}



	public void generateResourceFactory(IProject temporalProject, String fileExtension) throws CoreException, IOException {
		RESORCE_FACTORY_NAME = CodeGenerator.instance.getNamePrefix() + "TemporalResourceFactory";
		
		ICompilationUnit cu = javaPackage.createCompilationUnit(RESORCE_FACTORY_NAME + ".java", "", true, null);
		CodeGenerator.instance.registerGeneratedSourceFile(cu.getCorrespondingResource());
		
		cu.createPackageDeclaration(javaPackage.getElementName(), null);
		
		javaClass = cu.createType("public class " + RESORCE_FACTORY_NAME + " extends org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl {" + System.lineSeparator() + System.lineSeparator() + "}", null, false, null);

		javaClass.createMethod(
				"	public " + RESORCE_FACTORY_NAME + "() {" + System.lineSeparator() + 
				"		super();" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	@Override" + System.lineSeparator() + 
				"	public org.eclipse.emf.ecore.resource.Resource createResource(org.eclipse.emf.common.util.URI uri) {" + System.lineSeparator() + 
				"		return new " + RESOURCE_NAME + "(uri);" + System.lineSeparator() + 
				"	}"
				, null, false, null);

		registerFactoryExtensionPoint(temporalProject.getFile("plugin.xml"), javaPackage.getElementName() + "." + RESORCE_FACTORY_NAME, fileExtension);
	}



	private void registerFactoryExtensionPoint(IFile file, String replacingResourceFactoryQualifiedName, String fileExtension) throws CoreException, IOException {
		String lineEnding = FileUtil.getLineSeparator(file);
		String extensionToInsert =
				"   <extension" + lineEnding + 
				"         point=\"org.eclipse.emf.ecore.extension_parser\">" + lineEnding + 
				"      <parser" + lineEnding + 
				"            class=\"" + replacingResourceFactoryQualifiedName + "\"" + lineEnding + 
				"            type=\"" + fileExtension + "\">" + lineEnding + 
				"      </parser>" + lineEnding + 
				"   </extension>" + lineEnding;
		
		ExtensionPointWriter.writeExtension(file, extensionToInsert);
	}
	
}





















