package de.tu_bs.cs.isf.temporalregulator3000.api.generation.util;

import java.util.List;

import org.eclipse.emf.codegen.ecore.genmodel.GenClassifier;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.tu_bs.cs.isf.temporalregulator3000.api.CodeGenerator;

public class CounterPartFinder {
	
	public static String getQualifiedNonTemporalTypeName(EStructuralFeature feature) {
		if(feature instanceof EAttribute) {
			String attributeTypeName = feature.getEType().getInstanceClassName();
			if(attributeTypeName != null)
				return attributeTypeName;
		}
		
		return getQualifiedNonTemporalClassName(feature.getEType());
	}
	
	public static String getQualifiedTemporalTypeName(EStructuralFeature feature) {
		if(feature instanceof EAttribute) {
			String attributeTypeName = feature.getEType().getInstanceClassName();
			if(attributeTypeName != null)
				return attributeTypeName;
		}
		
		return getQualifiedTemporalClassName(feature.getEType());
	}

	
	
	public static String getQualifiedNonTemporalClassName(EClassifier eClassifier) {
		if(eClassifier.getName().equals("EObject"))
			return "java.lang.Object";
		
		return getQualifiedNonTemporalGenClassifier(eClassifier).getRawInstanceClassName();
	}

	public static String getQualifiedTemporalClassName(EClassifier eClassifier) {
		if(eClassifier.getName().equals("EObject"))
			return "java.lang.Object";
		
		return getQualifiedTemporalGenClassifier(eClassifier).getRawInstanceClassName();
	}

	
	
	public static GenClassifier getQualifiedNonTemporalGenClassifier(EClassifier eClassifier) {
		List<GenPackage> allPackages = CodeGenerator.instance.getNonTemporalGenModel().getAllGenUsedAndStaticGenPackagesWithClassifiers();
		return getQualifiedClassName(eClassifier, allPackages, false);
	}

	public static GenClassifier getQualifiedTemporalGenClassifier(EClassifier eClassifier) {
		List<GenPackage> allPackages = CodeGenerator.instance.getTemporalGenModel().getAllGenUsedAndStaticGenPackagesWithClassifiers();
		return getQualifiedClassName(eClassifier, allPackages, true);
	}
	
	

	private static GenClassifier getQualifiedClassName(EClassifier eClassifier, List<GenPackage> allPackages, boolean removeTemporalSuffix) {
		EPackage containingPackage = eClassifier.getEPackage();
		String containingPackageNS = containingPackage.getNsURI();
		
		for(GenPackage pack : allPackages) {
			String packNS = pack.getEcorePackage().getNsURI();
			
			boolean packNamesMatch = false;
			if(packNS.equals(containingPackageNS))
				packNamesMatch = true;
			
			if(removeTemporalSuffix && !packNamesMatch) {
				packNS = packNS.substring(0, packNS.length()-9);
				if(packNS.equals(containingPackageNS))
					packNamesMatch = true;
			}
			
			if(packNamesMatch) {
				// "pack" is the gen model counterpart to "containingPackage"
				for(GenClassifier genClass : pack.getGenClassifiers()) {
					if(genClass.getName().equals(eClassifier.getName())) {
						return genClass;
					}
				}
			}
		}
		
		throw new RuntimeException("Could not find a Temporal Gen Class for: " + eClassifier.getName());
	}
	
}
