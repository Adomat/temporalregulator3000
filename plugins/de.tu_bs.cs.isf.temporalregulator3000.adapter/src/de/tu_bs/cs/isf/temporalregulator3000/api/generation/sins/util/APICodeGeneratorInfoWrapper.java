package de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.util;

public class APICodeGeneratorInfoWrapper {
	
	public String
		
		featureName,
		capitalWrittenFeatureName,
		camelCasedQualifiedFeatureName,
		
		temporalAdapterInterface,
		nonTemporalAdapterInterface,
		
		temporalType,
		nonTemporalType,
		
		obtainingMethodPrefix,
		
		associationReplacementInterfaceName,
		
		temporalFactory,
		
		oppositeName,
		
		defaultValue;
	
	
	
	public boolean
		
		isEnumTyped = false,
		isContainment = false,
		hasOppositeReference = false,
		isOppositeReferenceMultiple = false,
		isTemporalFeature = false;
	
	
	
	public String[] qualifiedNamesOfInheritingTypes;



}
