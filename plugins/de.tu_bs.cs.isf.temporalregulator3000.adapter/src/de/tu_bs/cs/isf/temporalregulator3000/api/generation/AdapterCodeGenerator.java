package de.tu_bs.cs.isf.temporalregulator3000.api.generation;

import static de.tu_bs.cs.isf.temporalregulator3000.core.StringUtil.firstLetterToUpper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.codegen.ecore.genmodel.GenClass;
import org.eclipse.emf.codegen.ecore.genmodel.GenClassifier;
import org.eclipse.emf.codegen.ecore.genmodel.GenFeature;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;

import de.tu_bs.cs.isf.temporalregulator3000.api.CodeGenerator;
import de.tu_bs.cs.isf.temporalregulator3000.api.EClassInheritanceStore;
import de.tu_bs.cs.isf.temporalregulator3000.api.analysis.info.JavaFieldInformation;
import de.tu_bs.cs.isf.temporalregulator3000.api.analysis.info.JavaPackageInformation;
import de.tu_bs.cs.isf.temporalregulator3000.api.analysis.info.JavaTypeInformation;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.util.APICodeGeneratorInfoWrapper;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.util.CounterPartFinder;
import de.tu_bs.cs.isf.temporalregulator3000.core.EcoreUtil;
import de.tu_bs.cs.isf.temporalregulator3000.core.StringUtil;

public class AdapterCodeGenerator extends BaseCodeGenerator {
	
	private Map<Integer, String> featureMap;
	private FacadeCodeGenerator facadeGenerator;
	private EvolutionGatewayCodeGenerator gatewayGenerator;
	
	private String temporalAwareFacadeQualifiedName;
	
	
	public AdapterCodeGenerator(IPackageFragment javaPackage, GenPackage temporalGenPackage, GenPackage nonTemporalGenPackage, FacadeCodeGenerator adapterGenerator, EvolutionGatewayCodeGenerator apiGenerator) {
		super(javaPackage, temporalGenPackage, nonTemporalGenPackage);
		
		this.featureMap = new HashMap<>();
		this.facadeGenerator = adapterGenerator;
		this.gatewayGenerator = apiGenerator;
	}
	
	
	
	public void generateAdapterForClass(GenClassifier temporalGenClassifier, GenClassifier nonTemporalGenClassifier) throws JavaModelException {
		EClassifier nonTemporalEClassifier = nonTemporalGenClassifier.getEcoreClassifier();
		if(!(nonTemporalEClassifier instanceof EClass))
			return;
		EClassifier temporalEClassifier = temporalGenClassifier.getEcoreClassifier();
		if(!(temporalEClassifier instanceof EClass))
			throw new UnsupportedOperationException("Unexpected behaviour: The EClassifier to a temporal GenClassifier was not an instance of EClass.");
		
		String interfaceName = nonTemporalGenClassifier.getName();
		String className = nonTemporalGenClassifier.getName() + "Impl";

		String qualifiedNonTemporalInterfaceName = nonTemporalRootPackage + "." + interfaceName;
		String qualifiedNonTemporalClassName = nonTemporalRootPackage + ".impl." + className;

		String qualifiedTemporalInterfaceName = temporalRootPackage + "." + interfaceName;
//		String qualifiedTemporalClassName = temporalRootPackage + ".impl." + className;
		
		String adapterClassName = "TemporalAwareAdapterFor" + className;
		
		temporalAwareFacadeQualifiedName = CodeGenerator.instance.getQualifiedFacadeName();
		
		ICompilationUnit cu = javaPackage.createCompilationUnit(adapterClassName + ".java", "", true, null);
		CodeGenerator.instance.registerGeneratedSourceFile(cu.getCorrespondingResource());
		
		cu.createPackageDeclaration(javaPackage.getElementName(), null);
		cu.createImport("java.util.Observable", null, null);
		cu.createImport("java.util.Observer", null, null);
		cu.createImport("org.eclipse.emf.common.util.EList", null, null);
		cu.createImport("de.tu_bs.cs.isf.temporalregulator3000.core.RegulatorDecoratorEList", null, null);
		
		javaClass = cu.createType("public class " + adapterClassName + " extends " + qualifiedNonTemporalClassName + " implements Observer {" + System.lineSeparator() + System.lineSeparator() + "}", null, false, null);
		
		for(EStructuralFeature nonTemporalFeature : ((EClass) nonTemporalEClassifier).getEAllStructuralFeatures()) {
			APICodeGeneratorInfoWrapper info = new APICodeGeneratorInfoWrapper();
			info.featureName = getName(nonTemporalFeature);
			for(GenFeature genFeature : ((GenClass) nonTemporalGenClassifier).getAllGenFeatures()) {
				if(genFeature.getName().equals(nonTemporalFeature.getName())) {
					info.capitalWrittenFeatureName = genFeature.getUpperName();
					break;
				}
			}

			info.isTemporalFeature = isTemporalFeature(nonTemporalFeature, ((EClass) temporalEClassifier).getEAllStructuralFeatures());
			
			info.temporalType = CounterPartFinder.getQualifiedTemporalTypeName(nonTemporalFeature);
			info.nonTemporalType = CounterPartFinder.getQualifiedNonTemporalTypeName(nonTemporalFeature);
			
			info.temporalAdapterInterface = qualifiedTemporalInterfaceName;
			info.nonTemporalAdapterInterface = qualifiedNonTemporalInterfaceName;

			info.camelCasedQualifiedFeatureName = EcoreUtil.buildCamelCasedFeatureName(nonTemporalFeature);
			info.associationReplacementInterfaceName = "TemporalAssociation" + info.camelCasedQualifiedFeatureName;
			
			
			if(nonTemporalFeature.getUpperBound() == 1)
				createSingleFeatureMethods(nonTemporalFeature, info);
			else
				createMultipleFeatureMethods(nonTemporalFeature, info);
		}
		
		gatewayGenerator.addInstanceCheckForNonTemporalCacheMapGetter(interfaceName, temporalRootPackage);
		createUpdateMethod();
	}



	private void createSingleFeatureMethods(EStructuralFeature nonTemporalFeature, APICodeGeneratorInfoWrapper info) throws JavaModelException {
		String qualifiedAssociationReplacementInterfaceName = this.temporalRootPackage + "." + info.associationReplacementInterfaceName;
		info.temporalFactory = this.temporalRootPackage + "." + this.temporalGenPackage.getFactoryInterfaceName();
		info.obtainingMethodPrefix = info.nonTemporalType.equals("boolean") ? "is" : "get";
		
		javaClass.createMethod(
				"	@Override" + System.lineSeparator() + 
				"	public " + info.nonTemporalType + " " + info.obtainingMethodPrefix + info.featureName + "() {" + System.lineSeparator() + 
				"		return " + temporalAwareFacadeQualifiedName + ".instance." + info.obtainingMethodPrefix + info.featureName + "(this);" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	@Override" + System.lineSeparator() + 
				"	public void set" + info.featureName + "(" + info.nonTemporalType + " value) {" + System.lineSeparator() + 
				"		" + temporalAwareFacadeQualifiedName + ".instance.set" + info.featureName + "(this, value);" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		facadeGenerator.generateTemporalAwareAdapterGetterCode(info.nonTemporalType, info.obtainingMethodPrefix, info.featureName, info.nonTemporalAdapterInterface);
		facadeGenerator.generateTemporalAwareAdapterUpdateCode(info.nonTemporalType, "set", info.featureName, info.nonTemporalAdapterInterface);
		
		if(info.isTemporalFeature) {
			gatewayGenerator.addImport(qualifiedAssociationReplacementInterfaceName);
			
			if(nonTemporalFeature instanceof EAttribute) {
				info.isEnumTyped = ((EAttribute) nonTemporalFeature).getEAttributeType() instanceof EEnum;
				info.defaultValue = getDefaultValue(nonTemporalFeature, info.capitalWrittenFeatureName);
				
				gatewayGenerator.generateGetSingleAttributeMethodCode(info);
				gatewayGenerator.generateSetSingleAttributeMethodCode(info);
			}
			else if(nonTemporalFeature instanceof EReference) {
				setUpInfoForOpposites(info, (EReference) nonTemporalFeature);
				info.isContainment = ((EReference) nonTemporalFeature).isContainment();
				
				gatewayGenerator.generateGetSingleReferenceMethodCode(info);
				gatewayGenerator.generateSetSingleReferenceMethodCode(info);
			}
		} else {
			gatewayGenerator.generateMethodForGettingSingleOrMultipleNonTemporalFeature(info);
			gatewayGenerator.generateMethodForSettingNonTemporalSingleFeature(info);
		}
	}



	private void createMultipleFeatureMethods(EStructuralFeature nonTemporalFeature, APICodeGeneratorInfoWrapper info) throws JavaModelException {
		String featureID = String.valueOf(nonTemporalFeature.getFeatureID());
		
		info.temporalFactory = this.temporalRootPackage + "." + this.temporalGenPackage.getFactoryInterfaceName();
		
		javaClass.createMethod(
				"	@Override" + System.lineSeparator() + 
				"	public EList<" + info.nonTemporalType + "> get" + info.featureName + "() {" + System.lineSeparator() + 
				"		// the feature ID is obtained from the actual feature, it is conform to the IDs that are set in the package class interface." + System.lineSeparator() + 
				"		int featureID = " + featureID + ";" + System.lineSeparator() + 
				"		RegulatorDecoratorEList<" + info.nonTemporalType + "> decoratorList = new RegulatorDecoratorEList<>(" + temporalAwareFacadeQualifiedName + ".instance.get" + info.featureName + "(this), featureID);" + System.lineSeparator() + 
				"		decoratorList.addObserver(this);" + System.lineSeparator() + 
				"		return decoratorList;" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		featureMap.put(nonTemporalFeature.getFeatureID(), info.featureName);
		facadeGenerator.generateTemporalAwareAdapterGetterCode("List<" + info.nonTemporalType + ">", "get", info.featureName, info.nonTemporalAdapterInterface);
		facadeGenerator.generateTemporalAwareAdapterUpdateCode("List<Object>", "add", info.featureName, info.nonTemporalAdapterInterface);
		facadeGenerator.generateTemporalAwareAdapterUpdateCode("List<Object>", "remove", info.featureName, info.nonTemporalAdapterInterface);
		
		if(info.isTemporalFeature) {
			if(nonTemporalFeature.isOrdered()) {
				// Temporally Sorted List
				gatewayGenerator.generateUtilFieldCodeForOrderedFeature("utilFor" + info.camelCasedQualifiedFeatureName, info.temporalType);
				
				if(nonTemporalFeature instanceof EAttribute) {
					info.isEnumTyped = ((EAttribute) nonTemporalFeature).getEAttributeType() instanceof EEnum;
					
					gatewayGenerator.generateGetMutlipleOrderedAttributeMethodCode(info);
					gatewayGenerator.generateAddMutlipleOrderedAttributeMethodCode(info);
					gatewayGenerator.generateRemoveMutlipleOrderedAttributeMethodCode(info);
				}
				else if(nonTemporalFeature instanceof EReference) {
					setUpInfoForOpposites(info, (EReference) nonTemporalFeature);
					info.isContainment = ((EReference) nonTemporalFeature).isContainment();
					
					gatewayGenerator.generateGetMutlipleOrderedReferenceMethodCode(info);
					gatewayGenerator.generateAddMutlipleOrderedReferenceMethodCode(info);
					gatewayGenerator.generateRemoveMutlipleOrderedReferenceMethodCode(info);
				}
			}
			else {
				// Regular association replacement
				String qualifiedAssociationReplacementInterfaceName = this.temporalRootPackage + "." + info.associationReplacementInterfaceName;
				gatewayGenerator.addImport(qualifiedAssociationReplacementInterfaceName);
				
				if(nonTemporalFeature instanceof EAttribute) {
					info.isEnumTyped = ((EAttribute) nonTemporalFeature).getEAttributeType() instanceof EEnum;
					
					gatewayGenerator.generateGetMutlipleUnorderedAttributeMethodCode(info);
					gatewayGenerator.generateAddMutlipleUnorderedAttributeMethodCode(info);
					gatewayGenerator.generateRemoveMutlipleUnorderedAttributeMethodCode(info);
				}
				else if(nonTemporalFeature instanceof EReference) {
					setUpInfoForOpposites(info, (EReference) nonTemporalFeature);
					info.isContainment = ((EReference) nonTemporalFeature).isContainment();
					
					gatewayGenerator.generateGetMutlipleUnorderedReferenceMethodCode(info);
					gatewayGenerator.generateAddMutlipleUnorderedReferenceMethodCode(info);
					gatewayGenerator.generateRemoveMutlipleUnorderedReferenceMethodCode(info);
				}
			}
		}
		else {
			gatewayGenerator.generateMethodForGettingSingleOrMultipleNonTemporalFeature(info);
			gatewayGenerator.generateMethodForAddingNonTemporalMultipleFeature(info);
			gatewayGenerator.generateMethodForRemovingNonTemporalMultipleFeature(info);
		}
	}



	private void setUpInfoForOpposites(APICodeGeneratorInfoWrapper info, EReference nonTemporalReference) {
		EReference opposite = ((EReference) nonTemporalReference).getEOpposite();
		if(opposite != null) {
			info.hasOppositeReference = true;
			info.oppositeName = StringUtil.firstLetterToUpper(opposite.getName());
			info.isOppositeReferenceMultiple = opposite.getUpperBound() != 1;
			info.qualifiedNamesOfInheritingTypes = EClassInheritanceStore.INSTANCE.getInheritingConcreteEClassesNames(nonTemporalReference.getEReferenceType()).toArray(new String[] {});
		}
	}



	private String getDefaultValue(EStructuralFeature nonTemporalFeature, String capitalWrittenFeatureName) {
		if(nonTemporalFeature.getDefaultValueLiteral() != null) {
			EClassifier type = nonTemporalFeature.getEType();
			if(type instanceof EEnum) {
				String qualifiedNonTemporalTypeName = CounterPartFinder.getQualifiedNonTemporalClassName(type);
				return qualifiedNonTemporalTypeName + "." + nonTemporalFeature.getDefaultValueLiteral();
			}
			else if(type instanceof EDataType) {
				return nonTemporalFeature.getDefaultValueLiteral();
			}
		}
		
		
		GenClassifier genClass = CounterPartFinder.getQualifiedNonTemporalGenClassifier(nonTemporalFeature.getEContainingClass());
		
		String genClassImplClassName = genClass.getImportedInstanceClassName();
		String[] containingEClassNameSegments = genClassImplClassName.split("\\.");
		
		for(JavaPackageInformation rootPackInfo : CodeGenerator.instance.getModelSrcFoldersInfo()) {
			int segmentCount = 0;
			if(rootPackInfo.getName().equals(containingEClassNameSegments[segmentCount])) {
				segmentCount ++;
				
				JavaPackageInformation packInfo = rootPackInfo;
				outer: while(true) {
					List<JavaPackageInformation> subPackInfos = packInfo.getSubpackages();
					for(JavaPackageInformation subPackInfo : subPackInfos) {
						if(subPackInfo.getName().equals(containingEClassNameSegments[segmentCount])) {
							packInfo = subPackInfo;
							segmentCount ++;
							
							if(segmentCount == containingEClassNameSegments.length-1) {
								JavaPackageInformation implPackInfo = packInfo.getSubpackage("impl");
								JavaTypeInformation containingTypeInfo = implPackInfo.getContainedJavaType(containingEClassNameSegments[segmentCount] + "Impl");
								
								JavaFieldInformation fieldInfo = containingTypeInfo.getField(capitalWrittenFeatureName + "_EDEFAULT");
								
								if(fieldInfo != null)
									return fieldInfo.getInitializer();
							}
							
							continue outer;
						}
					}
					break;
				}
			}
		}
		
		throw new RuntimeException("Could not determine the default value for the feature \"" + nonTemporalFeature.getName() + "\"");
	}



	private void createUpdateMethod() throws JavaModelException {
		String updateMethodContents =
				"	@Override" + System.lineSeparator() + 
				"	public void update(Observable list, Object info) {" + System.lineSeparator() + 
				"		if(list instanceof RegulatorDecoratorEList<?> && info instanceof RegulatorDecoratorEList.ChangeWrapper) {" + System.lineSeparator() + 
				"			RegulatorDecoratorEList<?> decoratedList = ((RegulatorDecoratorEList<?>) list);" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			switch (decoratedList.getFeatureID()) {" + System.lineSeparator();
				
		for(int featureID : this.featureMap.keySet()) {
			String featureName = this.featureMap.get(featureID);
			
			updateMethodContents +=
				"					" + System.lineSeparator() + 
				"				case " + featureID + ":" + System.lineSeparator() + 
				"					if(info instanceof RegulatorDecoratorEList.AdditionChangeWrapper) {" + System.lineSeparator() + 
				"						" + temporalAwareFacadeQualifiedName + ".instance.add" + featureName + "(this, ((RegulatorDecoratorEList.AdditionChangeWrapper) info).elements, ((RegulatorDecoratorEList.AdditionChangeWrapper) info).listIndexOfOperation);" + System.lineSeparator() + 
				"					}" + System.lineSeparator() + 
				"					else if(info instanceof RegulatorDecoratorEList.DeletionChangeWrapper) {" + System.lineSeparator() + 
				"						" + temporalAwareFacadeQualifiedName + ".instance.remove" + featureName + "(this, ((RegulatorDecoratorEList.DeletionChangeWrapper) info).elements);" + System.lineSeparator() + 
				"					}" + System.lineSeparator() + 
				"					else {" + System.lineSeparator() + 
				"						throw new UnsupportedOperationException(\"Unhandled Kind of Change Information...\");" + System.lineSeparator() + 
				"					}" + System.lineSeparator() + 
				"					break;" + System.lineSeparator();
		}
		this.featureMap.clear();
				
		updateMethodContents +=
				"					" + System.lineSeparator() + 
				"				default:" + System.lineSeparator() + 
				"					throw new UnsupportedOperationException(\"Unknown Feature ID of Decorated List: \" + decoratedList.getFeatureID());" + System.lineSeparator() +
				"					" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"	}";
		
		javaClass.createMethod(updateMethodContents, null, false, null);
	}



	private boolean isTemporalFeature(EStructuralFeature nonTemporalFeature, EList<EStructuralFeature> temporalFeatures) {
		for(EStructuralFeature temporalFeature : temporalFeatures) {
			String nonTemporalFeatureName = nonTemporalFeature.getName();
			for(int i=0; i<=1; i++) {
				if(nonTemporalFeatureName.equals(temporalFeature.getName()))
					return false;
				
				if(temporalFeature.getName().contains(nonTemporalFeatureName)) {
					if(temporalFeature.getName().replace(nonTemporalFeatureName, "").equals("temporalReplacement"))
						return true;
				}
				
				nonTemporalFeatureName = StringUtil.firstLetterToUpper(nonTemporalFeatureName);
			}
		}
		
		throw new UnsupportedOperationException("Could not find a temporal counter feature to " + nonTemporalFeature);
	}



//	private String getUsedEListQualifiedNameForMultipleFeature(EStructuralFeature feature) {
//		JavaPackageInformation implPackageInfo = this.packageInfo.getSubpackage("impl");
//		JavaTypeInformation generatedNonTemporalClassInfo = implPackageInfo.getContainedJavaType(feature.getEContainingClass().getName() + "Impl");
//		JavaMethodInformation originalGetMethodInfo = generatedNonTemporalClassInfo.getMethod("get" + getName(feature));
//		
//		String interestingStatement = originalGetMethodInfo.getStatements().get(0);
//		String unqualifiedListName = interestingStatement.substring(interestingStatement.indexOf("=new ") + 5);
//		unqualifiedListName = unqualifiedListName.substring(0, unqualifiedListName.indexOf("<"));
//		
//		for(String qualifiedImportEntry : generatedNonTemporalClassInfo.getImports()) {
//			String[] splitQualifiedImportEntry = qualifiedImportEntry.split("\\.");
//			String unqualifiedImportEntry = splitQualifiedImportEntry[splitQualifiedImportEntry.length-1];
//			
//			if(unqualifiedImportEntry.equals(unqualifiedListName))
//				return qualifiedImportEntry;
//		}
//		
//		throw new UnsupportedOperationException("Could not find a qualified list name for the implementation of a given feature:\n\n" + feature.toString());
//	}
	
	
	
	private String getName(EStructuralFeature feature) {
		return firstLetterToUpper(feature.getName());
	}
	
}
















