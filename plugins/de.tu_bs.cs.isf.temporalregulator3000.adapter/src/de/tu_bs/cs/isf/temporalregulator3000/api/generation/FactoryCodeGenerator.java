package de.tu_bs.cs.isf.temporalregulator3000.api.generation;

import java.io.IOException;

import org.eclipse.core.internal.utils.FileUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;

import de.tu_bs.cs.isf.temporalregulator3000.api.CodeGenerator;
import de.tu_bs.cs.isf.temporalregulator3000.core.ExtensionPointWriter;

public class FactoryCodeGenerator extends BaseCodeGenerator {
	
	private String nonTemporalFactoryQualifiedClassName;
	private String temporalFactoryQualifiedClassName;
	
	private String nonTemporalNSURI;
	
	
	
	public FactoryCodeGenerator(IPackageFragment javaPackage, GenPackage temporalGenPackage, GenPackage nonTemporalGenPackage) {
		super(javaPackage, temporalGenPackage, nonTemporalGenPackage);
		
		nonTemporalNSURI = nonTemporalGenPackage.getNSURI();
	}
	
	

	public void generateBaseCode(IProject temporalProject) throws CoreException, IOException {
		nonTemporalFactoryQualifiedClassName = nonTemporalRootPackage + ".impl." + nonTemporalGenPackage.getFactoryClassName();
		temporalFactoryQualifiedClassName = temporalRootPackage + ".impl." + temporalGenPackage.getFactoryClassName();
		
		String factoryClassName = "TemporalAwareAdapterFor" + temporalGenPackage.getFactoryClassName();
		
		ICompilationUnit cu = javaPackage.createCompilationUnit(factoryClassName + ".java", "", true, null);
		CodeGenerator.instance.registerGeneratedSourceFile(cu.getCorrespondingResource());
		
		cu.createPackageDeclaration(javaPackage.getElementName(), null);
		cu.createImport("de.tu_bs.cs.isf.temporalregulator3000.core.ClockMechanism", null, null);
		cu.createImport(CodeGenerator.instance.getQualifiedGatewayName(), null, null);

		javaClass = cu.createType(
				"public class " + factoryClassName + " extends " + nonTemporalFactoryQualifiedClassName + " {" + System.lineSeparator() + System.lineSeparator() + "}"
				, null, false, null);
		
		javaClass.createMethod(
				"public " + factoryClassName + "() {" + System.lineSeparator() + 
				"		super();" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		registerFactoryExtensionPoint(temporalProject.getFile("plugin.xml"), temporalRootPackage + ".impl." + factoryClassName);
	}
	
	
	
	public void generateFactoryCodeForClass(String className, boolean isTemporalClass) throws JavaModelException {
		if(javaClass == null)
			throw new UnsupportedOperationException("You can not write to the Factory class before it was created.");
		
		String nonTemporalClassQualifiedName = nonTemporalRootPackage + "." + className;
		String temporalClassQualifiedName = temporalRootPackage + "." + className;
		String adapterName = "TemporalAwareAdapterFor" + className + "Impl";
		
		String contents =
				"@Override" + System.lineSeparator() + 
				"	public " + nonTemporalClassQualifiedName + " create" + className + "() {" + System.lineSeparator() + 
				"		" + adapterName + " adapterElement = new " + adapterName + "();" + System.lineSeparator() + 
				"		" + temporalClassQualifiedName + " temporalElement = " + temporalFactoryQualifiedClassName + ".eINSTANCE.create" + className + "();" + System.lineSeparator() + 
				(isTemporalClass ? "		temporalElement.setTemporalRegulatorValidSince(ClockMechanism.instance.getTime());" : "") + System.lineSeparator() + 
				(isTemporalClass ? "		temporalElement.createId();" : "") + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		// save a mapping of adapter to non-temporal element" + System.lineSeparator() + 
				"		" + CodeGenerator.instance.getQualifiedGatewayName() + ".instance.addCacheMapEntry(adapterElement, temporalElement);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		// return the adapter" + System.lineSeparator() + 
				"		return adapterElement;" + System.lineSeparator() + 
				"	}";
		
		javaClass.createMethod(contents, null, false, null);
	}



	private void registerFactoryExtensionPoint(IFile file, String qualifiedFactoryName) throws CoreException, IOException {
		String lineEnding = FileUtil.getLineSeparator(file);
		String extensionToInsert =
				"   <extension point=\"org.eclipse.emf.ecore.factory_override\">" + lineEnding + 
				"      <factory" + lineEnding + 
				"            class=\"" + qualifiedFactoryName + "\"" + lineEnding + 
				"            uri=\"" + nonTemporalNSURI + "\">" + lineEnding + 
				"      </factory>" + lineEnding + 
				"   </extension>" + lineEnding;
		
		ExtensionPointWriter.writeExtension(file, extensionToInsert);
	}
	
}
