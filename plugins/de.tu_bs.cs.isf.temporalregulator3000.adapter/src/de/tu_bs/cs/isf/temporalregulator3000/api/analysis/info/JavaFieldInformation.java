package de.tu_bs.cs.isf.temporalregulator3000.api.analysis.info;

import java.util.LinkedList;
import java.util.List;

public class JavaFieldInformation {
	
	private JavaTypeInformation parentType_;
	
	private final String name_;
	private String initializer_;
	
	private List<String> statements_;
	
	
	
	@Override
	public String toString() {
		return "Java Method \"" + this.getName() + "\" inside " + this.parentType_;
	}
	
	
	
	@SuppressWarnings("unused")
	private JavaFieldInformation() {
		this.name_ = "<incorrectly instanciated method>";
	}
	
	public JavaFieldInformation(String name) {
		this.name_ = name;
		this.statements_ = new LinkedList<>();
	}
	
	public String getName() {
		return this.name_;
	}
	
	
	
	public void setInitializer(String init) {
		this.initializer_ = init;
	}
	
	public String getInitializer() {
		return this.initializer_;
	}
	
	

	public void setParentType(JavaTypeInformation parent) {
		if(this.parentType_ == null)
			this.parentType_ = parent;
		else throw new UnsupportedOperationException("Setting the parent type for \"" + this.name_ + "\" to \"" + parent.getQualifiedName() + "\" failed. It was already assigned to the type \"" + this.parentType_.getQualifiedName() + "\".");
	}
	
	public JavaTypeInformation getParentType() {
		return this.parentType_;
	}
	
	
	
	public void addStatement(String statement) {
		this.statements_.add(statement);
	}
	
	public List<String> getStatements() {
		return this.statements_;
	}
	
}
