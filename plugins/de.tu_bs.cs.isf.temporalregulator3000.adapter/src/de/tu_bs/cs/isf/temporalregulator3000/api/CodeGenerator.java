package de.tu_bs.cs.isf.temporalregulator3000.api;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.internal.utils.FileUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.codegen.ecore.genmodel.GenClass;
import org.eclipse.emf.codegen.ecore.genmodel.GenClassifier;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;

import de.tu_bs.cs.isf.temporalregulator3000.api.analysis.SourceFolderAnalyzer;
import de.tu_bs.cs.isf.temporalregulator3000.api.analysis.info.JavaPackageInformation;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.AdapterCodeGenerator;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.AdapterElementDiffCodeGenerator;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.AdapterElementInitializerCodeGenerator;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.EvolutionGatewayCodeGenerator;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.FacadeCodeGenerator;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.FactoryCodeGenerator;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.ResourceCodeGenerator;
import de.tu_bs.cs.isf.temporalregulator3000.core.StringUtil;
import de.tu_bs.cs.isf.temporalregulator3000.core.TemporalRegulatorCodeFormatter;

public class CodeGenerator {
	
	public static CodeGenerator instance = new CodeGenerator();
	
	private CodeGenerator() {
		this.registeredGeneratedResources = new LinkedList<>();
	}
	
	
	
	private List<IResource> registeredGeneratedResources;
	private TemporalRegulatorCodeFormatter formatter;
	
	private List<JavaPackageInformation> commonSrcGenFoldersInfo;
	
	private IProject temporalProject;
	private IJavaProject temporalJavaProject;
//	private IProject nonTemporalProject;
	private GenModel temporalGenModel;
	private GenModel nonTemporalGenModel;
	
	private String qualifiedEvolutionGatewayName, qualifiedFacadeName, qualifiedAdapterInitiliazerName;
	private String namePrefix;
	
	
	
	public void initialize(IProject temporalProject, IJavaProject temporalJavaProject, IProject nonTemporalProject, GenModel temporalGenModel, GenModel nonTemporalGenModel) {
		this.temporalProject = temporalProject;
		this.temporalJavaProject = temporalJavaProject;
//		this.nonTemporalProject = nonTemporalProject;
		this.temporalGenModel = temporalGenModel;
		this.nonTemporalGenModel = nonTemporalGenModel;

		EClassInheritanceStore.INSTANCE.registerGenPackage(this.nonTemporalGenModel.getAllGenPackagesWithClassifiers());
		EClassInheritanceStore.INSTANCE.registerGenPackage(this.nonTemporalGenModel.getAllUsedGenPackagesWithClassifiers());
		
		this.qualifiedEvolutionGatewayName = null;
		this.qualifiedFacadeName = null;
	}
	
	
	
	public void generate() throws CoreException, IOException {
		debug("Starting code generation process");
		
		this.registeredGeneratedResources.clear();

		String[] splitName = temporalProject.getName().split("\\.");
		this.namePrefix = StringUtil.firstLetterToUpper(splitName[splitName.length-2]);
		
		IFolder sourceFolder = temporalProject.getFolder("src-adapter");

		debug("Reading out generated source folder of the non temporal source project.");
		this.commonSrcGenFoldersInfo = analyzeSrcFolders();
		
		debug("Creating the adapter source package.");
		String basePackageName = temporalGenModel.getGenPackages().get(0).getBasePackage();
		IPackageFragment basePackage = temporalJavaProject.getPackageFragmentRoot(sourceFolder).createPackageFragment(basePackageName, true, null);

		debug("Generating Evolution Gateway Base Code.");
		EvolutionGatewayCodeGenerator gatewayGenerator = new EvolutionGatewayCodeGenerator(basePackage, null, null);
		gatewayGenerator.generateBaseCode();
		this.qualifiedEvolutionGatewayName = basePackageName + "." + gatewayGenerator.GATEWAY_NAME;

		debug("Generating Facade Base Code.");
		FacadeCodeGenerator facadeGenerator = new FacadeCodeGenerator(basePackage, null, null);
		facadeGenerator.generateBaseCode();
		this.qualifiedFacadeName = basePackageName + "." + facadeGenerator.FACADE_NAME;

		debug("Generating Custom Resource and Resource Factory.");
		ResourceCodeGenerator resourceGenerator = new ResourceCodeGenerator(basePackage, null, null);
		resourceGenerator.generateResource(nonTemporalGenModel);
		resourceGenerator.generateResourceFactory(temporalProject, nonTemporalGenModel.getGenPackages().get(0).getFileExtension());

		debug("Generating Adapter Element Initializer.");
		AdapterElementInitializerCodeGenerator adapterInitGenerator = new AdapterElementInitializerCodeGenerator(basePackage, null, null);
		adapterInitGenerator.generateBaseCode();
		this.qualifiedAdapterInitiliazerName = basePackageName + "." + adapterInitGenerator.ADAPTER_INITILIAZER_NAME;

		debug("Generating Adapter Element Diffing.");
		AdapterElementDiffCodeGenerator diffingGenerator = new AdapterElementDiffCodeGenerator(basePackage, null, null);
		diffingGenerator.generateBaseCode();
		
		exportPackage(basePackageName);

		for(GenPackage temporalGenPackage : temporalGenModel.getAllGenPackagesWithClassifiers()) {
			debug("  > Generating Code for Ecore package '" + temporalGenPackage.getEcorePackage().getName() + "'.");
			
			GenPackage nonTemporalGenPackage = findNonTemporalCounterPart(temporalGenPackage, nonTemporalGenModel.getGenPackages());
			IPackageFragment implPackage = temporalJavaProject.getPackageFragmentRoot(sourceFolder).createPackageFragment(temporalGenPackage.getBasePackage() + "." + temporalGenPackage.getPackageName() + ".impl", true, null);
			
			FactoryCodeGenerator factoryGenerator = new FactoryCodeGenerator(implPackage, temporalGenPackage, nonTemporalGenPackage);
			factoryGenerator.generateBaseCode(temporalProject);
			
			AdapterCodeGenerator classAdapterGenerator = new AdapterCodeGenerator(implPackage, temporalGenPackage, nonTemporalGenPackage, facadeGenerator, gatewayGenerator);
			
			for(GenClassifier nonTemporalGenClassifier : nonTemporalGenPackage.getGenClassifiers()) {
				if(!(nonTemporalGenClassifier.getEcoreClassifier() instanceof EClass))
					continue;
				
				EClass nonTemporalEClass = (EClass) nonTemporalGenClassifier.getEcoreClassifier();
				adapterInitGenerator.generateAdapterInitCodeFor(nonTemporalEClass, nonTemporalGenClassifier);
				diffingGenerator.generateDiffingCodeFor(nonTemporalEClass, nonTemporalGenClassifier);
				
				if(nonTemporalEClass.isAbstract())
					continue;
				
				debug("    > Generating Code for Ecore class '" + nonTemporalEClass.getName() + "'.");
				
				GenClassifier temporalGenClassifier = findTemporalCounterPart(nonTemporalGenClassifier, temporalGenPackage.getGenClassifiers());
				
				if(temporalGenClassifier instanceof GenClass) {
					classAdapterGenerator.generateAdapterForClass(temporalGenClassifier, nonTemporalGenClassifier);
					factoryGenerator.generateFactoryCodeForClass(nonTemporalGenClassifier.getName(), isTemporalClass(temporalGenClassifier));
				}
			}
		}
		
		diffingGenerator.generateRootDiffingCode();
		adapterInitGenerator.generateAdapterInitCodeForExternalEClasses();
		gatewayGenerator.generateGetNonTemporalObjectFromCacheMapMethod();

		debug("Formatting created resources...");
		formatRegisteredResources();
		debug("Done!");
	}



	private List<JavaPackageInformation> analyzeSrcFolders() {
		List<JavaPackageInformation> info = new LinkedList<>();
		SourceFolderAnalyzer analyzer = new SourceFolderAnalyzer();
		
		Set<GenModel> allGenModels = new HashSet<>();
		for(GenPackage pack : nonTemporalGenModel.getAllGenAndUsedGenPackagesWithClassifiers())
			allGenModels.add(pack.getGenModel());
		
		for(GenModel model : allGenModels) {
			for(String srcFolder : model.getModelSourceFolders()) {
				IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(model.getModelProjectDirectory());
				if(project.exists()) {
					info.addAll(analyzer.analyse(project.getFolder(srcFolder)));
				}
				else {
					// TODO There is a referenced gen package laying somewhere in an installed plugin...
					
//					Bundle thisBundle = FrameworkUtil.getBundle(this.getClass());
//					BundleContext ctx = thisBundle.getBundleContext(); 
//					Bundle[] bundles = ctx.getBundles();
				}
			}
		}
		
		return info;
	}



	private void debug(String message) {
		System.out.println("++ Temporal Regulator Debug ++\t" + message);
	}
	
	
	
	private void exportPackage(String basePackage) throws UnsupportedEncodingException, CoreException {
		// Read current MANIFEST.MF content...
		
		IFile file = this.temporalProject.getFile("META-INF/MANIFEST.MF");
		
		String fileEnding = FileUtil.getLineSeparator(file);
		String content = "";
		
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(file.getContents(true), file.getCharset()));) {
			String line = reader.readLine();
			
			while (line != null) {
				if(line.contains("Export-Package: ")) {
					line = line.substring(0, "Export-Package: ".length()) + basePackage + "," + fileEnding + " " + line.substring("Export-Package: ".length(), line.length());
				}
				
				content += line + fileEnding;
				line = reader.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Write new content...
		// Get bytes using the file's encoding
		byte[] bytes = content.getBytes(file.getCharset());
		InputStream source = new ByteArrayInputStream(bytes);
		
		file.setContents(source, IResource.FORCE, null);
	}



	public void registerGeneratedSourceFile(IResource generatedResource) {
		if(generatedResource == null)
			throw new NullPointerException("Can not register a resource in the CodeGenerator: Null Pointer.");
		
		this.registeredGeneratedResources.add(generatedResource);
	}
	
	public String getNamePrefix() {
		return this.namePrefix;
	}
	
	public String getQualifiedGatewayName() {
		return this.qualifiedEvolutionGatewayName;
	}
	
	public String getQualifiedFacadeName() {
		return this.qualifiedFacadeName;
	}
	
	public String getQualifiedAdapterInitiliazerName() {
		return this.qualifiedAdapterInitiliazerName;
	}
	
	public GenModel getNonTemporalGenModel() {
		return this.nonTemporalGenModel;
	}
	
	public GenModel getTemporalGenModel() {
		return this.temporalGenModel;
	}
	
	public List<JavaPackageInformation> getModelSrcFoldersInfo() {
		return this.commonSrcGenFoldersInfo;
	}



	private Collection<? extends GenPackage> findAllGenPackages(GenPackage pack) {
		List<GenPackage> allPackages = new LinkedList<>();
		allPackages.add(pack);
		
		for(GenPackage subPack : pack.getSubGenPackages()) {
			allPackages.addAll(findAllGenPackages(subPack));
		}
		return allPackages;
	}
	
	

	private GenPackage findNonTemporalCounterPart(GenPackage temporalGenPackage, EList<GenPackage> nonTemporalGenPackages) {
		String fullyQualifiedTemporalGenPackageName = "";
		GenPackage currentTemporalGenPackage = temporalGenPackage;
		while(true) {
			fullyQualifiedTemporalGenPackageName = currentTemporalGenPackage.getPackageName() + "." + fullyQualifiedTemporalGenPackageName;
			
			if(currentTemporalGenPackage.eContainer() instanceof GenPackage)
				currentTemporalGenPackage = (GenPackage) currentTemporalGenPackage.eContainer();
			else break;
		}
		
		GenPackage lastPackageInPath = null;
		qualifiedTemporalPackageLoop: for(String temporalPackageSegment : fullyQualifiedTemporalGenPackageName.split("\\.")) {
			for(GenPackage nonTemporalSubpackage : nonTemporalGenPackages) {
				if(nonTemporalSubpackage.getPackageName().equals(temporalPackageSegment)) {
					lastPackageInPath = nonTemporalSubpackage;
					nonTemporalGenPackages = nonTemporalSubpackage.getNestedGenPackages();
					continue qualifiedTemporalPackageLoop;
				}
			}
			
			throw new UnsupportedOperationException("Could not find a non-temporal Generation Package as counter part to: " + temporalGenPackage.getPackageName());
		}
		
		return lastPackageInPath;
	}
	
	private GenClassifier findTemporalCounterPart(GenClassifier nonTemporalClassifier, EList<GenClassifier> temporalClassifiers) {
		for(GenClassifier temporalClassifier : temporalClassifiers)
			if(nonTemporalClassifier.getName().equals(temporalClassifier.getName()))
				return temporalClassifier;
		
		throw new UnsupportedOperationException("Could not find a temporal Generation Class as counter part to: " + nonTemporalClassifier.getName());
	}
	
	
	
	private boolean isTemporalClass(GenClassifier temporalClassifier) {
		if(temporalClassifier.getEcoreClassifier() instanceof EClass)
			for(EClass superclass : ((EClass) temporalClassifier.getEcoreClassifier()).getEAllSuperTypes())
				if(superclass.getEPackage().getNsURI().equals("https://www.tu-braunschweig.de/isf/TemporalRegulator/1.0"))
					if(superclass.getEPackage().getName().equals("temporalmodel"))
						if(superclass.getName().equals("TemporalElement"))
							return true;
				
		return false;
	}
	
	
	
	private void formatRegisteredResources() throws IOException, CoreException {
		for(IResource registeredResource : this.registeredGeneratedResources) {
			if(registeredResource instanceof IFile) {
				IFile registeredFile = (IFile) registeredResource;
				if(formatter == null)
					formatter = new TemporalRegulatorCodeFormatter();
				formatter.format(registeredFile);
			}
		}
	}
}






















