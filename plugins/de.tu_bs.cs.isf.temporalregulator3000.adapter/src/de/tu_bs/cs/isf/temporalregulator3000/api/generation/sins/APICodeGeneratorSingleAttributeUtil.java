package de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins;

import de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.util.APICodeGeneratorInfoWrapper;

public class APICodeGeneratorSingleAttributeUtil {
	
	public static String createContentsForGetSingleAttributeMethod(APICodeGeneratorInfoWrapper info) {
		String returnCode = "validTemporalAttribute." + info.obtainingMethodPrefix + info.featureName + "()";
		if(info.isEnumTyped) {
			returnCode = info.nonTemporalType + ".get(validTemporalAttribute.get" + info.featureName + "().getValue())";
			
			if(info.defaultValue.split("\\.").length <= 2)
				// No default value was assigned to this Enum attribute...
				// Hence, the default value was derived from the generated source folder of the non temporal project which
				// needs to be refined with a package in the following step
				info.defaultValue = info.defaultValue.replace(info.defaultValue.split("\\.")[0], info.nonTemporalType);
		}
		
		return
				"	public " + info.nonTemporalType + " " + info.obtainingMethodPrefix + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, Date timePoint) {" + System.lineSeparator() + 
				"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
				"			List<" + info.associationReplacementInterfaceName + "> temporalAttributes = ((" + info.temporalAdapterInterface + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
				"			" + info.associationReplacementInterfaceName + " validTemporalAttribute = EvolutionUtil.getValidTemporalElement(temporalAttributes, timePoint);" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			if(validTemporalAttribute != null)" + System.lineSeparator() + 
				"				return " + returnCode + ";" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			return " + info.defaultValue + ";" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
				"	}";
}
	
	
	
	
	
	public static String createContentsForSetSingleAttributeMethod(APICodeGeneratorInfoWrapper info) {
		if(info.isTemporalFeature) {
			String valueCode = "value";
			if(info.isEnumTyped)
				valueCode = info.temporalType + ".get(value.getValue())";
			
			return
				"	public void set" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, " + info.nonTemporalType + " value, Date timePoint) {" + System.lineSeparator() + 
				"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
				"			List<" + info.associationReplacementInterfaceName + "> temporalAttributes = ((" + info.temporalAdapterInterface + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
				"			" + info.associationReplacementInterfaceName + " oldTemporalAttribute = EvolutionUtil.getValidTemporalElement(temporalAttributes, timePoint);" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			" + info.associationReplacementInterfaceName + " newTemporalAttribute = " + info.temporalFactory + ".eINSTANCE.create" + info.associationReplacementInterfaceName + "();" + System.lineSeparator() + 
				"			newTemporalAttribute.set" + info.featureName + "(" + valueCode + ");" + System.lineSeparator() + 
				"			newTemporalAttribute.setTemporalRegulatorValidSince(timePoint);" + System.lineSeparator() + 
				"			temporalAttributes.add(newTemporalAttribute);" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			TemporalUtil.doAdditionalTemporalCorrections(temporalMetaModelElement, temporalAttributes, oldTemporalAttribute, newTemporalAttribute, timePoint);" + System.lineSeparator() + 
				"			return;" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
				"	}";
		}
		else {
			return
					"	public void set" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, " + info.nonTemporalType + " value, Date timePoint) {" + System.lineSeparator() + 
					"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
					"		" + System.lineSeparator() + 
					"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
					"			((" + info.temporalAdapterInterface + ") temporalMetaModelElement).set" + info.featureName + "(value);" + System.lineSeparator() + 
					"			return;" + System.lineSeparator() + 
					"		}" + System.lineSeparator() + 
					"		" + System.lineSeparator() + 
					"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
					"	}";
		}
	}
	
}
