package de.tu_bs.cs.isf.temporalregulator3000.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.emf.codegen.ecore.genmodel.GenClass;
import org.eclipse.emf.codegen.ecore.genmodel.GenClassifier;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;

import de.tu_bs.cs.isf.temporalregulator3000.api.generation.util.CounterPartFinder;

public class EClassInheritanceStore {
	
	public static EClassInheritanceStore INSTANCE = new EClassInheritanceStore();
	
	
	
	private List<GenPackage> nonTemporalGenPackages;
	private Map<GenClassifier, List<GenClassifier>> store; 
	
	
	
	private EClassInheritanceStore() {
		this.store = new HashMap<>();
		this.nonTemporalGenPackages = new ArrayList<>();
	}
	
	
	
	public void registerGenPackage(List<GenPackage> newPackages) {
		this.nonTemporalGenPackages.addAll(newPackages);
	}
	
	
	
	public List<GenClassifier> getInheritingConcreteGenClasses(EClass eClassKey) {
		return getInheritingConcreteGenClasses(CounterPartFinder.getQualifiedNonTemporalGenClassifier(eClassKey));
	}
	
	
	
	public List<GenClassifier> getInheritingConcreteGenClasses(GenClassifier genClassKey) {
		List<GenClassifier> result = this.store.get(genClassKey);
		if(result == null) {
			addEntry(genClassKey);
			result = this.store.get(genClassKey);
		}
		
		return result;
	}
	
	
	
	public List<String> getInheritingConcreteEClassesNames(EClass eClassKey) {
		GenClassifier genClassKey = CounterPartFinder.getQualifiedNonTemporalGenClassifier(eClassKey);
		
		List<GenClassifier> result = this.store.get(genClassKey);
		if(result == null) {
			addEntry(genClassKey);
			result = this.store.get(genClassKey);
		}
		
		List<String> stringResult = new ArrayList<>();
		for(GenClassifier genClassifierResult : result)
			stringResult.add(genClassifierResult.getRawInstanceClassName());
		
		return stringResult;
	}
	
	
	
	private void addEntry(GenClassifier newEntry) {
		boolean insertClassToInheritanceList = false;
		List<GenClassifier> inheritingClasses = new ArrayList<>();
		
		for(GenPackage genPackage : this.nonTemporalGenPackages) {
//			EPackage ePackage = genPackage.getEcorePackage();
//			for(EClassifier eClassifier : ePackage.getEClassifiers()) {
			for(GenClassifier genClassifier : genPackage.getGenClassifiers()) {
				if(genClassifier.getEcoreClassifier() instanceof EClass) {
					if(!((EClass) newEntry.getEcoreClassifier()).isAbstract())
						if(genClassifier.equals(newEntry))
							insertClassToInheritanceList = true;
						
					if(((EClass) genClassifier.getEcoreClassifier()).getEAllSuperTypes().contains(newEntry.getEcoreClassifier())) {
						if(!((EClass) genClassifier.getEcoreClassifier()).isAbstract())
							inheritingClasses.add(genClassifier);
						
						inheritingClasses.addAll(getInheritingConcreteGenClasses(genClassifier));
					}
				}
			}
		}
		
		// Remove doubles
		inheritingClasses = inheritingClasses.stream().distinct().collect(Collectors.toList());
		if(insertClassToInheritanceList)
			inheritingClasses.add(newEntry);
		
		this.store.put(newEntry, inheritingClasses);
	}
	
	
	private String getQualifiedName(GenPackage genPackage, EClassifier eClassifier) {
		String jPackageName = genPackage.getBasePackage() + "." + genPackage.getPackageName();
		String eClassName = eClassifier.getName();
		return jPackageName + "." + eClassName;
	}
	
}
