package de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins;

import de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.util.APICodeGeneratorInfoWrapper;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.util.APICodeGeneratorOppositeHandlingUtil;

public class APICodeGeneratorMultipleOrderedReferenceUtil {
	
	public static String createContentsForGetMultipleOrderedReferenceMethod(APICodeGeneratorInfoWrapper info) {
		String utilInstanceName = "utilFor" + info.camelCasedQualifiedFeatureName;
		
		return
				"	public List<" + info.nonTemporalType + "> get" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, Date timePoint) {" + System.lineSeparator() + 
				"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
				"			TemporallySortedList<" + info.temporalType + "> referenceList = ((" + info.temporalAdapterInterface + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			if(referenceList == null) {" + System.lineSeparator() + 
				"				referenceList = " + utilInstanceName + ".createTemporallySortedList();" + System.lineSeparator() + 
				"				((" + info.temporalAdapterInterface + ") temporalMetaModelElement).setTemporal" + info.featureName + "Replacement(referenceList);" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			List<" + info.temporalType + "> temporalResultList = " + utilInstanceName + ".getSortedElementsAt(referenceList, timePoint);" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			List<" + info.nonTemporalType + "> nonTemporalResultList = new LinkedList<>();" + System.lineSeparator() + 
				"			for(" + info.temporalType + " temporalResultListEntry : temporalResultList)" + System.lineSeparator() + 
				"				nonTemporalResultList.add((" + info.nonTemporalType + ") getNonTemporalObjectFromCacheMap(temporalResultListEntry));" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			return nonTemporalResultList;" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
				"	}";
	}
	
	
	
	public static String createContentsForAddMultipleOrderedReferenceMethod(APICodeGeneratorInfoWrapper info) {
		String utilInstanceName = "utilFor" + info.camelCasedQualifiedFeatureName;
		
		String containmentCode = "";
		if(info.isContainment)
			containmentCode = System.lineSeparator() + 
			"					// Keep up the containment hierarchy" + System.lineSeparator() + 
			"					//if(temporalReferenceToAdd.eContainer() == null)" + System.lineSeparator() + 
			"						((" + info.temporalAdapterInterface + ") temporalMetaModelElement).getRelocatedTemporallySortedListContent" + info.camelCasedQualifiedFeatureName + "().add(temporalReferenceToAdd);" + System.lineSeparator();
		
		String oppositeReferenceMaintainCode = APICodeGeneratorOppositeHandlingUtil.createCodeForSetMultipleReference(info);
		
		String headLine;
		if(!info.hasOppositeReference)
			headLine = "	public void add" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, int listIndexOfOperation, Date timePoint) {";
		else
			headLine = "	private void add" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, int listIndexOfOperation, Date timePoint, boolean maintainOpposite) {";
		
		return
			headLine + System.lineSeparator() + 
			"		Collections.reverse(elements);" + System.lineSeparator() + 
			"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
			"		" + System.lineSeparator() + 
			"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
			"			TemporallySortedList<" + info.temporalType + "> referenceList = ((" + info.temporalAdapterInterface + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
			"			" + System.lineSeparator() + 
			"			if(referenceList == null) {" + System.lineSeparator() + 
			"				referenceList = " + utilInstanceName + ".createTemporallySortedList();" + System.lineSeparator() + 
			"				((" + info.temporalAdapterInterface + ") temporalMetaModelElement).setTemporal" + info.featureName + "Replacement(referenceList);" + System.lineSeparator() + 
			"			}" + System.lineSeparator() + 
			"			" + System.lineSeparator() + 
			"			for(Object commonReferenceToAdd : elements) {" + System.lineSeparator() + 
			"				Assert.isNotNull(commonReferenceToAdd);" + System.lineSeparator() + 
			"				if(commonReferenceToAdd instanceof " + info.nonTemporalType + ") {" + System.lineSeparator() + 
			"					" + info.temporalType + " temporalReferenceToAdd = (" + info.temporalType + ") getTemporalObjectFromCacheMap((EObject) commonReferenceToAdd);" + System.lineSeparator() + 
			"					" + containmentCode + System.lineSeparator() + 
			"					" + System.lineSeparator() + 
			"					" + oppositeReferenceMaintainCode + System.lineSeparator() + 
			"					if(listIndexOfOperation != -1)" + System.lineSeparator() + 
			"						" + utilInstanceName + ".addElementAt(referenceList, (" + info.temporalType + ") temporalReferenceToAdd, listIndexOfOperation, timePoint);" + System.lineSeparator() + 
			"					else" + System.lineSeparator() + 
			"						" + utilInstanceName + ".addElementAt(referenceList, (" + info.temporalType + ") temporalReferenceToAdd, timePoint);" + System.lineSeparator() + 
			"				}" + System.lineSeparator() + 
			"				else throw new RuntimeException(\"Wrong type of reference!\");" + System.lineSeparator() + 
			"			}" + System.lineSeparator() + 
			"		}" + System.lineSeparator() + 
			"	}";
	}
	
	
	
	public static String createContentsForRemoveMultipleOrderedReferenceMethod(APICodeGeneratorInfoWrapper info) {
		String utilInstanceName = "utilFor" + info.camelCasedQualifiedFeatureName;
		
		String oppositeReferenceMaintainCode = APICodeGeneratorOppositeHandlingUtil.createCodeForRemoveMultipleReference(info);
		
		String headLine;
		if(!info.hasOppositeReference)
			headLine = "	public void remove" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, Date timePoint) {";
		else
			headLine = "	private void remove" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, Date timePoint, boolean maintainOpposite) {";
		
		return
			headLine + System.lineSeparator() + 
			"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
			"		" + System.lineSeparator() + 
			"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
			"			TemporallySortedList<" + info.temporalType + "> referenceList = ((" + info.temporalAdapterInterface + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
			"			" + System.lineSeparator() + 
			"			if(referenceList == null) {" + System.lineSeparator() + 
			"				referenceList = " + utilInstanceName + ".createTemporallySortedList();" + System.lineSeparator() + 
			"				((" + info.temporalAdapterInterface + ") temporalMetaModelElement).setTemporal" + info.featureName + "Replacement(referenceList);" + System.lineSeparator() + 
			"			}" + System.lineSeparator() + 
			"			" + System.lineSeparator() + 
			"			for(Object commonReferenceToRemove : elements) {" + System.lineSeparator() + 
			"				Assert.isNotNull(commonReferenceToRemove);" + System.lineSeparator() + 
			"				if(commonReferenceToRemove instanceof " + info.nonTemporalType + ") {" + System.lineSeparator() + 
			"					" + info.temporalType + " temporalReferenceToAdd = (" + info.temporalType + ") getTemporalObjectFromCacheMap((EObject) commonReferenceToRemove);" + System.lineSeparator() + 
			"					" + System.lineSeparator() + 
			"					" + oppositeReferenceMaintainCode + System.lineSeparator() + 
			"					" + utilInstanceName + ".removeElementAt(referenceList, temporalReferenceToAdd, timePoint);" + System.lineSeparator() + 
			"				}" + System.lineSeparator() + 
			"				else throw new RuntimeException(\"Wrong type of reference!\");" + System.lineSeparator() + 
			"			}" + System.lineSeparator() + 
			"		}" + System.lineSeparator() + 
			"	}";
	}
	
}
