package de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.util;

public class APICodeGeneratorOppositeHandlingUtil {
	
	
	
	public static String createCodeForSetSingleReference(APICodeGeneratorInfoWrapper info) {
		String code = "";
		
		if(info.hasOppositeReference) {
			code +=
					"// maintain the opposite reference" + System.lineSeparator() + 
					"		if(maintainOpposite) {" + System.lineSeparator();
			
			if(info.isOppositeReferenceMultiple)
				code +=
						"	List<Object> oppositeList = new LinkedList<>();" + System.lineSeparator() + 
						"	oppositeList.add(adapter);" + System.lineSeparator();
			
			String[] inheritingTypes = info.qualifiedNamesOfInheritingTypes;
			if(inheritingTypes.length == 0)
				inheritingTypes = new String[] { info.nonTemporalType };
			
			boolean inheritingTypeCaseWritten = false;
			for(String inheritingType : inheritingTypes) {
				if(!info.isOppositeReferenceMultiple) {
					code +=
							"	" + (inheritingTypeCaseWritten ? "else " : "") + "if(commonValue instanceof " + inheritingType + ")" + System.lineSeparator() + 
							"		set" + info.oppositeName + "At((" + inheritingType + ") commonValue, adapter, timePoint, false);" + System.lineSeparator();
				} else {
					code +=
							"	" + (inheritingTypeCaseWritten ? "else " : "") + "if(commonValue instanceof " + inheritingType + ")" + System.lineSeparator() + 
							"		add" + info.oppositeName + "At((" + inheritingType + ") commonValue, oppositeList, -1, timePoint, false);" + System.lineSeparator();
				}
				
				inheritingTypeCaseWritten = true;
			}
			
			code += "		}" + System.lineSeparator();
		}
		
		code += System.lineSeparator();
		return code;
	}
	
	
	
	public static String createCodeForSetMultipleReference(APICodeGeneratorInfoWrapper info) {
		String code = "";
		if(info.hasOppositeReference) {
			code +=
					"// set opposite reference" + System.lineSeparator() + 
					"		if(maintainOpposite) {" + System.lineSeparator();
			
			if(info.isOppositeReferenceMultiple)
				code +=
						"		List<Object> oppositeList = new LinkedList<>();" + System.lineSeparator() + 
						"		oppositeList.add(adapter);" + System.lineSeparator();
			
			String[] inheritingTypes = info.qualifiedNamesOfInheritingTypes;
			if(inheritingTypes.length == 0)
				inheritingTypes = new String[] { info.nonTemporalType };
			
			boolean inheritingTypeCaseWritten = false;
			for(String inheritingType : inheritingTypes) {
				if(!info.isOppositeReferenceMultiple) {
					code +=
							"		" + (inheritingTypeCaseWritten ? "else " : "") + "if(commonReferenceToAdd instanceof " + inheritingType + ")" + System.lineSeparator() + 
							"			set" + info.oppositeName + "At((" + inheritingType + ") commonReferenceToAdd, adapter, timePoint, false);" + System.lineSeparator();
				} else {
					code +=
							"		" + (inheritingTypeCaseWritten ? "else " : "") + "if(commonReferenceToAdd instanceof " + inheritingType + ")" + System.lineSeparator() + 
							"			add" + info.oppositeName + "At((" + inheritingType + ") commonReferenceToAdd, oppositeList, timePoint, false);" + System.lineSeparator();
				}
				
				inheritingTypeCaseWritten = true;
			}
			
			code += "		}" + System.lineSeparator();
		}

		code += System.lineSeparator();
		return code;
	}
	
	
	
	public static String createCodeForRemoveMultipleReference(APICodeGeneratorInfoWrapper info) {
		String code = "";
		
		if(info.hasOppositeReference) {
			code +=
					"// remove opposite reference" + System.lineSeparator() + 
					"		if(maintainOpposite) {" + System.lineSeparator();
			
			if(info.isOppositeReferenceMultiple)
				code +=
						"		List<Object> oppositeList = new LinkedList<>();" + System.lineSeparator() + 
						"		oppositeList.add(adapter);" + System.lineSeparator();
			
			String[] inheritingTypes = info.qualifiedNamesOfInheritingTypes;
			if(inheritingTypes.length == 0)
				inheritingTypes = new String[] { info.nonTemporalType };
			
			boolean inheritingTypeCaseWritten = false;
			for(String inheritingType : inheritingTypes) {
				if(!info.isOppositeReferenceMultiple) {
					code +=
							"		" + (inheritingTypeCaseWritten ? "else " : "") + "if(commonReferenceToRemove instanceof " + inheritingType + ")" + System.lineSeparator() + 
							"			set" + info.oppositeName + "At((" + inheritingType + ") commonReferenceToRemove, null, timePoint, false);" + System.lineSeparator();
				} else {
					code +=
							"		" + (inheritingTypeCaseWritten ? "else " : "") + "if(commonReferenceToRemove instanceof " + inheritingType + ")" + System.lineSeparator() + 
							"			remove" + info.oppositeName + "At((" + inheritingType + ") commonReferenceToRemove, oppositeList, timePoint, false);" + System.lineSeparator();
				}
				
				inheritingTypeCaseWritten = true;
			}
			
			code += "		}" + System.lineSeparator();
		}

		code += System.lineSeparator();
		return code;
	}
	
	
	
}
