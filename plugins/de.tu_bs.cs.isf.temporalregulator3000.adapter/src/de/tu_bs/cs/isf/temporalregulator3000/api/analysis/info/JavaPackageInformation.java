package de.tu_bs.cs.isf.temporalregulator3000.api.analysis.info;

import java.util.LinkedList;
import java.util.List;

public class JavaPackageInformation {

	private JavaPackageInformation parentPackage_;
	private List<JavaPackageInformation> subPackages_;
	private List<JavaTypeInformation> containedTypes_;
	
	private final String name_;
	
	
	
	@Override
	public String toString() {
		return "Java Package \"" + this.getQualifiedName() + "\".";
	}
	
	public String printPackageContent(int depth) {
		String result = "P";
		for(int i=0; i<depth; i++)
			result += "--";
		result += " " + this.toString() + "\n";
		
		for(JavaPackageInformation info : this.subPackages_) {
			result += info.printPackageContent(depth+1);
		}
		
		for(JavaTypeInformation info : this.containedTypes_) {
			result += "C";
			for(int i=0; i<depth+1; i++)
				result += "--";
			result += " " + info.toString() + "\n";
			
			for(JavaMethodInformation methodInfo : info.getMethods()) {
				result += "M";
				for(int i=0; i<depth+2; i++)
					result += "--";
				result += " " + methodInfo.toString() + "\n";
			}
		}
		
		return result;
	}
	
	
	
	@SuppressWarnings("unused")
	private JavaPackageInformation() {
		this.name_ = "<incorrectly instanciated package>";
	}
	
	public JavaPackageInformation(String name) {
		this.name_ = name;
		this.subPackages_ = new LinkedList<>();
		this.containedTypes_ = new LinkedList<>();
	}
	
	
	
	// Name
	
	public String getName() {
		return this.name_;
	}
	
	public String getQualifiedName() {
		if(this.parentPackage_ == null )
			return this.name_;
		else
			return this.parentPackage_.getQualifiedName() + "." + this.name_;
	}
	
	
	
	// Type
	
	public void addJavaType(JavaTypeInformation newType) {
		newType.setParentPackage(this);
		this.containedTypes_.add(newType);
	}
	
	public List<JavaTypeInformation> getContainedJavaTypes() {
		return this.containedTypes_;
	}
	
	public JavaTypeInformation getContainedJavaType(String className) {
		for(JavaTypeInformation containedType : this.containedTypes_) {
			if(containedType.getName().equals(className))
				return containedType;
		}
		
		return null;
	}
	
	
	
	// Package
	
	public JavaPackageInformation getParentPackage() {
		return this.parentPackage_;
	}
	
	public List<JavaPackageInformation> getSubpackages() {
		return this.subPackages_;
	}
	
	public JavaPackageInformation getSubpackage(String packageName) {
		for(JavaPackageInformation subPackage : this.subPackages_) {
			if(subPackage.getName().equals(packageName))
				return subPackage;
		}
		
		return null;
	}
	
	public void addSubPackage(JavaPackageInformation newPack) {
		this.subPackages_.add(newPack);
		newPack.setParentPackage(this);
	}

	public void setParentPackage(JavaPackageInformation parent) {
		this.parentPackage_ = parent;
	}

}
