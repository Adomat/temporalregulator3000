package de.tu_bs.cs.isf.temporalregulator3000.slicing;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.zip.ZipFile;

import org.eclipse.core.internal.utils.FileUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.osgi.framework.Bundle;

import de.tu_bs.cs.isf.temporalregulator3000.core.ExtensionPointWriter;
import de.tu_bs.cs.isf.temporalregulator3000.core.ProjectCreator;
import de.tu_bs.cs.isf.temporalregulator3000.core.TemporalRegulatorCodeFormatter;
import de.tu_bs.cs.isf.temporalregulator3000.core.UnZipper;

public class SlicingProjectCreator extends ProjectCreator {
	
	
	
	private static String SRC_FOLDER_NAME = "src";
	
	

	public SlicingProjectCreator(IProject inputProject) {
		super(inputProject);

		this.srcFolders = new ArrayList<>();
		this.srcFolders.add(SRC_FOLDER_NAME);

		this.requiredBundles = new ArrayList<>();
		this.requiredBundles.add(inputProject.getName() + ".temporal");

		String projectName = this.inputProject.getName() + ".temporal.slicing";
		generatedProject = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
	}
	
	
	
	public void createSlicingCode(String basePackageName, String fullyQualifiedGatewayName, String fileExtension) throws IOException, CoreException {
		IPackageFragment basePackage = generatedJavaProject.getPackageFragmentRoot(generatedProject.getFolder(SRC_FOLDER_NAME)).createPackageFragment(basePackageName, true, null);
		TemporalRegulatorCodeFormatter formatter = new TemporalRegulatorCodeFormatter();
		
		formatter.format(createSlicer(basePackage));
		formatter.format(createSliceDialog(basePackage));
		formatter.format(createActionDelegate(basePackage, fullyQualifiedGatewayName));
		
		writeExtensionToPluginXML(basePackageName, fileExtension);
		
		String projectFolder = generatedProject.getParent().getLocation() + File.separator + generatedProject.getName();
		File unzipDestinationFile = new File(projectFolder);

		Bundle bundle = Platform.getBundle("de.tu_bs.cs.isf.temporalregulator3000.slicing");
		URL url = bundle.getEntry("icons.zip");
		URL urlZipLocal = FileLocator.toFileURL(url);
		ZipFile zipFile = new ZipFile(urlZipLocal.getPath());
		
		UnZipper unZipper = new UnZipper();
		unZipper.unzip(zipFile, unzipDestinationFile);
		generatedProject.refreshLocal(IProject.DEPTH_INFINITE, null);
	}



	private IResource createSlicer(IPackageFragment basePackage) throws JavaModelException {
		String resName = "Slicer";
		ICompilationUnit cu = basePackage.createCompilationUnit(resName + ".java", "", true, null);

		cu.createPackageDeclaration(basePackage.getElementName(), null);

		cu.createImport("java.io.IOException", null, null);
		cu.createImport("java.text.SimpleDateFormat", null, null);
		cu.createImport("java.util.Collections", null, null);
		cu.createImport("java.util.Date", null, null);
		cu.createImport("java.util.LinkedList", null, null);
		cu.createImport("java.util.List", null, null);

		cu.createImport("org.eclipse.emf.common.util.URI", null, null);
		cu.createImport("org.eclipse.emf.ecore.EObject", null, null);
		cu.createImport("org.eclipse.emf.ecore.EReference", null, null);
		cu.createImport("org.eclipse.emf.ecore.resource.Resource", null, null);
		cu.createImport("org.eclipse.emf.ecore.resource.ResourceSet", null, null);
		cu.createImport("org.eclipse.emf.ecore.util.EContentsEList", null, null);
		cu.createImport("org.eclipse.emf.ecore.util.EcoreUtil", null, null);

		cu.createImport("de.tu_bs.cs.isf.temporalregulator3000.model.util.DateResolverUtil", null, null);
		cu.createImport("de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporalElement", null, null);
		
		IType javaClass = cu.createType("public class " + resName + " {" + System.lineSeparator() + System.lineSeparator() + "}", null, false, null);
		
		javaClass.createField("	private Resource resUnsplit;", null, false, null);
		javaClass.createField("	private Resource resSplitBefore;", null, false, null);
		javaClass.createField("	private Resource resSplitAfter;", null, false, null);
		javaClass.createField("	private Date sliceDate;", null, false, null);

		javaClass.createMethod(
				"	public Slicer(Resource unsplitResource, Date sliceDate) {" + System.lineSeparator() + 
				"		initResources(unsplitResource, sliceDate);" + System.lineSeparator() + 
				"	}"
				, null, false, null);

		javaClass.createMethod(
				"	private void initResources(Resource unsplitResource, Date sliceDate) {" + System.lineSeparator() + 
				"		this.resUnsplit = unsplitResource;" + System.lineSeparator() + 
				"		URI resUnsplitURI = resUnsplit.getURI();" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		String resUnsplitFileName = resUnsplitURI.lastSegment();" + System.lineSeparator() + 
				"		String resUnsplitFileExtension = resUnsplitURI.fileExtension();" + System.lineSeparator() + 
				"		String resUnsplitFileNameWithoutExtension = resUnsplitFileName.substring(0," + System.lineSeparator() + 
				"				resUnsplitFileName.length() - resUnsplitFileExtension.length() - 1);" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		SimpleDateFormat dateFormat = new SimpleDateFormat(\"yyyy-MM-dd'_'HH-mm-ss\");" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		String resUnsplitURIPath = resUnsplitURI.toString().substring(0," + System.lineSeparator() + 
				"				resUnsplitURI.toString().length() - resUnsplitFileName.length());" + System.lineSeparator() + 
				"		URI resSplitBeforeURI = URI.createURI(resUnsplitURIPath + resUnsplitFileNameWithoutExtension + \"_before_\"" + System.lineSeparator() + 
				"				+ DateResolverUtil.deresolveDate(sliceDate, dateFormat) + \".\" + resUnsplitFileExtension);" + System.lineSeparator() + 
				"		URI resSplitAfterURI = URI.createURI(resUnsplitURIPath + resUnsplitFileNameWithoutExtension + \"_after_\"" + System.lineSeparator() + 
				"				+ DateResolverUtil.deresolveDate(sliceDate, dateFormat) + \".\" + resUnsplitFileExtension);" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		ResourceSet resSet = resUnsplit.getResourceSet();" + System.lineSeparator() + 
				"		this.resSplitBefore = resSet.createResource(resSplitBeforeURI);" + System.lineSeparator() + 
				"		this.resSplitAfter = resSet.createResource(resSplitAfterURI);" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		this.sliceDate = sliceDate;" + System.lineSeparator() + 
				"	}"
				, null, false, null);

		javaClass.createMethod(
				"	public void slice(TemporalElement temporalElement) {\r\n" + 
				"		TemporalElement temporalElementCopy = EcoreUtil.copy(temporalElement);\r\n" + 
				"		TemporalElement temporalElementBefore = EcoreUtil.copy(temporalElement);\r\n" + 
				"		TemporalElement temporalElementAfter = EcoreUtil.copy(temporalElement);\r\n" + 
				"\r\n" + 
				"		sliceElementRecursively(temporalElementCopy, temporalElementBefore, temporalElementAfter);\r\n" + 
				"\r\n" + 
				"		removeInvalidElementsAfterSliceDate(temporalElementBefore);\r\n" + 
				"		removeInvalidElementsBeforeSliceDate(temporalElementAfter);\r\n" + 
				"\r\n" + 
				"		removeDeadReferences(temporalElementBefore, temporalElementBefore);\r\n" + 
				"		removeDeadReferences(temporalElementAfter, temporalElementAfter);\r\n" + 
				"\r\n" + 
				"		sanityCheck(temporalElementBefore);\r\n" + 
				"		sanityCheck(temporalElementAfter);\r\n" + 
				"\r\n" + 
				"		resSplitBefore.getContents().add(temporalElementBefore);\r\n" + 
				"		resSplitAfter.getContents().add(temporalElementAfter);\r\n" + 
				"\r\n" + 
				"		try {\r\n" + 
				"			resSplitBefore.save(Collections.EMPTY_MAP);\r\n" + 
				"			resSplitAfter.save(Collections.EMPTY_MAP);\r\n" + 
				"		} catch (IOException e) {\r\n" + 
				"			e.printStackTrace();\r\n" + 
				"		}\r\n" + 
				"	}"
				, null, false, null);

		javaClass.createMethod(
				"	private void sanityCheck(EObject temporalElement) {\r\n" + 
				"		int lastCompare = 2;\r\n" + 
				"			\r\n" + 
				"		for(EObject e : temporalElement.eContents()) {\r\n" + 
				"			if(e instanceof TemporalElement) {\r\n" + 
				"				Date since = ((TemporalElement) e).getTemporalRegulatorValidSince();\r\n" + 
				"				Date until = ((TemporalElement) e).getTemporalRegulatorValidUntil();\r\n" + 
				"				\r\n" + 
				"				if(since.equals(until))\r\n" + 
				"					System.err.println(\"Something went wrong during a slice. The validity of this element is not valid (since == until)\");\r\n" + 
				"				\r\n" + 
				"				int thisCompare = compareTemporalElementToSliceDate((TemporalElement) e);\r\n" + 
				"				if(lastCompare == 2) {\r\n" + 
				"					if(thisCompare == 0)\r\n" + 
				"						continue;\r\n" + 
				"					lastCompare = thisCompare;\r\n" + 
				"				} else {\r\n" + 
				"					if(thisCompare == 0)\r\n" + 
				"						continue;\r\n" + 
				"					if(thisCompare != lastCompare)\r\n" + 
				"						System.err.println(\"Something went wrong during a slice. The compare numbers differ\");\r\n" + 
				"				}\r\n" + 
				"			}\r\n" + 
				"			\r\n" + 
				"			sanityCheck(e);\r\n" + 
				"		}\r\n" + 
				"	}"
				, null, false, null);

		javaClass.createMethod(
				"	private List<TemporalElement> sliceElementRecursively(EObject element, EObject elementBefore, EObject elementAfter) {\r\n" + 
				"		List<TemporalElement> elementsToRemoveInRecursionStepAbove = new LinkedList<>();\r\n" + 
				"\r\n" + 
				"		if (element instanceof TemporalElement) {\r\n" + 
				"			TemporalElement temporalElement = (TemporalElement) element;\r\n" + 
				"			TemporalElement temporalElementBefore = (TemporalElement) elementBefore;\r\n" + 
				"			TemporalElement temporalElementAfter = (TemporalElement) elementAfter;\r\n" + 
				"			\r\n" + 
				"			int compare = compareTemporalElementToSliceDate(temporalElement);\r\n" + 
				"			\r\n" + 
				"			// change validity of model elements according to slice date\r\n" + 
				"			{\r\n" + 
				"				if (compare == -1) {\r\n" + 
				"					// the element starts and ends before the slice\r\n" + 
				"					elementsToRemoveInRecursionStepAbove.add(temporalElementAfter);\r\n" + 
				"				} else if (compare == +1) {\r\n" + 
				"					// the element starts and ends after the slice\r\n" + 
				"					elementsToRemoveInRecursionStepAbove.add(temporalElementBefore);\r\n" + 
				"				} else if (compare == 0) {\r\n" + 
				"					// the element starts before and ends after the slice\r\n" + 
				"					// --> split it into two elements and add them to respective models\r\n" + 
				"\r\n" + 
				"					Date originalSince = temporalElement.getTemporalRegulatorValidSince();\r\n" + 
				"					Date originalUntil = temporalElement.getTemporalRegulatorValidUntil();\r\n" + 
				"					\r\n" + 
				"					temporalElementBefore.setTemporalRegulatorValidUntil(sliceDate);\r\n" + 
				"					if (originalSince != null)\r\n" + 
				"						if (temporalElementBefore.getTemporalRegulatorValidSince()\r\n" + 
				"								.compareTo(temporalElementBefore.getTemporalRegulatorValidUntil()) >= 0)\r\n" + 
				"							elementsToRemoveInRecursionStepAbove.add(temporalElementBefore);\r\n" + 
				"\r\n" + 
				"					temporalElementAfter.setTemporalRegulatorValidSince(sliceDate);\r\n" + 
				"					if (originalUntil != null)\r\n" + 
				"						if (temporalElementAfter.getTemporalRegulatorValidSince()\r\n" + 
				"								.compareTo(temporalElementAfter.getTemporalRegulatorValidUntil()) >= 0)\r\n" + 
				"							elementsToRemoveInRecursionStepAbove.add(temporalElementAfter);\r\n" + 
				"				} else {\r\n" + 
				"					throw new RuntimeException(\r\n" + 
				"							\"What? An element starts after and ends before the slice date. How is this possible!?\");\r\n" + 
				"				}\r\n" + 
				"			}\r\n" + 
				"		}\r\n" + 
				"		\r\n" + 
				"		List<TemporalElement> elementsToRemoveFromThisRecursionStep = new LinkedList<>();\r\n" + 
				"		for (int i = 0; i < element.eContents().size(); i++) {\r\n" + 
				"			EObject childElement = element.eContents().get(i);\r\n" + 
				"			EObject childElementBefore = elementBefore.eContents().get(i);\r\n" + 
				"			EObject childElementAfter = elementAfter.eContents().get(i);\r\n" + 
				"\r\n" + 
				"			elementsToRemoveFromThisRecursionStep.addAll(sliceElementRecursively(childElement, childElementBefore, childElementAfter));\r\n" + 
				"		}\r\n" + 
				"\r\n" + 
				"//		for (TemporalElement elementToRemove : elementsToRemoveFromThisRecursionStep) {\r\n" + 
				"//			if (elementToRemove.eContainmentFeature() != null)\r\n" + 
				"//				elementAfter.eUnset(elementToRemove.eContainmentFeature());\r\n" + 
				"//			if (elementToRemove.eContainmentFeature() != null)\r\n" + 
				"//				elementBefore.eUnset(elementToRemove.eContainmentFeature());\r\n" + 
				"//			if(elementToRemove.eContainer() != null) {\r\n" + 
				"//				elementAfter.eUnset(elementToRemove.eContainmentFeature());\r\n" + 
				"//			}\r\n" + 
				"//			else\r\n" + 
				"//				System.out.println(\"Was deleted already...\");\r\n" + 
				"//		}\r\n" + 
				"\r\n" + 
				"		return elementsToRemoveInRecursionStepAbove;\r\n" + 
				"	}"
				, null, false, null);

		javaClass.createMethod(
				"	private void removeInvalidElementsAfterSliceDate(EObject temporalElementBefore) {\r\n" + 
				"		List<TemporalElement> elementsToDelete = new LinkedList<>();\r\n" + 
				"		\r\n" + 
				"		for(EObject e : temporalElementBefore.eContents()) {\r\n" + 
				"			if(e instanceof TemporalElement) {\r\n" + 
				"				TemporalElement te = (TemporalElement) e;\r\n" + 
				"				int compare = compareTemporalElementToSliceDate(te);\r\n" + 
				"				if (compare > 0)\r\n" + 
				"					elementsToDelete.add(te);\r\n" + 
				"\r\n" + 
				"				Date since = ((TemporalElement) e).getTemporalRegulatorValidSince();\r\n" + 
				"				Date until = ((TemporalElement) e).getTemporalRegulatorValidUntil();\r\n" + 
				"				if(since.equals(until))\r\n" + 
				"					elementsToDelete.add(te);\r\n" + 
				"			}\r\n" + 
				"		}\r\n" + 
				"		\r\n" + 
				"		for(TemporalElement te : elementsToDelete)\r\n" + 
				"			EcoreUtil.delete(te, true);\r\n" + 
				"\r\n" + 
				"		for(EObject e : temporalElementBefore.eContents())\r\n" + 
				"			removeInvalidElementsAfterSliceDate(e);\r\n" + 
				"	}"
				, null, false, null);

		javaClass.createMethod(
				"	private void removeInvalidElementsBeforeSliceDate(EObject temporalElementAfter) {\r\n" + 
				"		List<TemporalElement> elementsToDelete = new LinkedList<>();\r\n" + 
				"		\r\n" + 
				"		for(EObject e : temporalElementAfter.eContents()) {\r\n" + 
				"			if(e instanceof TemporalElement) {\r\n" + 
				"				TemporalElement te = (TemporalElement) e;\r\n" + 
				"				int compare = compareTemporalElementToSliceDate(te);\r\n" + 
				"				if (compare < 0)\r\n" + 
				"					elementsToDelete.add(te);\r\n" + 
				"\r\n" + 
				"				Date since = ((TemporalElement) e).getTemporalRegulatorValidSince();\r\n" + 
				"				Date until = ((TemporalElement) e).getTemporalRegulatorValidUntil();\r\n" + 
				"				if(since.equals(until))\r\n" + 
				"					elementsToDelete.add(te);\r\n" + 
				"			}\r\n" + 
				"		}\r\n" + 
				"		\r\n" + 
				"		for(TemporalElement te : elementsToDelete)\r\n" + 
				"			EcoreUtil.delete(te, true);\r\n" + 
				"\r\n" + 
				"		for(EObject e : temporalElementAfter.eContents())\r\n" + 
				"			removeInvalidElementsBeforeSliceDate(e);\r\n" + 
				"	}"
				, null, false, null);

		javaClass.createMethod(
				"	/**" + System.lineSeparator() + 
				"	 * Return -1 if the element starts and ends before the slice,<br>" + System.lineSeparator() + 
				"	 * 0 if the element starts before and ends after the slice (overlapping) and<br>" + System.lineSeparator() + 
				"	 * 1 if the element starts and ends after the slice" + System.lineSeparator() + 
				"	 * @param temporalElement" + System.lineSeparator() + 
				"	 * @return the relation of the temporal element with the slice date" + System.lineSeparator() + 
				"	 */" + System.lineSeparator() + 
				"	private int compareTemporalElementToSliceDate(TemporalElement temporalElement) {" + System.lineSeparator() + 
				"		Date since = temporalElement.getTemporalRegulatorValidSince();" + System.lineSeparator() + 
				"		Date until = temporalElement.getTemporalRegulatorValidUntil();" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		boolean startsBeforeSlice;" + System.lineSeparator() + 
				"		boolean endsBeforeSlice;" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		// Set up startsBeforeSlice" + System.lineSeparator() + 
				"		{" + System.lineSeparator() + 
				"			if (since == null) {" + System.lineSeparator() + 
				"				// element is valid since the beginning of time" + System.lineSeparator() + 
				"				// --> sliceDate is after since" + System.lineSeparator() + 
				"				startsBeforeSlice = true;" + System.lineSeparator() + 
				"			} else if (since.compareTo(sliceDate) <= 0) {" + System.lineSeparator() + 
				"				// since is at or before sliceDate" + System.lineSeparator() + 
				"				startsBeforeSlice = true;" + System.lineSeparator() + 
				"			} else {" + System.lineSeparator() + 
				"				// since is after sliceDate" + System.lineSeparator() + 
				"				startsBeforeSlice = false;" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		// Set up endsBeforeSlice" + System.lineSeparator() + 
				"		{" + System.lineSeparator() + 
				"			if (until == null) {" + System.lineSeparator() + 
				"				// element is valid until the end of time" + System.lineSeparator() + 
				"				// --> sliceDate is before until" + System.lineSeparator() + 
				"				endsBeforeSlice = false;" + System.lineSeparator() + 
				"			} else if (sliceDate.compareTo(until) > 0) {" + System.lineSeparator() + 
				"				// sliceDate is after until" + System.lineSeparator() + 
				"				endsBeforeSlice = true;" + System.lineSeparator() + 
				"			} else {" + System.lineSeparator() + 
				"				// sliceDate is at or before until" + System.lineSeparator() + 
				"				endsBeforeSlice = false;" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if (startsBeforeSlice && endsBeforeSlice) {" + System.lineSeparator() + 
				"			// the element starts and ends before the slice" + System.lineSeparator() + 
				"			return -1;" + System.lineSeparator() + 
				"		} else if (!startsBeforeSlice && !endsBeforeSlice) {" + System.lineSeparator() + 
				"			// the element starts and ends after the slice" + System.lineSeparator() + 
				"			return 1;" + System.lineSeparator() + 
				"		} else if (startsBeforeSlice && !endsBeforeSlice) {" + System.lineSeparator() + 
				"			// the element starts before and ends after the slice" + System.lineSeparator() + 
				"			return 0;" + System.lineSeparator() + 
				"		} else {" + System.lineSeparator() + 
				"			throw new RuntimeException(" + System.lineSeparator() + 
				"					\"What? An element starts after and ends before the slice date. How is this possible!?\");" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"	}"
				, null, false, null);

		javaClass.createMethod(
				"	private static void removeDeadReferences(EObject element, EObject startingElement) {" + System.lineSeparator() + 
				"		for (EContentsEList.FeatureIterator<EObject> featureIterator = (EContentsEList.FeatureIterator<EObject>) element" + System.lineSeparator() + 
				"				.eCrossReferences().iterator(); featureIterator.hasNext();) {" + System.lineSeparator() + 
				"			// Always get these two object together!" + System.lineSeparator() + 
				"			EObject referencedObject = (EObject) featureIterator.next();" + System.lineSeparator() + 
				"			EReference reference = (EReference) featureIterator.feature();" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			if (referencedObject.eContainer() != null)" + System.lineSeparator() + 
				"				continue;" + System.lineSeparator() + 
				"			if(referencedObject.equals(startingElement))" + System.lineSeparator() + 
				"				continue;" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			System.out.println(\"Reference to an element with broken containment in: \" + element.getClass().getSimpleName());" + System.lineSeparator() + 
				"			System.out.println(\"\\treference \\\"\" + reference.getName() + \"\\\" of type \" + referencedObject.getClass().getSimpleName());" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			element.eUnset(reference);" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		for(EObject containmentChild : element.eContents())" + System.lineSeparator() + 
				"			removeDeadReferences(containmentChild, startingElement);" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		return cu.getResource();
	}
	
	
	
	private IResource createSliceDialog(IPackageFragment basePackage) throws JavaModelException {
		String resName = "SliceDialog";
		ICompilationUnit cu = basePackage.createCompilationUnit(resName + ".java", "", true, null);

		cu.createPackageDeclaration(basePackage.getElementName(), null);

		cu.createImport("java.util.Date", null, null);
		cu.createImport("java.util.List", null, null);

		cu.createImport("org.eclipse.jface.dialogs.IDialogConstants", null, null);
		cu.createImport("org.eclipse.jface.dialogs.MessageDialog", null, null);
		cu.createImport("org.eclipse.swt.SWT", null, null);
		cu.createImport("org.eclipse.swt.events.SelectionEvent", null, null);
		cu.createImport("org.eclipse.swt.events.SelectionListener", null, null);
		cu.createImport("org.eclipse.swt.graphics.Image", null, null);
		cu.createImport("org.eclipse.swt.widgets.Combo", null, null);
		cu.createImport("org.eclipse.swt.widgets.Composite", null, null);
		cu.createImport("org.eclipse.swt.widgets.Control", null, null);
		cu.createImport("org.eclipse.swt.widgets.Shell", null, null);
		
		IType javaClass = cu.createType("public class " + resName + " extends MessageDialog {" + System.lineSeparator() + System.lineSeparator() + "}", null, false, null);

		javaClass.createField("	private List<Date> allDates;", null, false, null);
		javaClass.createField("	private Date selectedDate;", null, false, null);
		javaClass.createField("	private Combo comboSimple;", null, false, null);
		
		javaClass.createMethod(
				"	public SliceDialog(Shell parentShell, String dialogTitle, Image dialogTitleImage," + System.lineSeparator() + 
				"			String dialogMessage, int dialogImageType, String[] dialogButtonLabels, int defaultIndex) {" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		super(parentShell, dialogTitle, dialogTitleImage, dialogMessage, dialogImageType, dialogButtonLabels, defaultIndex);" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	@Override" + System.lineSeparator() + 
				"	protected Control createContents(Composite parent) {" + System.lineSeparator() + 
				"		Control control = super.createContents(parent);" + System.lineSeparator() + 
				"		getButton(IDialogConstants.OK_ID).setEnabled(false);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		return control;" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	@Override" + System.lineSeparator() + 
				"	protected Control createDialogArea(Composite parent) {" + System.lineSeparator() + 
				"		Composite container = (Composite) super.createDialogArea(parent);" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"//		comboSimple = new Combo(parent, SWT.SIMPLE | SWT.BORDER);" + System.lineSeparator() + 
				"		comboSimple = new Combo(parent, SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		for(Date date : allDates) {" + System.lineSeparator() + 
				"			comboSimple.add(date.toString());" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		comboSimple.addSelectionListener(new SelectionListener() {" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"			@Override" + System.lineSeparator() + 
				"			public void widgetSelected(SelectionEvent e) {" + System.lineSeparator() + 
				"				selectedDate = allDates.get(comboSimple.getSelectionIndex());" + System.lineSeparator() + 
				"				getButton(IDialogConstants.OK_ID).setEnabled(true);" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"			@Override" + System.lineSeparator() + 
				"			public void widgetDefaultSelected(SelectionEvent e) {" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"		});" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		return container;" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	public void setDates(List<Date> allDates) {" + System.lineSeparator() + 
				"		this.allDates = allDates;" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	public Date getSelectedDate() {" + System.lineSeparator() + 
				"		return selectedDate;" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		return cu.getResource();
	}
	
	
	
	private IResource createActionDelegate(IPackageFragment basePackage, String fullyQualifiedGatewayName) throws JavaModelException {
		String resName = "ActionDelegate";
		ICompilationUnit cu = basePackage.createCompilationUnit(resName + ".java", "", true, null);

		cu.createPackageDeclaration(basePackage.getElementName(), null);

		cu.createImport("java.util.ArrayList", null, null);
		cu.createImport("java.util.Collections", null, null);
		cu.createImport("java.util.Date", null, null);
		cu.createImport("java.util.HashSet", null, null);
		cu.createImport("java.util.LinkedList", null, null);
		cu.createImport("java.util.List", null, null);
		cu.createImport("java.util.Set", null, null);
		
		cu.createImport("org.eclipse.core.resources.IFile", null, null);
		cu.createImport("org.eclipse.emf.common.util.URI", null, null);
		cu.createImport("org.eclipse.emf.ecore.EObject", null, null);
		cu.createImport("org.eclipse.emf.ecore.resource.Resource", null, null);
		cu.createImport("org.eclipse.emf.ecore.resource.ResourceSet", null, null);
		cu.createImport("org.eclipse.emf.ecore.resource.impl.ResourceSetImpl", null, null);
		cu.createImport("org.eclipse.jface.action.IAction", null, null);
		cu.createImport("org.eclipse.jface.viewers.ISelection", null, null);
		cu.createImport("org.eclipse.jface.viewers.TreePath", null, null);
		cu.createImport("org.eclipse.jface.viewers.TreeSelection", null, null);
		cu.createImport("org.eclipse.swt.widgets.Shell", null, null);
		cu.createImport("org.eclipse.ui.IObjectActionDelegate", null, null);
		cu.createImport("org.eclipse.ui.IWorkbenchPart", null, null);
		cu.createImport("org.eclipse.ui.internal.ObjectPluginAction", null, null);

		cu.createImport("de.tu_bs.cs.isf.temporalregulator3000.core.DialogUtil", null, null);
		cu.createImport("de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporalElement", null, null);
		
		IType javaClass = cu.createType("public class " + resName + " implements IObjectActionDelegate {" + System.lineSeparator() + System.lineSeparator() + "}", null, false, null);
		
		javaClass.createField("	private Shell shell;", null, false, null);
		
		javaClass.createMethod(
				"	public " + resName + "() {" + System.lineSeparator() + 
				"		super();" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	public void setActivePart(IAction action, IWorkbenchPart targetPart) {" + System.lineSeparator() + 
				"		shell = targetPart.getSite().getShell();" + System.lineSeparator() + 
				"		DialogUtil.initialize(shell);" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	@Override" + System.lineSeparator() + 
				"	public void run(IAction action) {" + System.lineSeparator() + 
				"		if(action instanceof ObjectPluginAction) {" + System.lineSeparator() + 
				"			@SuppressWarnings(\"restriction\")" + System.lineSeparator() + 
				"			ISelection selection = ((ObjectPluginAction) action).getSelection();" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			if(selection instanceof TreeSelection) {" + System.lineSeparator() + 
				"				TreePath treePath = ((TreeSelection) selection).getPaths()[0];" + System.lineSeparator() + 
				"				Object file = treePath.getSegment(treePath.getSegmentCount()-1);" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"				if(file instanceof IFile) {" + System.lineSeparator() + 
				"					handleIFile((IFile) file);" + System.lineSeparator() + 
				"				}" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	private void handleIFile(IFile file) {" + System.lineSeparator() + 
				"		ResourceSet resSet = new ResourceSetImpl();" + System.lineSeparator() + 
				"		URI uri = URI.createURI(\"platform:/resource\" + file.getFullPath());" + System.lineSeparator() + 
				"		Resource res = resSet.getResource(uri, true);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		EObject rootAdapter = res.getContents().get(0);" + System.lineSeparator() + 
				"		EObject rootTemporalElement = " + fullyQualifiedGatewayName + ".instance.getTemporalObjectFromCacheMap(rootAdapter);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if(rootTemporalElement == null) {" + System.lineSeparator() + 
				"			DialogUtil.getInstance().openErrorDialog(\"The model you tried to slice is unknown to the Evolution Gateway.\");" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		else if(rootTemporalElement instanceof TemporalElement) {" + System.lineSeparator() + 
				"			List<TemporalElement> temporalElements = getTemporalElementChildren(rootTemporalElement);" + System.lineSeparator() + 
				"			List<Date> allDates = collectAndSortDates(temporalElements);" + System.lineSeparator() + 
				"			allDates.remove(0);" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			SliceDialog dialog = new SliceDialog(shell, \"Slicing history into chunks\", null, \"Please select a date that shall be used to split the selected model into two chunks\", 0, new String[] { \"Slice\", \"Cancel\" }, 1);" + System.lineSeparator() + 
				"			dialog.setDates(allDates);" + System.lineSeparator() + 
				"			int dialogResult = dialog.open();" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			if(dialogResult == 0) {" + System.lineSeparator() + 
				"				Date sliceDate = dialog.getSelectedDate();" + System.lineSeparator() + 
				"				" + System.lineSeparator() + 
				"				Slicer slicer = new Slicer(rootAdapter.eResource(), sliceDate);" + System.lineSeparator() + 
				"				slicer.slice((TemporalElement) rootTemporalElement);" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		else {" + System.lineSeparator() + 
				"			DialogUtil.getInstance().openErrorDialog(\"The model you tried to slice is not a temporal model.\");" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	private static List<TemporalElement> getTemporalElementChildren(EObject model) {" + System.lineSeparator() + 
				"		List<TemporalElement> result = new LinkedList<>();" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		for(EObject child : model.eContents()) {" + System.lineSeparator() + 
				"			if (child instanceof TemporalElement)" + System.lineSeparator() + 
				"				result.add((TemporalElement) child);" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			result.addAll(getTemporalElementChildren(child));" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		return result;" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	private static List<Date> collectAndSortDates(List<? extends TemporalElement> elements) {" + System.lineSeparator() + 
				"		Set<Date> rawDates = new HashSet<Date>();" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		for (TemporalElement temporalElement : elements) {" + System.lineSeparator() + 
				"			Date since = temporalElement.getTemporalRegulatorValidSince();" + System.lineSeparator() + 
				"			Date until = temporalElement.getTemporalRegulatorValidUntil();" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"			if(since != null)" + System.lineSeparator() + 
				"				rawDates.add(since);" + System.lineSeparator() + 
				"			if(until != null)" + System.lineSeparator() + 
				"				rawDates.add(until);" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		List<Date> dates = new ArrayList<Date>(rawDates);" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		Collections.sort(dates);" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		return dates;" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	@Override" + System.lineSeparator() + 
				"	public void selectionChanged(IAction action, ISelection selection) {" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		return cu.getResource();
	}
	
	
	
	private void writeExtensionToPluginXML(String basePackageName, String fileExtension) throws CoreException, IOException {
		IFile pluginXmlFile = generatedProject.getFile("plugin.xml");
		if(!pluginXmlFile.exists()) {
			String pluginXmlStub =
					"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + System.lineSeparator() + 
					"<?eclipse version=\"3.4\"?>" + System.lineSeparator() + 
					"<plugin>" + System.lineSeparator() + 
					"</plugin>" + System.lineSeparator() + 
					"";
			
			pluginXmlFile.create(new ByteArrayInputStream(pluginXmlStub.getBytes()), true, null);
		}
		
		String lineEnding = FileUtil.getLineSeparator(pluginXmlFile);
		String extensionToInsert =
				"   <extension" + lineEnding + 
				"         point=\"org.eclipse.ui.popupMenus\">" + lineEnding + 
				"      <objectContribution" + lineEnding + 
				"            id=\"" + basePackageName + ".contribution\"" + lineEnding + 
				"            nameFilter=\"*." + fileExtension + "\"" + lineEnding + 
				"            objectClass=\"org.eclipse.core.resources.IFile\">" + lineEnding + 
				"         <action" + lineEnding + 
				"               class=\"" + basePackageName + ".ActionDelegate\"" + lineEnding + 
				"               enablesFor=\"1\"" + lineEnding + 
				"               icon=\"icons/icon.png\"" + lineEnding + 
				"               id=\"" + basePackageName + ".action\"" + lineEnding + 
				"               label=\"Slice history into chunks\">" + lineEnding + 
				"         </action>" + lineEnding + 
				"      </objectContribution>" + lineEnding + 
				"   </extension>" + lineEnding;
		
		ExtensionPointWriter.writeExtension(pluginXmlFile, extensionToInsert);
	}

}
