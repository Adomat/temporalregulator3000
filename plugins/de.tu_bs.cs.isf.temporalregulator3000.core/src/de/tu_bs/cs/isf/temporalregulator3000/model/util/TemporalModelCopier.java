package de.tu_bs.cs.isf.temporalregulator3000.model.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

import de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporalElement;


public class TemporalModelCopier extends Copier {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8170521632121647245L;

	
	@Override
    public EObject copy(EObject eObject) {
		EObject copiedObject = super.copy(eObject);
		
		if(eObject instanceof TemporalElement) {
			TemporalElement copiedElement = (TemporalElement) copiedObject;
			TemporalElement element = (TemporalElement) eObject;
			copiedElement.setTemporalRegulatorID(element.getTemporalRegulatorID());
		}
		
		return copiedObject;
	}
}
