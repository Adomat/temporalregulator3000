package de.tu_bs.cs.isf.temporalregulator3000.core;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import de.tu_bs.cs.isf.temporalregulator3000.model.util.EvolutionUtil;
import de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporalElement;
import de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporallySortedList;
import de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporallySortedListAssociation;
import de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporallySortedListNode;
import de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporalmodelFactory;

public class TemporallySortedListUtil<T> {

	public TemporallySortedList<T> createTemporallySortedList() {
		return TemporalmodelFactory.eINSTANCE.createTemporallySortedList();
	}

	public List<T> getSortedElementsAt(TemporallySortedList<T> temporalList, Date timePoint) {
		List<TemporallySortedListNode<T>> sortedNodes = getSortedNodesAt(temporalList, timePoint);
		
		List<T> result = new LinkedList<>();
		for(TemporallySortedListNode<T> validNode : sortedNodes) {
			if(validNode.getElement() instanceof TemporalElement)
				if(EvolutionUtil.isValid((TemporalElement) validNode.getElement(), timePoint))
					result.add(validNode.getElement());
		}
		return result;
	}

	public void addElementAt(TemporallySortedList<T> temporalList, T element, Date timePoint) {
		addElementAt(temporalList, element, getUnsortedNodesAt(temporalList, timePoint).size(), timePoint);
	}

	public void addElementAt(TemporallySortedList<T> temporalList, T element, int index, Date timePoint) {
		TemporallySortedListNode<T> predecessorNode = getValidNodeAt(temporalList, index-1, timePoint);
		TemporallySortedListNode<T> newNode = TemporalmodelFactory.eINSTANCE.createTemporallySortedListNode();
		newNode.setTemporalRegulatorValidSince(timePoint);
		newNode.setElement(element);
		
		if(predecessorNode != null) {
			TemporallySortedListNode<T> newNodesSuccessor = getSuccessorNodeAt(predecessorNode, timePoint);
			if(newNodesSuccessor != null)
				setSuccessorNodeAt(newNode, newNodesSuccessor, timePoint);

			setSuccessorNodeAt(predecessorNode, newNode, timePoint);
		}
		else {
			setSuccessorNodeAt(newNode, getRootNodeAt(temporalList, timePoint), timePoint);
			setRootNodeAt(temporalList, newNode, timePoint);
		}
		
		temporalList.getAllNodes().add(newNode);
	}

	public void removeElementAt(TemporallySortedList<T> temporalList, T element, Date timePoint) {
		TemporallySortedListNode<T> nodeToRemove = null;
		
		List<TemporallySortedListNode<T>> unsortedValidNodes = getUnsortedNodesAt(temporalList, timePoint);
		for(TemporallySortedListNode<T> validNode : unsortedValidNodes)
			if(validNode.getElement().equals(element))
				nodeToRemove = validNode;
		
		if(nodeToRemove != null) {
			TemporallySortedListNode<T> successor = getSuccessorNodeAt(nodeToRemove, timePoint);
			TemporallySortedListNode<T> predecessor = getPredecessorNodeAt(temporalList, nodeToRemove, timePoint);
			
			if(predecessor != null)
				setSuccessorNodeAt(predecessor, successor, timePoint);
			
			nodeToRemove.setTemporalRegulatorValidUntil(timePoint);
			if(nodeToRemove.getTemporalRegulatorValidSince().equals(nodeToRemove.getTemporalRegulatorValidUntil()))
				temporalList.getAllNodes().remove(nodeToRemove);
			return;
		}
		
		throw new ArrayIndexOutOfBoundsException("Node to remove was not found.");
	}
	
	
	
	// ===== Helping Methods ===== //
	
	
	
	private List<TemporallySortedListNode<T>> getSortedNodesAt(TemporallySortedList<T> temporalList, Date timePoint) {
		List<TemporallySortedListNode<T>> sortedNodeList = new LinkedList<>();
		
		TemporallySortedListNode<T> currentNode = getRootNodeAt(temporalList, timePoint);
		while(currentNode != null) {
			sortedNodeList.add(currentNode);
			currentNode = getSuccessorNodeAt(currentNode, timePoint);
		}
		
		return sortedNodeList;
	}

	private List<TemporallySortedListNode<T>> getUnsortedNodesAt(TemporallySortedList<T> temporalList, Date timePoint) {
		List<TemporallySortedListNode<T>> allNodes = temporalList.getAllNodes();
		return EvolutionUtil.getValidTemporalElements(allNodes, timePoint);
	}
	
	private TemporallySortedListNode<T> getValidNodeAt(TemporallySortedList<T> temporalList, int index, Date timePoint) {
		List<TemporallySortedListNode<T>> sortedNodeList = getSortedNodesAt(temporalList, timePoint);
		
		if(index >= 0 && index < sortedNodeList.size())
			return sortedNodeList.get(index);
		else return null;
	}
	
	private TemporallySortedListNode<T> getPredecessorNodeAt(TemporallySortedList<T> temporalList, TemporallySortedListNode<T> nodeToRemove, Date timePoint) {
		TemporallySortedListNode<T> predecessor = getRootNodeAt(temporalList, timePoint);
		
		while(true) {
			TemporallySortedListNode<T> nextPredecessor = getSuccessorNodeAt(predecessor, timePoint);
			
			if(nextPredecessor == null) {
				// end of the list, the algorithm did not find a node which
				// has the searched node as successor
				predecessor = null;
				break;
			} else if(nextPredecessor.equals(nodeToRemove)) {
				// Iterate through the list until you find a node which
				// has the searched node as successor -> predecessor found!
				break;
			}
			predecessor = nextPredecessor;
		}
		
		return predecessor;
	}
	
	
	
	// ===== Root Node ===== //

	private TemporallySortedListNode<T> getRootNodeAt(TemporallySortedList<T> temporalList, Date timePoint) {
		TemporallySortedListAssociation<T> rootAssociation = EvolutionUtil.getValidTemporalElement(temporalList.getRoot(), timePoint);
		
		if (rootAssociation == null)
			return null;
		else
			return (TemporallySortedListNode<T>) rootAssociation.getTarget();
	}
	
	private void setRootNodeAt(TemporallySortedList<T> temporalList, TemporallySortedListNode<T> newNode, Date timePoint) {
		TemporallySortedListAssociation<T> oldRootAssociation = EvolutionUtil.getValidTemporalElement(temporalList.getRoot(), timePoint);
		if(oldRootAssociation != null) {
			oldRootAssociation.setTemporalRegulatorValidUntil(timePoint);
			if(oldRootAssociation.getTemporalRegulatorValidSince().equals(oldRootAssociation.getTemporalRegulatorValidUntil()))
				temporalList.getRoot().remove(oldRootAssociation);
		}
		
		TemporallySortedListAssociation<T> newRootAssociation = TemporalmodelFactory.eINSTANCE.createTemporallySortedListAssociation();
		newRootAssociation.setTemporalRegulatorValidSince(timePoint);
		newRootAssociation.setTarget(newNode);
		temporalList.getRoot().add(newRootAssociation);
	}
	
	
	
	// ===== Successor Association ===== //
	
	private TemporallySortedListNode<T> getSuccessorNodeAt(TemporallySortedListNode<T> node, Date timePoint) {
		TemporallySortedListAssociation<T> successorAssociation = EvolutionUtil.getValidTemporalElement(node.getSuccessor(), timePoint);
		if(successorAssociation != null)
			return successorAssociation.getTarget();
		else
			return null;
	}
	
	private void setSuccessorNodeAt(TemporallySortedListNode<T> node, TemporallySortedListNode<T> newSuccessor, Date timePoint) {
		TemporallySortedListAssociation<T> oldSuccessorAssociation = EvolutionUtil.getValidTemporalElement(node.getSuccessor(), timePoint);
		
		if(newSuccessor == null) {
			manageLifetimeOfSuccessorAssociation(node, oldSuccessorAssociation, timePoint);
			return;
		}
		
		if(newSuccessor.getElement() == null)
			throw new NullPointerException("The given successor element is null.");

		TemporallySortedListAssociation<T> newSuccessorAssociation = TemporalmodelFactory.eINSTANCE.createTemporallySortedListAssociation();
		newSuccessorAssociation.setTemporalRegulatorValidSince(timePoint);
		newSuccessorAssociation.setTarget(newSuccessor);
		node.getSuccessor().add(newSuccessorAssociation);
		
		if(oldSuccessorAssociation != null) {
			newSuccessorAssociation.setTemporalRegulatorValidUntil(oldSuccessorAssociation.getTemporalRegulatorValidUntil());
			manageLifetimeOfSuccessorAssociation(node, oldSuccessorAssociation, timePoint);
		}
	}

	private void manageLifetimeOfSuccessorAssociation(TemporallySortedListNode<T> node, TemporallySortedListAssociation<T> association, Date timePoint) {
		if(association != null) {
			association.setTemporalRegulatorValidUntil(timePoint);
			if(association.getTemporalRegulatorValidSince().equals(association.getTemporalRegulatorValidUntil()))
				node.getSuccessor().remove(association);
		}
	}

}





































