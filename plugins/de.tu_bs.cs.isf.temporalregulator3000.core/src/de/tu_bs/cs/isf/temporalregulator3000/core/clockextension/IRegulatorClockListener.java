package de.tu_bs.cs.isf.temporalregulator3000.core.clockextension;

import java.util.Date;

public interface IRegulatorClockListener {

	/**
	 * Handle the time change of the <strong>default clock</strong> (key: null) or a <strong>specific clock</strong> (key: some string)
	 * @param newTime
	 */
	public void handleTimeChange(String key, Date newTime);
	
}
