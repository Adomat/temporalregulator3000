package de.tu_bs.cs.isf.temporalregulator3000.core;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

public class DefaultClockGUIHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);

//		DefaultClockGUIWindow dialog = new DefaultClockGUIWindow(window.getShell());
		DateDialog dialog = new DateDialog(window.getShell(), ClockMechanism.instance.getTime());
		dialog.open();
		
		if(dialog.getValue() != null)
			ClockMechanism.instance.setTime(dialog.getValue());
		
		return null;
	}
}



//class DefaultClockGUIWindow extends InputDialog {
//	
//	private static SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
//
//	public DefaultClockGUIWindow(Shell parentShell) {
//		super(parentShell, "Temporal Regulator Default Clock Input", "Current time: " + ClockMechanism.instance.getTime() + System.lineSeparator() + "Set a new time below", formatter.format(ClockMechanism.instance.getTime()), new IInputValidator() {
//			@Override
//			public String isValid(String newText) {
//				try {
//					Date newDate = formatter.parse(newText);
//					ClockMechanism.instance.setTime(newDate);
//					return null;
//				} catch (ParseException e) {
//					return "Invalid date format, please use a date according to " + formatter.toPattern();
//				}
//			}
//		});
//	}
//	
//}