package de.tu_bs.cs.isf.temporalregulator3000.core;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.core.internal.utils.FileUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;

public class ExtensionPointWriter {
	
	public static void writeExtension(IFile pluginXmlFile, String extensionToInsert) throws CoreException, IOException {
		String fileEnding = FileUtil.getLineSeparator(pluginXmlFile);
		BufferedReader reader = new BufferedReader(new InputStreamReader(pluginXmlFile.getContents(true), pluginXmlFile.getCharset()));
		String line = reader.readLine();
		
		String content = "";
		while (line != null) {
			content += line + fileEnding;
			if(line.trim().equals("<plugin>"))
				content+= extensionToInsert;
			
			line = reader.readLine();
		}
		reader.close();
		
		pluginXmlFile.setContents(new ByteArrayInputStream(content.getBytes()), IFile.KEEP_HISTORY, null);
	}

}
