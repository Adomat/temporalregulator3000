package de.tu_bs.cs.isf.temporalregulator3000.core;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class DialogUtil {
	private static DialogUtil instance;
	
	public static void initialize(Shell shell) {
		instance = new DialogUtil(shell);
	}
	
	public static DialogUtil getInstance() {
		if(instance == null)
			throw new UnsupportedOperationException("Singleton DialogUtil was not initialized.");
		
		return instance;
	}
	
	// non-static functionality
	
	private Shell shell;
	
	private DialogUtil(Shell shell) {
		this.shell = shell;
	}
	private DialogUtil() {}
	
	
	
	public String openInputDialog(String title, String message, String initialValue) {
		InputDialog dialog = new InputDialog(shell, title, message, initialValue, null) {
			@Override
			protected Control createDialogArea(Composite parent) {
				Composite composite = (Composite) super.createDialogArea(parent);

				GridData testLayoutData = (GridData) this.getText().getLayoutData();
				testLayoutData.heightHint = 200;
				return composite;
			}
		};
		if(dialog.open() == 0)
			return dialog.getValue();
		else return null;
	}
	
	public int openErrorDialog(String message) {
		return openDialog(message, MessageDialog.ERROR, new String[] { "Oh no..." });
	}

	public int openWarningDialog(String message) {
		return openDialog(message, MessageDialog.WARNING, new String[] { "Alright" });
	}

	public int openInfoDialog(String message) {
		return openDialog(message, MessageDialog.CONFIRM, new String[] { "Okay" });
	}

	public int openQuestionDialog(String message, String[] buttons) {
		return openDialog(message, MessageDialog.QUESTION, buttons);
	}

	private int openDialog(String message, int messageType, String[] buttons) {
		MessageDialog dialog = new MessageDialog(shell, "Temporal Model Converter", null, message, messageType, buttons, 0);
		return dialog.open();
	}
}
