package de.tu_bs.cs.isf.temporalregulator3000.core;

public class StringUtil {
	
	public static String firstLetterToLower(String eName) {
		return ("" + eName.charAt(0)).toLowerCase() + eName.substring(1);
	}

	public static String firstLetterToUpper(String eName) {
		return ("" + eName.charAt(0)).toUpperCase() + eName.substring(1);
	}
	
}
