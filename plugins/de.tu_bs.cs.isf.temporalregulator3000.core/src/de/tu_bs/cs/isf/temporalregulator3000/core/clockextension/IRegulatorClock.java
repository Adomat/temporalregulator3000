package de.tu_bs.cs.isf.temporalregulator3000.core.clockextension;

import java.util.Date;

public interface IRegulatorClock {
	
	public Date getTime(String key);
	
}
