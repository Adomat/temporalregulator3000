package de.tu_bs.cs.isf.temporalregulator3000.core;

import static de.tu_bs.cs.isf.temporalregulator3000.core.StringUtil.firstLetterToUpper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.impl.EcoreFactoryImpl;
import org.eclipse.emf.ecore.resource.Resource;

public class EcoreUtil {
	
	private static EcoreFactory factory = EcoreFactory.eINSTANCE;



	public static EPackage createEPackage(String name, String nsPrefix, String nsURI) {
		EPackage ePackage = factory.createEPackage();

		ePackage.setName(name);
		ePackage.setNsPrefix(nsPrefix);
		ePackage.setNsURI(nsURI);

		return ePackage;
	}

	public static EClass createEClass(String name) {
		return createEClass(name, false, false);
	}

	public static EClass createEClass(String name, boolean _abstract, boolean _interface) {
		EClass eClass = factory.createEClass();

		eClass.setName(name);
		eClass.setAbstract(_abstract);
		eClass.setInterface(_interface);

		return eClass;
	}

	public static EAttribute createEAttribute(String name, EClassifier eType, int lowerBound, int upperBound) {
		EAttribute eAttribute = factory.createEAttribute();

		eAttribute.setName(name);
		eAttribute.setLowerBound(lowerBound);
		eAttribute.setUpperBound(upperBound);
		eAttribute.setEType(eType);

		return eAttribute;
	}

	public static EReference createEReference(String name, EClassifier eType, int lowerBound, int upperBound) {
		EReference eReference = factory.createEReference();

		eReference.setName(name);
		eReference.setLowerBound(lowerBound);
		eReference.setUpperBound(upperBound);
		eReference.setEType(eType);

		return eReference;
	}

	public static List<EObject> getContainedElementsOfType(EObject e, Object referenceObject) {
		List<EObject> returnList = new ArrayList<>();

		if (referenceObject != null) {
			if (e.getClass().isInstance(referenceObject))
				returnList.add(e);

			for (EObject child : e.eContents()) {
				returnList.addAll(getContainedElementsOfType(child, referenceObject));
			}
		}

		return returnList;
	}
	
	public static EGenericType createEGenericType(EClassifier typeArgument) {
		EGenericType genType = factory.createEGenericType();
		genType.setEClassifier(typeArgument);
		
		return genType;
	}
	
	

	public static void createTemporalUpperBoundOCLConstraint(EModelElement element, String featureName, int upperBound) {
		String oclQuery = "self?."+ featureName +"->forAll(proxy1 | "
				+ "self?."+ featureName +"->select(proxy2 | "
				+ "proxy1.temporalRegulatorValidSince <= proxy2.temporalRegulatorValidUntil or proxy1.temporalRegulatorValidUntil >= proxy2.temporalRegulatorValidSince)"
				+ "->size() <= "+ upperBound +")";
		
		createOCLConstraint(element, "UpperBoundsConstraint" + firstLetterToUpper(featureName), oclQuery);
	}
	
	private static void createOCLConstraint(EModelElement element, String constraintName, String query) {
		EAnnotation ecoreAnnotation = element.getEAnnotation("http://www.eclipse.org/emf/2002/Ecore");
		if(ecoreAnnotation != null) {
			String spaceSeperatedExistingConstraintNames = ecoreAnnotation.getDetails().get("constraints");
			List<String> existingConstraintNames = Arrays.asList(spaceSeperatedExistingConstraintNames.split(" "));
			if(existingConstraintNames.contains(constraintName)) {
				createOCLConstraint(element, constraintName+"_", query);
				return;
			}
			
			ecoreAnnotation.getDetails().put("constraints", ecoreAnnotation.getDetails().get("constraints") + " " + constraintName);
		} else {
			ecoreAnnotation = factory.createEAnnotation();
			ecoreAnnotation.setEModelElement(element);
			ecoreAnnotation.setSource("http://www.eclipse.org/emf/2002/Ecore");
			ecoreAnnotation.getDetails().put("constraints", constraintName);
		}
		
		EAnnotation oclAnnotation = element.getEAnnotation("http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot");
		if(oclAnnotation == null) {
			oclAnnotation = factory.createEAnnotation();
			oclAnnotation.setEModelElement(element);
			oclAnnotation.setSource("http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot");
		}
		oclAnnotation.getDetails().put(constraintName, query);
	}
	
	

	public static void correctNameSpaces(Resource resource) {
		List<? extends EObject> packageList = EcoreUtil.getContainedElementsOfType(resource.getContents().get(0), EcoreFactoryImpl.eINSTANCE.createEPackage());
		
		String additionalNameSpaceFragment = resource.getURI().lastSegment().split("\\.")[0];
		additionalNameSpaceFragment = additionalNameSpaceFragment.substring(additionalNameSpaceFragment.indexOf("_temporal"), additionalNameSpaceFragment.length());
		
		for(EObject e : packageList) {
			EPackage ePackage = (EPackage) e;
			ePackage.setNsURI(ePackage.getNsURI() + additionalNameSpaceFragment);
		}
	}
	
	
	
	public static void removeIdFlagsFromAttributes(Resource resource) {
		List<? extends EObject> attributeList = EcoreUtil.getContainedElementsOfType(resource.getContents().get(0), EcoreFactoryImpl.eINSTANCE.createEAttribute());

		for(EObject e : attributeList) {
			EAttribute attribute = (EAttribute) e;
			if(attribute.isID())
				attribute.setID(false);
		}
	}
	

	
	public static String buildCamelCasedFeatureName(EStructuralFeature feature) {
		return getNameSpacesCamelCase(feature.getEContainingClass().getEPackage())
		+ firstLetterToUpper(feature.getEContainingClass().getName())
		+ firstLetterToUpper(feature.getName());
	}
	
	public static String getNameSpacesCamelCase(EPackage p) {
		String namespaces = "";
		for(String namespace : getNamespaces(p))
			namespaces += firstLetterToUpper(namespace);
		return namespaces;
	}
	
	public static String getNameSpacesDotSeparated(EPackage p) {
		String namespaces = "";
		for(String namespace : getNamespaces(p))
			namespaces += "." + namespace;
		return namespaces.substring(1);
	}
	
	private static List<String> getNamespaces(EPackage p) {
		List<String> namespaces = new ArrayList<String>();
		
		if(p.getESuperPackage() != null)
			namespaces.addAll(getNamespaces(p.getESuperPackage()));
		namespaces.add(p.getName());
		
		return namespaces;
	}
	
	public static boolean eStructuralFeaturesMatch(EStructuralFeature feature1, EStructuralFeature feature2) {
		if(!feature1.getName().equals(feature2.getName()))
			return false;
		return eClassesMatch(feature1.getEContainingClass(), feature2.getEContainingClass());
	}
	
	public static boolean eClassesMatch(EClassifier class1, EClassifier class2) {
		if(class1 == null)
			return class2 == null;
		if(class2 == null)
			return class1 == null;
		
		if(!class1.getName().equals(class2.getName()))
			return false;
		if(!class1.getEPackage().getNsURI().equals(class2.getEPackage().getNsURI()))
			return false;
		
		return true;
	}

	public static void saveResource(Resource resource) {
		try {
			resource.save(null);
		} catch (IOException e) {
			throw new RuntimeException("Something went wrong while saving the converted temporal meta model:\n\n" + e.getMessage());
		}
	}
	
}
