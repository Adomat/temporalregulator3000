package de.tu_bs.cs.isf.temporalregulator3000.core;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.core.internal.utils.FileUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.ToolFactory;
import org.eclipse.jdt.core.formatter.CodeFormatter;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.text.edits.TextEdit;

public class TemporalRegulatorCodeFormatter {
	
	private CodeFormatter codeFormatter;

	public void format(IResource res) throws IOException, CoreException {
		if(res instanceof IFile)
			format((IFile) res);
		else throw new RuntimeException("Trying to format java code of resource \"" + res.getFullPath() + "\" which is not a file!");
	}
	
	public void format(IFile registeredFile) throws IOException, CoreException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(registeredFile.getContents(true), registeredFile.getCharset()));
		String line = reader.readLine();

		String fileEnding = FileUtil.getLineSeparator(registeredFile);
		String rawSourceCode = "";
		while (line != null) {
			rawSourceCode += line + fileEnding;
			line = reader.readLine();
		}
		
		reader.close();
		
		createCodeFormatter(registeredFile.getProject());
		String formattedSourceCode = formatCode(rawSourceCode);
		ByteArrayInputStream byteStream = new ByteArrayInputStream(formattedSourceCode.getBytes());
		
		try {
			registeredFile.setContents(byteStream, false, false, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	private void createCodeFormatter(IProject project) {
		if(codeFormatter == null) {
			IJavaProject javaProject = JavaCore.create(project);
			codeFormatter = ToolFactory.createCodeFormatter(javaProject.getOptions(true));
		}
	}
	
	private String formatCode(String contents) {
		IDocument doc = new Document(contents);
		TextEdit edit = codeFormatter.format(CodeFormatter.K_COMPILATION_UNIT, doc.get(), 0, doc.get().length(), 0, null);
		if (edit != null) {
			try {
				edit.apply(doc);
				contents = doc.get();
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return contents;
	}

}
