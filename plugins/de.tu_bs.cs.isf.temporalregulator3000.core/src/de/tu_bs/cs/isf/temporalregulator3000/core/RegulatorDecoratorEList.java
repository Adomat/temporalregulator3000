package de.tu_bs.cs.isf.temporalregulator3000.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Observable;

import org.eclipse.emf.common.util.EList;

public class RegulatorDecoratorEList<T> extends Observable implements EList<T> {
	
	public static class ChangeWrapper {
		public ChangeWrapper() {
			listIndexOfOperation = -1;
			elements = new ArrayList<>();
		}
		
		public List<Object> elements;
		public int listIndexOfOperation;
	}
	
	public static class AdditionChangeWrapper extends ChangeWrapper {
		public AdditionChangeWrapper() {
			super();
		}
	}
	
	public static class DeletionChangeWrapper extends ChangeWrapper {
		public DeletionChangeWrapper() {
			super();
		}
	}
	
	public static class MoveChangeWrapper extends ChangeWrapper {
		public MoveChangeWrapper() {
			super();
		}
	}
	
	
	
	private List<T> component;
	// This is needed to tell the observing adapter what attribute this list is for
	private int featureID;
	
	

	@SuppressWarnings("unused")
	private RegulatorDecoratorEList() {
	}

	public RegulatorDecoratorEList(List<T> component, int featureID) {
		super();
		this.component = component;
		this.featureID = featureID;
	}
	
	public int getFeatureID() {
		return this.featureID;
	}
	
	@Override
	public String toString() {
		String result = "de.tu_bs.cs.isf.temporalregulator3000.core.RegulatorDecoratorEList@" + this.hashCode() + "\n[";
		for(T element : this) {
			result += "\n\n" + element.toString();
		}
		return result + "\n\n]";
	}
	
	

	@Override
	public boolean add(T e) {
		boolean r = component.add(e);
		this.setChanged();

		ChangeWrapper info = new AdditionChangeWrapper();
		info.elements.add(e);
		this.notifyObservers(info);
		
		return r;
	}

	@Override
	public void add(int index, T element) {
		component.add(index, element);
		this.setChanged();

		ChangeWrapper info = new AdditionChangeWrapper();
		info.elements.add(element);
		info.listIndexOfOperation = index;
		this.notifyObservers(info);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		boolean r = component.addAll(c);
		this.setChanged();
		
		ChangeWrapper info = new AdditionChangeWrapper();
		info.elements.addAll(c);
		this.notifyObservers(info);
		
		return r;
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		boolean r = component.addAll(index, c);
		this.setChanged();
		
		ChangeWrapper info = new AdditionChangeWrapper();
		info.elements.addAll(c);
		info.listIndexOfOperation = index;
		this.notifyObservers(info);
		
		return r;
	}

	@Override
	public void clear() {
		component.clear();
		this.setChanged();
		
		ChangeWrapper info = new DeletionChangeWrapper();
		info.elements.addAll(component);
		this.notifyObservers(info);
	}

	@Override
	public boolean contains(Object o) {
		return component.contains(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return component.containsAll(c);
	}

	@Override
	public T get(int index) {
		return component.get(index);
	}

	@Override
	public int indexOf(Object o) {
		return component.indexOf(o);
	}

	@Override
	public boolean isEmpty() {
		return component.isEmpty();
	}

	@Override
	public Iterator<T> iterator() {
		return component.iterator();
	}

	@Override
	public int lastIndexOf(Object o) {
		return component.lastIndexOf(o);
	}

	@Override
	public ListIterator<T> listIterator() {
		return component.listIterator();
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		return component.listIterator(index);
	}

	@Override
	public boolean remove(Object o) {
		boolean r = component.remove(o);
		this.setChanged();
		
		ChangeWrapper info = new DeletionChangeWrapper();
		info.elements.add(o);
		this.notifyObservers(info);
		
		return r;
	}

	@Override
	public T remove(int index) {
		T r = component.remove(index);
		this.setChanged();
		
		ChangeWrapper info = new DeletionChangeWrapper();
		info.listIndexOfOperation = index;
		this.notifyObservers(info);
		
		return r;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		boolean r = component.removeAll(c);
		this.setChanged();
		
		ChangeWrapper info = new DeletionChangeWrapper();
		info.elements.addAll(c);
		this.notifyObservers(info);
		
		return r;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		List<T> differenceList = new LinkedList<>(component);
		boolean r = component.retainAll(c);
		differenceList.removeAll(component);
		this.setChanged();
		
		ChangeWrapper info = new DeletionChangeWrapper();
		info.elements.addAll(differenceList);
		this.notifyObservers(info);
		
		return r;
	}

	@Override
	public T set(int index, T element) {
		T r = component.set(index, element);
		
		// Reduce this operation to a delete and add operation.
		// 1. Propagate deletion
		this.setChanged();
		ChangeWrapper deleteInfo = new DeletionChangeWrapper();
		deleteInfo.listIndexOfOperation = index;
		this.notifyObservers(deleteInfo);
		
		// 2. Propagate addition
		this.setChanged();
		ChangeWrapper additionInfo = new AdditionChangeWrapper();
		additionInfo.elements.add(element);
		additionInfo.listIndexOfOperation = index;
		this.notifyObservers(additionInfo);
		
		return r;
	}

	@Override
	public int size() {
		return component.size();
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		return component.subList(fromIndex, toIndex);
	}

	@Override
	public Object[] toArray() {
		return component.toArray();
	}

	@Override
	public <Type> Type[] toArray(Type[] a) {
		return component.toArray(a);
	}

	@Override
	public T move(int newPosition, int oldPosition) {
		T r;
		if(component instanceof EList) {
			r = ((EList<T>) component).move(newPosition, oldPosition);
		} else {
			// TODO implement!
			throw new UnsupportedOperationException("Not implemented!");
		}
		T object = component.get(oldPosition);
		
		this.setChanged();
		ChangeWrapper info = new MoveChangeWrapper();
		info.elements.add(object);
		info.listIndexOfOperation = newPosition;
		this.notifyObservers(info);
		
		return r;
	}

	@Override
	public void move(int newPosition, T object) {
		if(component instanceof EList) {
			((EList<T>) component).move(newPosition, object);
		} else {
			// TODO
			throw new UnsupportedOperationException("Not implemented!");
		}
		
		this.setChanged();
		ChangeWrapper info = new MoveChangeWrapper();
		info.elements.add(object);
		info.listIndexOfOperation = newPosition;
		this.notifyObservers(info);
	}

}
























