package de.tu_bs.cs.isf.temporalregulator3000.core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;
import org.osgi.framework.Bundle;

public class UnZipper {
	
	private static final int BUFFER_SIZE = 102400;
	
	public void unzip(ZipFile zipFolder, File targetFolderFile) throws IOException {
		Enumeration<? extends ZipEntry> e = zipFolder.entries();
		
		while (e.hasMoreElements()) {
			ZipEntry zipEntry = (ZipEntry) e.nextElement();
			File file = new File(targetFolderFile, zipEntry.getName());
			
			if (zipEntry.isDirectory()) {
				file.mkdirs();
			} else {
				File parentFile = file.getParentFile();
				if (parentFile != null && !parentFile.exists()) {
					parentFile.mkdirs();
				}
				
				if (new Path(file.getPath()).getFileExtension().equals("java")) {
					OutputStreamWriter outStream = null;
					InputStreamReader inStream = null;
					
					try {
						outStream = new OutputStreamWriter(new FileOutputStream(file), ResourcesPlugin.getEncoding());
						inStream = new InputStreamReader(zipFolder.getInputStream(zipEntry), "ISO-8859-1");
						
						char[] buffer = new char[BUFFER_SIZE];
						while (true) {
							int length = inStream.read(buffer);
							if (length < 0)
								break;
							outStream.write(buffer, 0, length);
						}
					} finally {
						if (outStream != null)
							outStream.close();
						
						if (inStream != null)
							inStream.close();
					}
				} else {
					OutputStream outStream = null;
					InputStream inStream = null;
					
					try {
						outStream = new FileOutputStream(file);
						inStream = zipFolder.getInputStream(zipEntry);
						
						byte[] buffer = new byte[BUFFER_SIZE];
						while (true) {
							int length = inStream.read(buffer);
							if (length < 0) {
								break;
							}
							outStream.write(buffer, 0, length);
						}
					} finally {
						if (outStream != null)
							outStream.close();
						if (inStream != null)
							inStream.close();
					}
				}
			}
		}
	}

}
