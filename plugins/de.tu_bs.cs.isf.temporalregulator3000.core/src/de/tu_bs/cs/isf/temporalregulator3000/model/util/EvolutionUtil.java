package de.tu_bs.cs.isf.temporalregulator3000.model.util;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporalElement;

public class EvolutionUtil {

	/**
	 * Return type equals input type. IDs are preserved
	 * 
	 * @param model
	 * @param date
	 * @return
	 */
	public static <T extends EObject> T getCopyOfValidModel(T model, Date date) {
		TemporalModelCopier copier = new TemporalModelCopier();
		EObject result = copier.copy(model);
		copier.copyReferences();

		@SuppressWarnings("unchecked")
		T copiedModel = (T) result;

		removeAllInvalidElements(copiedModel, date);
		return copiedModel;
	}

	/**
	 * Return type equals input type. IDs NOT preserved
	 * 
	 * @param model
	 * @param date
	 * @return
	 */
	public static <T extends EObject> T getCopyOfValidModelWithNewIds(T model, Date date) {
		T copiedModel = EcoreUtil.copy(model);
		removeAllInvalidElements(copiedModel, date);
		return copiedModel;
	}
	
	/**
	 * Sorts depending on TemporalRegulatorValidSince (ascending, starting with TemporalRegulatorValidSince == null)
	 * @param elements
	 * @return
	 */
	public static <T extends TemporalElement> List<T> sortInChronologicalOrder(List<T> elements) {
		List<Date> dates = collectDates(elements);
		
		List<T> sortedElements = new LinkedList<T>();
		
		for (T element : elements) {
			if (element.getTemporalRegulatorValidSince() == null) {
				sortedElements.add(element);
			}
		}
		
		for (Date date : dates) {
			for (T element : elements) {
				if (element.getTemporalRegulatorValidSince() != null) {
					if (element.getTemporalRegulatorValidSince().equals(date)) {
						sortedElements.add(element);
					}
				}
			}
		}
		
		return sortedElements;
	}
	
	/**
	 * 
	 * @param elements
	 * @return
	 */
	public static <T extends TemporalElement> T getEarliest(Collection<T> elements) {
		return getEarliestAfter(elements, null);
	}
	
	/**
	 * 
	 * @param elements
	 * @param after
	 * @return
	 */
	public static <T extends TemporalElement> T getEarliestAfter(Collection<T> elements, Date after) {
		boolean first = true;
		Date earliestSince = null;
		T earliestElement = null;
		
		for (T element : elements) {
			Date TemporalRegulatorValidSince = element.getTemporalRegulatorValidSince();
			
			if (after != null && (TemporalRegulatorValidSince == null || !TemporalRegulatorValidSince.after(after))) {
				continue;
			}
			
			if (first) {
				earliestSince = TemporalRegulatorValidSince;
				earliestElement = element;
				first = false;
				continue;
			}
			
			if (TemporalRegulatorValidSince == null) {
				earliestSince = null;
				earliestElement = element;
			}
			else {
				if(earliestSince != null && TemporalRegulatorValidSince.before(earliestSince)) {
					earliestSince = TemporalRegulatorValidSince;
					earliestElement = element;
				}
			}
		}
		
		return earliestElement;
	}

	/**
	 * Use with caution! Deletes elements from model with TemporalElements!
	 * 
	 * @param featureModel
	 * @param date
	 */
	public static void removeAllInvalidElements(EObject model, Date date) {
		List<EObject> eObjectsToRemove = new LinkedList<EObject>();
		{
			TreeIterator<EObject> iterator = model.eAllContents();

			while (iterator.hasNext()) {
				EObject content = iterator.next();
				if (content instanceof TemporalElement) {
					if (!isValid(((TemporalElement) content), date)) {
						// EcoreUtil.remove(content);
						eObjectsToRemove.add(content);
					}
				}
			}

		}

		for (EObject eObjectToRemove : eObjectsToRemove) {
			EcoreUtil.delete(eObjectToRemove, true);
		}
	}

	/**
	 * Checks if an element is valid at a given date
	 * 
	 * @param temporalElement
	 * @param date
	 * @return
	 */
	public static boolean isValid(TemporalElement temporalElement, Date date) {
		if (date == null) {
			if (temporalElement.getTemporalRegulatorValidSince() == null && temporalElement.getTemporalRegulatorValidUntil() == null) {
				return true;
			} else {
				return false;
			}
		}

		if ((temporalElement.getTemporalRegulatorValidSince() == null || temporalElement.getTemporalRegulatorValidSince().before(date)
				|| temporalElement.getTemporalRegulatorValidSince().equals(date))
				&& (temporalElement.getTemporalRegulatorValidUntil() == null || temporalElement.getTemporalRegulatorValidUntil().after(date))) {
			return true;
		}

		return false;
	}

	/**
	 * Checks if the validity of @elementToCheck is within the validity
	 * of @elementToCheckAgainst Usable, e.g., to check if a constraint's
	 * ( @elementToCheck ) validity is no longer than it's referencing features'
	 * validity ( @elementToCheckAgainst )
	 * 
	 * @param elementToCheck
	 * @param elementToCheckAgainst
	 * @return
	 */
	public static boolean isWithinValidityOf(TemporalElement elementToCheck,
			TemporalElement elementToCheckAgainst) {
		if (elementToCheck.getTemporalRegulatorValidSince() == null && elementToCheckAgainst.getTemporalRegulatorValidSince() != null) {
			return false;
		} else if (elementToCheck.getTemporalRegulatorValidSince() != null && elementToCheckAgainst.getTemporalRegulatorValidSince() != null
				&& elementToCheck.getTemporalRegulatorValidSince().before(elementToCheckAgainst.getTemporalRegulatorValidSince())) {
			return false;
		} else if (elementToCheck.getTemporalRegulatorValidUntil() == null && elementToCheckAgainst.getTemporalRegulatorValidUntil() != null) {
			return false;
		} else if (elementToCheck.getTemporalRegulatorValidUntil() != null && elementToCheckAgainst.getTemporalRegulatorValidUntil() != null
				&& elementToCheck.getTemporalRegulatorValidUntil().after(elementToCheckAgainst.getTemporalRegulatorValidUntil())) {
			return false;
		}
		return true;
	}

	/**
	 * Checks if the validity of @elementToCheck is within the validity of each
	 * of @elementsToCheckAgainst Usable, e.g., to check if a constraint's
	 * ( @elementToCheck ) validity is no longer than it's referencing features'
	 * validity ( @elementsToCheckAgainst )
	 * 
	 * @param elementToCheck
	 * @param elementsToCheckAgainst
	 * @return
	 */
	public static boolean isWithinValidityOf(TemporalElement elementToCheck,
			List<TemporalElement> elementsToCheckAgainst) {
		for (TemporalElement elementToCheckAgainst : elementsToCheckAgainst) {
			if (!isWithinValidityOf(elementToCheck, elementToCheckAgainst)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if the each validity of @elementsToCheck is within the validity
	 * of @elementToCheckAgainst Usable, e.g., to check if a constraint's
	 * ( @elementsToCheck ) validity is no longer than it's referencing features'
	 * validity ( @elementToCheckAgainst )
	 * 
	 * @param elementsToCheck
	 * @param elementToCheckAgainst
	 * @return
	 */
	public static boolean areWithinValidityOf(List<TemporalElement> elementsToCheck,
			TemporalElement elementToCheckAgainst) {
		for (TemporalElement elementToCheck : elementsToCheck) {
			if (!isWithinValidityOf(elementToCheck, elementToCheckAgainst)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if the each validity of @elementsToCheck is within the validity of
	 * each @elementsToCheckAgainst Usable, e.g., to check if a constraint's
	 * ( @elementsToCheck ) validity is no longer than it's referencing features'
	 * validity ( @elementsToCheckAgainst )
	 * 
	 * @param elementsToCheck
	 * @param elementsToCheckAgainst
	 * @return
	 */
	public static boolean areWithinValitiyOf(List<TemporalElement> elementsToCheck,
			List<TemporalElement> elementsToCheckAgainst) {
		for (TemporalElement elementToCheck : elementsToCheck) {
			if (!isWithinValidityOf(elementToCheck, elementsToCheckAgainst)) {
				return false;
			}
		}
		return true;
	}

	// TODO assumption only one valid temporal element at a time, check that?
	public static <T extends TemporalElement> T getValidTemporalElement(List<T> elements, Date date) {
		if (elements == null || elements.isEmpty()) {
			// TODO proper logging
//			System.err.println(
//					"Something bad happened. EList<?> given to getValidTemporalElement(...) in HyEvolutionUtil.java was null or empty");
			return null;
		}

		if (date == null) {
			if (elements.size() == 1) {
				return elements.get(0);
			} else {
				System.err.println(
						"Something bad happened. Date given to getValidTemporalElement(...) in HyEvolutionUtil.java was null and more than one element was present.");
				return null;
			}
		}

		for (T element : elements) {
			if (isValid(element, date)) {
				return element;
			}
		}

		return null;
	}

	/**
	 * Non copy
	 * 
	 * @param elements
	 * @param date
	 * @return
	 */
	public static <T extends TemporalElement> List<T> getValidTemporalElements(List<T> elements, Date date) {
		List<T> validElements = new LinkedList<T>();

		if (elements == null || elements.isEmpty()) {
			return validElements;
		}

		if (date == null) {
//			System.out.println(
//					"Date given to getValidTemporalElements(...) in HyEvolutionUtil.java was null. Returning all elements!");
			validElements.addAll(elements);
			return validElements;
		}

		for (T element : elements) {
			if (isValid(element, date)) {
				validElements.add(element);
			}
		}

		return validElements;
	}

	public static <T extends TemporalElement> List<T> getInvalidTemporalElements(List<T> elements, Date date) {
		List<T> invalidElements = new LinkedList<T>(elements);

		if (date != null) {
			invalidElements.removeAll(getValidTemporalElements(elements, date));
		}

		return invalidElements;
	}
	
	/**
	 * Returns a list that contains all elements of @param elements that are valid within the interval between @param lowerBound and @param upperBound
	 * @param elements
	 * @param lowerBound
	 * @param upperBound
	 * @return
	 */
	public static <T extends TemporalElement> List<T> getValidTemporalElements(List<T> elements, Date lowerBound, Date upperBound) {
		Set<Date> datesToCheck = new HashSet<Date>();
		
		if (lowerBound != null) {
			datesToCheck.add(lowerBound);
		}
		
		List<Date> datesOfElements = collectDates(elements);
		for (Date dateOfElement : datesOfElements) {
			if ((upperBound == null || dateOfElement.before(upperBound)) && (lowerBound == null || !lowerBound.after(dateOfElement))) {
				datesToCheck.add(dateOfElement);
			}
		}
		
		List<T> validElements = new LinkedList<T>();
		
		for (T element : elements) {
			for (Date dateToCheck : datesToCheck) {
				if (isValid(element, dateToCheck)) {
					validElements.add(element);
				}
			}
		}
		
		return validElements;
	}

	/**
	 * Returns a sorted list of all available dates of TemporalElements in the
	 * given model
	 * 
	 * @param model
	 * @return
	 */
	public static List<Date> collectDates(EObject model) {
		if (model == null) {
			return new LinkedList<Date>();
		}

		// NOTE: Check if equals and identity work for hash set.
		Set<Date> rawDates = new HashSet<Date>();

		Iterator<EObject> iterator = model.eAllContents();
		// boolean TemporalRegulatorValidSinceNull = false;

		while (iterator.hasNext()) {
			EObject eObject = iterator.next();

			if (eObject instanceof TemporalElement) {
				TemporalElement temporalElement = (TemporalElement) eObject;
				Date TemporalRegulatorValidSince = temporalElement.getTemporalRegulatorValidSince();

				if (TemporalRegulatorValidSince != null) {
					rawDates.add(TemporalRegulatorValidSince);
					// } else {
					// TemporalRegulatorValidSinceNull = true;
				}

				Date TemporalRegulatorValidUntil = temporalElement.getTemporalRegulatorValidUntil();

				if (TemporalRegulatorValidUntil != null) {
					rawDates.add(TemporalRegulatorValidUntil);
				}
			}
		}

		List<Date> dates = new LinkedList<Date>(rawDates);

		Collections.sort(dates);

		return dates;
	}

	/**
	 * Collects a sorted list of unique dates of valid until and valid since of the
	 * given elements
	 * 
	 * @param elements
	 * @return sorted list of unique dates (created using a set)
	 */
	public static List<Date> collectDates(List<? extends TemporalElement> elements) {
		// NOTE: Check if equals and identity work for hash set.
		Set<Date> rawDates = new HashSet<Date>();

		for (TemporalElement temporalElement : elements) {
			Date TemporalRegulatorValidSince = temporalElement.getTemporalRegulatorValidSince();

			if (TemporalRegulatorValidSince != null) {
				rawDates.add(TemporalRegulatorValidSince);
				// } else {
				// TemporalRegulatorValidSinceNull = true;
			}

			Date TemporalRegulatorValidUntil = temporalElement.getTemporalRegulatorValidUntil();

			if (TemporalRegulatorValidUntil != null) {
				rawDates.add(TemporalRegulatorValidUntil);
			}
		}
		List<Date> dates = new LinkedList<Date>(rawDates);

		Collections.sort(dates);

		return dates;
	}

	public static boolean modelHasValiditySinceNull(EObject model) {
		Iterator<EObject> iterator = model.eAllContents();

		while (iterator.hasNext()) {
			EObject eObject = iterator.next();

			if (eObject instanceof TemporalElement) {
				TemporalElement temporalElement = (TemporalElement) eObject;
				Date TemporalRegulatorValidSince = temporalElement.getTemporalRegulatorValidSince();

				if (TemporalRegulatorValidSince == null) {
					return true;
				}
			}
		}

		return false;
	}

	public static boolean modelHasValidityUntilNull(EObject model) {
		Iterator<EObject> iterator = model.eAllContents();

		while (iterator.hasNext()) {
			EObject eObject = iterator.next();

			if (eObject instanceof TemporalElement) {
				TemporalElement temporalElement = (TemporalElement) eObject;
				Date TemporalRegulatorValidUntil = temporalElement.getTemporalRegulatorValidUntil();

				if (TemporalRegulatorValidUntil == null) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Collects all dates from parameter temporalModel and selects the date that is
	 * closest to the parameter date. If parameter date == null, the earliest date
	 * will be selected. If dates from parameter temporalModel are empty, null is
	 * returned.
	 * 
	 * @param dates
	 * @param date
	 * @return May be null if parameter temporalModel has no evolution steps.
	 */
	public static Date getClosestEvolutionDate(EObject temporalModel, Date date) {
		List<Date> dates = collectDates(temporalModel);

		return getClosestEvolutionDate(dates, date);
	}

	/**
	 * Selects the date of parameter dates that is closest to parameter date. If
	 * parameter date == null, the earliest date will be selected. If parameter are
	 * empty, null is returned.
	 * 
	 * @param dates
	 * @param date
	 * @return May be null if parameter temporalModel has no evolution steps.
	 */
	public static Date getClosestEvolutionDate(Collection<Date> dates, Date date) {
		if (dates == null || dates.isEmpty()) {
			return null;
		}

		Date closestDate = null;

		for (Date iterator : dates) {
			if (iterator.equals(date)) {
				return iterator;
			} else if (closestDate == null) {
				closestDate = iterator;
			} else {
				long distanceToOldClosestDate = (Math.abs(date.getTime() - closestDate.getTime()));
				long distanceToDate = (Math.abs(date.getTime() - iterator.getTime()));

				if (distanceToOldClosestDate > distanceToDate) {
					closestDate = iterator;
				}
			}
		}

		return closestDate;
	}

	public static <T extends TemporalElement> boolean isEverValid(T element) {
		if (element == null) {
			return false;
		}
		
		if ((element.getTemporalRegulatorValidSince() != null && element.getTemporalRegulatorValidUntil() != null
								&& !element.getTemporalRegulatorValidUntil().after(element.getTemporalRegulatorValidSince())) || (element.getTemporalRegulatorValidSince() == null && element.getTemporalRegulatorValidUntil() != null && element.getTemporalRegulatorValidUntil().getTime() == Long.MIN_VALUE) ) {
			return false;
		}
		
		return true;
	}
	
}
