package de.tu_bs.cs.isf.temporalregulator3000.core;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;

import de.tu_bs.cs.isf.temporalregulator3000.core.clockextension.IRegulatorClock;
import de.tu_bs.cs.isf.temporalregulator3000.core.clockextension.IRegulatorClockListener;

public class ClockMechanism {
	
	public static ClockMechanism instance = new ClockMechanism();
	
	private ClockMechanism() { }
	
	// ==================================================================================== //

	private String EXTENSION_POINT_ID = "de.tu_bs.cs.isf.temporalregulator3000.core.clockextension";
	private String LISTENING_POINT_ID = "de.tu_bs.cs.isf.temporalregulator3000.core.clocklistener";

	private Date defaultTime = new Date(System.currentTimeMillis());
	private Map<String, Date> dateMap = new HashMap<>();
	
	
	/**
	 * Returns the time of the default clock
	 * <br><br>
	 * It is <strong>not recommended</strong> to use this method.
	 * Use setTime(String key, Date newTime) and getTime(String key) instead in order to assign dates per resource or use-case
	 * @return default clock time
	 */
	public Date getTime() {
		return getTime(null);
	}
	
	/**
	 * Returns a time that is mapped to the given key
	 * @param key a String that is used as key
	 * @return the mapped time
	 */
	public Date getTime(String key) {
		if(key == null)
			return defaultTime;
		
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		
		if(registry != null) {
			IExtensionPoint point = registry.getExtensionPoint(EXTENSION_POINT_ID);
			
			if (point == null) {
				System.err.println("Extension point \"" + EXTENSION_POINT_ID + "\" not found.");
				return getDateMapEntry(key);
			}
			
			IExtension[] extensions = point.getExtensions();
			
			for(IExtension extension : extensions) {
				IConfigurationElement[] configurationElements = extension.getConfigurationElements();
//				String pluginId = extension.getNamespaceIdentifier();
				
				for (IConfigurationElement configurationElement : configurationElements) {
					String configurationElementName = configurationElement.getName();
					
					if (configurationElementName.equals("clock")) {
						Object clockExtension;
						try {
							clockExtension = configurationElement.createExecutableExtension("class");
							
							if (clockExtension instanceof IRegulatorClock) {
								return ((IRegulatorClock) clockExtension).getTime(key);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

		return getDateMapEntry(key);
	}
	
	private Date getDateMapEntry(String key) {
		Date entry = dateMap.get(key);
		if(entry == null)
			return defaultTime;
		return entry;
	}
	
	
	
	/**
	 * Sets the time of the default clock
	 * <br><br>
	 * It is <strong>not recommended</strong> to use this method.
	 * Use setTime(String key, Date newTime) instead in order to assign dates per resource or use-case
	 * @param newTime
	 */
	public void setTime(Date newTime) {
		setTime(null, newTime);
	}
	
	/**
	 * Sets a time that is mapped to the given key
	 * @param key a String that is used as key
	 * @param newTime time to map to the key
	 */
	public void setTime(String key, Date newTime) {
		if(key == null)
			defaultTime = newTime;
		else
			dateMap.put(key, newTime);
		
		// Notify listeners
		
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		
		if(registry != null) {
			IExtensionPoint point = registry.getExtensionPoint(LISTENING_POINT_ID);
			
			if (point == null) {
				System.err.println("Extension point \"" + LISTENING_POINT_ID + "\" not found.");
				return;
			}
			
			IExtension[] extensions = point.getExtensions();
			
			for(IExtension extension : extensions) {
				IConfigurationElement[] configurationElements = extension.getConfigurationElements();
//				String pluginId = extension.getNamespaceIdentifier();
				
				for (IConfigurationElement configurationElement : configurationElements) {
					String configurationElementName = configurationElement.getName();
					
					if (configurationElementName.equals("clock_listener")) {
						Object clockExtension;
						try {
							clockExtension = configurationElement.createExecutableExtension("class");
							
							if (clockExtension instanceof IRegulatorClockListener) {
								((IRegulatorClockListener) clockExtension).handleTimeChange(key, newTime);
							}
						} catch (CoreException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
}
