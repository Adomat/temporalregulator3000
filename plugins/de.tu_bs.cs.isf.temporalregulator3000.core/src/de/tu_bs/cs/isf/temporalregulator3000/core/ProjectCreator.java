package de.tu_bs.cs.isf.temporalregulator3000.core;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.internal.ui.preferences.NewJavaProjectPreferencePage;

public class ProjectCreator {
	protected IProject inputProject;
	protected IProject generatedProject;
	protected IJavaProject generatedJavaProject;
	
	protected List<String> srcFolders;
	protected List<String> requiredBundles;
	
	public ProjectCreator(IProject inputProject) {
		this.inputProject = inputProject;
		
		this.srcFolders = new ArrayList<>();
		this.srcFolders.add("src");
		this.srcFolders.add("src-gen");
		this.srcFolders.add("src-adapter");
		
		this.requiredBundles = new ArrayList<>();
		this.requiredBundles.add(inputProject.getName());
		this.requiredBundles.add("de.tu_bs.cs.isf.temporalregulator3000.core");
		this.requiredBundles.add("org.eclipse.emf.ecore.xmi");
		
		String projectName = this.inputProject.getName() + ".temporal";
		generatedProject = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
	}
	
	public boolean overrideNecessary() {
		return generatedProject.exists() ? true : false;
	}
	
	public IProject createPluginProject() throws CoreException {
		IProgressMonitor monitor = new NullProgressMonitor();
		
		if(generatedProject.exists())
			generatedProject.delete(true, monitor);

		generatedJavaProject = JavaCore.create(generatedProject);
		IProjectDescription projDesc = ResourcesPlugin.getWorkspace().newProjectDescription(generatedProject.getName());
		projDesc.setComment("Created with the Temporal Regulator 3000!");

		projDesc.setNatureIds(getNatures());
		projDesc.setBuildSpec(getBuilders(projDesc));

		// Create the project
		projDesc.setLocation(null);
		generatedProject.create(projDesc, monitor);

		// And open it
		generatedProject.open(monitor);
		generatedProject.setDescription(projDesc, monitor);

		// Set the class path entries
		setClassPathEntries(monitor);
		setOutputLocation(monitor);
		
		// And create plug-in specific files
		createManifestFile(monitor);
		createBuildFile(monitor);

		return generatedProject;
	}
	
	public IJavaProject getGeneratedJavaProject() {
		if(generatedJavaProject != null)
			return generatedJavaProject;
		else throw new UnsupportedOperationException("The Java Project is not yet set up.");
	}
	
	
	

	protected void createBuildFile(IProgressMonitor monitor) throws CoreException {
		// Create build.properties file
		final StringBuilder bpContent = new StringBuilder("source.. = ");
		for (final Iterator<String> iterator = this.srcFolders.iterator(); iterator.hasNext();) {
			bpContent.append(iterator.next()).append("/");
			if (iterator.hasNext()) {
				bpContent.append(",\n           ");
			}
		}
		bpContent.append("\n");
		bpContent.append("bin.includes = META-INF/,.\n");
		
		createFile("build.properties", generatedProject, bpContent.toString(), monitor);
	}

	protected void createManifestFile(IProgressMonitor monitor) throws CoreException {
		// Create the Manifest File
		final StringBuilder maniContent = new StringBuilder("Manifest-Version: 1.0\n");
		maniContent.append("Automatic-Module-Name: de.tu_bs.cs.isf.temporalregulator3000.example.temporal\n");
		maniContent.append("Bundle-ManifestVersion: 2\n");
		maniContent.append("Bundle-Name: " + generatedProject.getName() + "\n");
		maniContent.append("Bundle-SymbolicName: " + generatedProject.getName() + "; singleton:=true\n");
		maniContent.append("Bundle-Version: 1.0.0\n");
		
		if (!this.requiredBundles.isEmpty()) {
			maniContent.append("Require-Bundle:");
			for (final Iterator<String> iterator = this.requiredBundles.iterator(); iterator.hasNext();) {
				maniContent.append(" " + iterator.next() + ";visibility:=reexport");
				if (iterator.hasNext()) {
					maniContent.append(",\n");
				} else {
					maniContent.append("\n");
				}
			}
		}

		maniContent.append("Import-Package: org.osgi.framework\r\n");
		maniContent.append("Bundle-RequiredExecutionEnvironment: JavaSE-1.7\r\n");
//		maniContent.append("Bundle-ActivationPolicy: lazy\r\n");
//		maniContent.append("Bundle-Activator: org.eclipse.ppc.Activator\r\n");

		final IFolder metaInf = generatedProject.getFolder("META-INF");
		if(metaInf.exists())
			metaInf.delete(true, monitor);
		metaInf.create(false, true, monitor);

		createFile("MANIFEST.MF", metaInf, maniContent.toString(), monitor);
	}

	protected void createFile(String fileName, IContainer folder, String content, IProgressMonitor progressMonitor) throws CoreException {
		IFile file = folder.getFile(new Path(fileName));
		if(file.exists())
			file.delete(true, progressMonitor);
		file.create(new ByteArrayInputStream(content.getBytes()), true, progressMonitor);
	}

	protected void setClassPathEntries(IProgressMonitor monitor) throws CoreException {
		final List<IClasspathEntry> classpathEntries = new ArrayList<>();

		Collections.reverse(this.srcFolders);
		for (final String srcFolder : this.srcFolders) {
			final IFolder src = generatedProject.getFolder(srcFolder);
			if (src.exists())
				src.delete(true, monitor);
			src.create(true, true, monitor);
			
			final IClasspathEntry srcClasspathEntry = JavaCore.newSourceEntry(src.getFullPath());
			classpathEntries.add(0, srcClasspathEntry);
		}

		classpathEntries.addAll(Arrays.asList(NewJavaProjectPreferencePage.getDefaultJRELibrary()));
		classpathEntries.add(JavaCore.newContainerEntry(new Path("org.eclipse.pde.core.requiredPlugins")));

		generatedJavaProject.setRawClasspath(classpathEntries.toArray(new IClasspathEntry[classpathEntries.size()]), monitor);
	}
	
	protected void setOutputLocation(IProgressMonitor monitor) throws CoreException {
		IFolder binFolder = generatedProject.getFolder("bin");
		if(binFolder.exists())
			binFolder.delete(true, monitor);
		binFolder.create(true, true, monitor);
		generatedJavaProject.setOutputLocation(new Path("/" + generatedProject.getName() + "/bin"), monitor);
	}

	protected ICommand[] getBuilders(IProjectDescription projDesc) {
		// Add Builders
		List<ICommand> builders = new ArrayList<>();

		final ICommand java = projDesc.newCommand();
		java.setBuilderName(JavaCore.BUILDER_ID);
		builders.add(java);

		final ICommand manifest = projDesc.newCommand();
		manifest.setBuilderName("org.eclipse.pde.ManifestBuilder");
		builders.add(manifest);

		final ICommand schema = projDesc.newCommand();
		schema.setBuilderName("org.eclipse.pde.SchemaBuilder");
		builders.add(schema);

		return builders.toArray(new ICommand[builders.size()]);
	}

	protected String[] getNatures() {
		List<String> natureIds = new ArrayList<>();
		natureIds.add(JavaCore.NATURE_ID);

		return natureIds.toArray(new String[natureIds.size()]);
	}

	public String getGeneratedProjectName() {
		return this.generatedProject.getName();
	}
	
}
