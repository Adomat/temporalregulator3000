package de.tu_bs.cs.isf.temporalregulator3000.converter;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.swt.widgets.Display;

import de.tu_bs.cs.isf.temporalregulator3000.api.CodeGenerator;
import de.tu_bs.cs.isf.temporalregulator3000.converter.execution.EcoreModelTemporalizer;
import de.tu_bs.cs.isf.temporalregulator3000.converter.resources.EcoreResourceManager;
import de.tu_bs.cs.isf.temporalregulator3000.converter.resources.GenModelResourceManager;
import de.tu_bs.cs.isf.temporalregulator3000.core.DialogUtil;
import de.tu_bs.cs.isf.temporalregulator3000.core.EcoreUtil;
import de.tu_bs.cs.isf.temporalregulator3000.core.ProjectCreator;
import de.tu_bs.cs.isf.temporalregulator3000.slicing.SlicingProjectCreator;
import de.tu_bs.cs.isf.temporalregulator3000.viewer.ViewerProjectCreator;

public class ModelConverter {
	private IFile originalEcoreFile;

	private EcoreResourceManager ecoreResourceManager;
	private EcoreModelTemporalizer conversionExecuter;
	private ProjectCreator projectCreator;
	private GenModelResourceManager genModelResourceManager;

	private IProject temporalProject;
	private Resource temporalEcoreResource;
	
	private boolean conversionFailed = false;
	private int eClassesTemporalized, eAttributesTemporalized, eReferencesTemporalized;
	
	

	public ModelConverter(IFile file) {
		this.originalEcoreFile = file;
		
		this.projectCreator = new ProjectCreator(file.getProject());
		this.ecoreResourceManager = new EcoreResourceManager();
		this.genModelResourceManager = new GenModelResourceManager();
		this.conversionExecuter = new EcoreModelTemporalizer(this);
	}
	
	

	public void convert(List<EObject> elementsToConvert) {
		if(this.projectCreator.overrideNecessary())
			if(DialogUtil.getInstance().openQuestionDialog("A project \"" + this.projectCreator.getGeneratedProjectName() + "\" already exists.\n\nDo you want to override it?", new String[] {"Yes", "No"}) != 0)
				return;
		
		Job job = Job.create("Regulating temporality of \"" + ((IFile) this.originalEcoreFile).getName() + "\".", (ICoreRunnable) monitor -> {
			long startTime = System.currentTimeMillis();
			
			monitor.beginTask("Starting the process...", 15);
			try {
				monitor.setTaskName("Searching Generator Model for \"" + ((IFile) this.originalEcoreFile).getName() + "\"...");
				this.genModelResourceManager.searchOriginalGenModel(this.originalEcoreFile);
				monitor.worked(1);
				if(monitor.isCanceled())
					return;

				monitor.setTaskName("Creating Plugin Project \"" + this.projectCreator.getGeneratedProjectName() + "\"...");
				this.temporalProject = projectCreator.createPluginProject();
				if(this.temporalProject == null)
					throw new RuntimeException("Could not create \"" + this.projectCreator.getGeneratedProjectName() + "\" Plugin Project for the converted temporal meta model to \"" + ((IFile) this.originalEcoreFile).getName() + "\".");
				monitor.worked(1);
				if(monitor.isCanceled())
					return;

				monitor.setTaskName("Creating temporal meta model...");
				this.temporalEcoreResource = ecoreResourceManager.createResourceToConvert(this.temporalProject, elementsToConvert.get(0).eResource());
				monitor.worked(1);
				if(monitor.isCanceled())
					return;

				monitor.setTaskName("Converting temporal meta model \"" + this.temporalEcoreResource.getURI().lastSegment() + "\"...");
				conversionExecuter.temporalize(this.temporalEcoreResource, elementsToConvert);
				monitor.worked(1);
				if(monitor.isCanceled())
					return;

				monitor.setTaskName("Saving temporal meta model \"" + this.temporalEcoreResource.getURI().lastSegment() + "\"...");
				EcoreUtil.saveResource(this.temporalEcoreResource);
				monitor.worked(1);
				if(monitor.isCanceled())
					return;

				monitor.setTaskName("Creating generation model for the temporal meta model \"" + this.temporalEcoreResource.getURI().lastSegment() + "\"...");
				genModelResourceManager.createGenModel(this.temporalProject.getName(), this.temporalEcoreResource);
				monitor.worked(1);
				if(monitor.isCanceled())
					return;

				monitor.setTaskName("Generating model code for the temporal meta model \"" + this.temporalEcoreResource.getURI().lastSegment() + "\"... (Cannot be interrupted)");
				genModelResourceManager.generateModelCode(monitor);
				monitor.worked(2);
				if(monitor.isCanceled())
					return;

				monitor.setTaskName("Generating adapter code for the temporal meta model \"" + this.temporalEcoreResource.getURI().lastSegment() + "\"...");
				CodeGenerator.instance.initialize(this.temporalProject, projectCreator.getGeneratedJavaProject(), this.originalEcoreFile.getProject(), genModelResourceManager.getDerivedGenModel(), genModelResourceManager.getOriginalGenModel());
				CodeGenerator.instance.generate();
				monitor.worked(5);
				if(monitor.isCanceled())
					return;

				SlicingProjectCreator slicingProjectCreator = new SlicingProjectCreator(this.originalEcoreFile.getProject());
				monitor.setTaskName("Creating Plugin Project \"" + slicingProjectCreator.getGeneratedProjectName() + "\"...");
				IProject slicingProject = slicingProjectCreator.createPluginProject();
				if(slicingProject == null)
					throw new RuntimeException("Could not create \"" + slicingProjectCreator.getGeneratedProjectName() + "\" Plugin Project for the converted temporal meta model to \"" + ((IFile) this.originalEcoreFile).getName() + "\".");
				slicingProjectCreator.createSlicingCode(
						CodeGenerator.instance.getTemporalGenModel().getGenPackages().get(0).getBasePackage(),
						CodeGenerator.instance.getQualifiedGatewayName(),
						CodeGenerator.instance.getNonTemporalGenModel().getGenPackages().get(0).getFileExtension()
						);
				monitor.worked(1);
				if(monitor.isCanceled())
					return;

				ViewerProjectCreator viewerProjectCreator = new ViewerProjectCreator(this.originalEcoreFile.getProject());
				monitor.setTaskName("Creating Plugin Project \"" + viewerProjectCreator.getGeneratedProjectName() + "\"...");
				IProject viewerProject = viewerProjectCreator.createPluginProject();
				if(viewerProject == null)
					throw new RuntimeException("Could not create \"" + viewerProjectCreator.getGeneratedProjectName() + "\" Plugin Project for the converted temporal meta model to \"" + ((IFile) this.originalEcoreFile).getName() + "\".");
				viewerProjectCreator.createViewerCode(
						CodeGenerator.instance.getTemporalGenModel().getGenPackages().get(0).getBasePackage(),
						CodeGenerator.instance.getQualifiedGatewayName(),
						CodeGenerator.instance.getNonTemporalGenModel().getGenPackages().get(0).getFileExtension()
						);
				monitor.worked(1);
				
			} catch(Exception e) {
				e.printStackTrace();
				
				setConversionFailed();
				Display.getDefault().asyncExec(() -> {
					String message = e.getMessage();
					if(message == null)
						message = "A " + e.getClass().getSimpleName() + " occured. Check the console log for more information.";
					
					message += "Conversion duration: " + getTimeDiffAsString(startTime) + " minutes.\n\n" + message;
					
			        DialogUtil.getInstance().openErrorDialog(message);
			    });
			}

			if(!conversionFailed) {
				Display.getDefault().asyncExec(() -> {
						DialogUtil.getInstance().openInfoDialog("Conversion completed successfully!\n\n"
																	+ "Conversion duration: " + getTimeDiffAsString(startTime) + " minutes.\n\n"
																	+ "Temporalized EClasses: " + this.eClassesTemporalized + "\n"
																	+ "Temporalized EAttributes: " + this.eAttributesTemporalized + "\n"
																	+ "Temporalized EReferences: " + this.eReferencesTemporalized);
				});
			}
			
			monitor.done();
		});
		
		job.setPriority(10);
		job.schedule();
	}
	
	
	
	private String getTimeDiffAsString(long startTime) {
		long timeDiff = System.currentTimeMillis() - startTime;
		
		int seconds = (int) Math.ceil((double) timeDiff / 1000.0d);
		int minutes = (int) Math.floor((double) seconds / 60.0d);
		seconds -= minutes * 60;

		String minuteString = (minutes < 10 ? "0" : "") + minutes;
		String secondString = (seconds < 10 ? "0" : "") + seconds;
		
		return minuteString + ":" + secondString;
	}

	public void notifyTemporalizationOfEReference() {
		this.eReferencesTemporalized ++;
	}

	public void notifyTemporalizationOfEAttribute() {
		this.eAttributesTemporalized ++;
	}

	public void notifyTemporalizationOfEClass() {
		this.eClassesTemporalized ++;
	}
	
	public void setConversionFailed() {
		conversionFailed = true;
	}

}
























