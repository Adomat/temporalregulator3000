package de.tu_bs.cs.isf.temporalregulator3000.converter.resources;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;


public class EcoreResourceManager {
	
	private String METAMODELFOLDER = "model";
	private Set<EOperation> operations = new HashSet<>();
	
	
	
	public Resource createResourceToConvert(IProject destinationProject, Resource originalResource) throws CoreException, IOException {
		String originalFileName = originalResource.getURI().lastSegment();
		String newFileName = originalFileName.split("\\.")[0] + "_temporal" + "." + originalFileName.split("\\.")[1];
		
		IFile destinationFile = getDestinationFile(destinationProject, newFileName); 
		
		// Create Fresh / Overwrite
		URI convertedResouceURI = URI.createURI("platform:/resource/" + destinationProject.getName() + "/" + destinationFile.getProjectRelativePath());
		
		Resource convertedResouce = originalResource.getResourceSet().createResource(convertedResouceURI);
		convertedResouce.getContents().addAll(EcoreUtil.copyAll(originalResource.getContents()));

		// So far, we did not even think about operations. The temporal model will therefore not even contain them.
		{
			for(EObject root : convertedResouce.getContents())
				collectOperations(root);
			
			for(EOperation op : operations)
				EcoreUtil.delete(op);
			operations.clear();
		}
		
		return convertedResouce;
	}
	
	
	
	private IFile getDestinationFile(IProject project, String fileName) throws CoreException {
		IFolder metamodelFolder = project.getFolder(this.METAMODELFOLDER);
		if(!metamodelFolder.exists())
			metamodelFolder.delete(true, null);
		metamodelFolder.create(true, true, null);
		
		return metamodelFolder.getFile(fileName);
	}
	
	
	
	private void collectOperations(EObject object) {
		for(EObject child : object.eContents()) {
			if(child instanceof EOperation)
				operations.add((EOperation) child);
			
			else collectOperations(child);
		}
	}

}
















