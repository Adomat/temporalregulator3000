package de.tu_bs.cs.isf.temporalregulator3000.converter.execution;

import static de.tu_bs.cs.isf.temporalregulator3000.converter.execution.utility.ConversionUtil.findElementsToConvert;
import static de.tu_bs.cs.isf.temporalregulator3000.converter.execution.utility.ConversionUtil.getTemporalCoreClass;
import static de.tu_bs.cs.isf.temporalregulator3000.core.EcoreUtil.buildCamelCasedFeatureName;
import static de.tu_bs.cs.isf.temporalregulator3000.core.EcoreUtil.correctNameSpaces;
import static de.tu_bs.cs.isf.temporalregulator3000.core.EcoreUtil.createEClass;
import static de.tu_bs.cs.isf.temporalregulator3000.core.EcoreUtil.createEGenericType;
import static de.tu_bs.cs.isf.temporalregulator3000.core.EcoreUtil.createEReference;
import static de.tu_bs.cs.isf.temporalregulator3000.core.EcoreUtil.createTemporalUpperBoundOCLConstraint;
import static de.tu_bs.cs.isf.temporalregulator3000.core.StringUtil.firstLetterToUpper;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;

import de.tu_bs.cs.isf.temporalregulator3000.converter.ModelConverter;

public class EcoreModelTemporalizer {
	
	private ModelConverter callingConverter;
	private Set<EStructuralFeature> convertedFeatures = new HashSet<>();

	
	
	public EcoreModelTemporalizer(ModelConverter modelConverter) {
		this.callingConverter = modelConverter;
	}



	public void temporalize(Resource temporalResource, List<EObject> referenceElementsToConvert) throws IOException {
		if(temporalResource.getContents().size() == 0)
			throw new IOException("The selected meta model is empty.");
		
		Set<EObject> elementsToConvert = findElementsToConvert(temporalResource, referenceElementsToConvert);
		if(elementsToConvert == null)
			throw new IOException("Converting selected meta model elements failed: At least one element could not be resolved.");
		if(elementsToConvert.size() == 0)
			throw new IOException("No elements selected for conversion.");

		correctNameSpaces(temporalResource);
//		removeIdFlagsFromAttributes(temporalResource);
		
		for(EObject elementToConvert : elementsToConvert) {
			if(this.convertedFeatures.contains(elementToConvert))
				continue;
			
			if(elementToConvert instanceof EClass)
				convert((EClass) elementToConvert);
				
			else if(elementToConvert instanceof EStructuralFeature)
				convert((EStructuralFeature) elementToConvert, true);
		}
	}
	
	

	private void convert(EClass eClass) {
		// Add temporal element as super class, wow...
		eClass.getESuperTypes().add(getTemporalCoreClass("TemporalElement"));
		
		callingConverter.notifyTemporalizationOfEClass();
	}
	
	
	
	private void convert(EStructuralFeature featureToConvert, boolean primaryCall) {
		// Check if the reference has a containment opposite and convert it instead...
		if(featureToConvert instanceof EReference && primaryCall) {
			if(((EReference) featureToConvert).getEOpposite() != null) {
				if(((EReference) featureToConvert).getEOpposite().isContainment()) {
					convert(((EReference) featureToConvert).getEOpposite(), true);
					return;
				}
			}
		}
		
		EClass originalFeatureToConvertContainment = featureToConvert.getEContainingClass();

		if(featureToConvert.getUpperBound() != 1 && featureToConvert.isOrdered()) {
			// Use a temporally sorted list to replace the association
			String name = "temporal" + firstLetterToUpper(featureToConvert.getName()) + "Replacement";
			
			EReference replacingReference = createEReference(name, getTemporalCoreClass("TemporallySortedList"), 1, 1);
			replacingReference.getEGenericType().getETypeArguments().add(createEGenericType(featureToConvert.getEType()));
			replacingReference.setContainment(true);

			featureToConvert.getEContainingClass().getEStructuralFeatures().add(replacingReference);
			
			if(featureToConvert instanceof EReference) {
				// eOpposite handling
				EReference opposite = ((EReference) featureToConvert).getEOpposite();
				if(opposite != null && primaryCall)
					convert(opposite, false);
				
				// if the reference to convert is a containment reference, move its containment
				if(((EReference) featureToConvert).isContainment())
					handleContainment((EReference) featureToConvert, "TemporallySortedListContent" + firstLetterToUpper(buildCamelCasedFeatureName(featureToConvert)), featureToConvert.getEContainingClass());
			}

			org.eclipse.emf.ecore.util.EcoreUtil.delete(featureToConvert);
		}
		else {
			// Build a regular association replacement
			// First, create a temporal EClass that will replace the non-temporal Attribute or Reference (a.k.a. Structural Feature)
			EClass temporalAssociationEClass = createTemporalAssociationEClass(featureToConvert);
			// Then, relocate the the original non-temporal Structural Feature and replace it with a reference to the freshly created Association EClass
			EReference replacingReferenceToTemporalAssociation = relocateAndReplaceFeature(temporalAssociationEClass, featureToConvert);
			
			if(featureToConvert instanceof EReference) {
				// eOpposite handling
				if(((EReference) featureToConvert).getEOpposite() != null) {
					if(primaryCall)
						convert(((EReference) featureToConvert).getEOpposite(), false);
//						relocateAndReplaceOppositeReferences((EReference) featureToConvert, temporalAssociationEClass, replacingReferenceToTemporalAssociation);
					
					((EReference) featureToConvert).setEOpposite(null);
				}
				
				// if the reference to convert is a containment reference, move its containment
				if(((EReference) featureToConvert).isContainment())
					handleContainment((EReference) featureToConvert, temporalAssociationEClass.getName(), originalFeatureToConvertContainment);
				
				((EReference) featureToConvert).setContainment(false);
			}
		}

		if(featureToConvert instanceof EReference)
			callingConverter.notifyTemporalizationOfEReference();
		else if(featureToConvert instanceof EAttribute)
			callingConverter.notifyTemporalizationOfEAttribute();
	}
	
	

	private EClass createTemporalAssociationEClass(EStructuralFeature originalFeature) {
		String name = "TemporalAssociation" + buildCamelCasedFeatureName(originalFeature);
		EClass eClass = createEClass(name);
		eClass.getESuperTypes().add(getTemporalCoreClass("TemporalElement"));
		
		originalFeature.getEContainingClass().getEPackage().getEClassifiers().add(eClass);
		return eClass;
	}

	private EReference relocateAndReplaceFeature(EClass temporalAssociationEClass, EStructuralFeature originalFeature) {
		String name = "temporal" + firstLetterToUpper(originalFeature.getName()) + "Replacement";
		EReference replacingReference = createEReference(name, temporalAssociationEClass, originalFeature.getLowerBound(), -1);
		replacingReference.setContainment(true);
		
		originalFeature.getEContainingClass().getEStructuralFeatures().add(replacingReference);
		
		// Create constraints
		if (originalFeature.getUpperBound() != -1) {
			createTemporalUpperBoundOCLConstraint(originalFeature.getEContainingClass(), replacingReference.getName(), originalFeature.getUpperBound());
		}
		
		//Modify and relocate the original structural feature.
		originalFeature.setLowerBound(1);
		originalFeature.setUpperBound(1);
		temporalAssociationEClass.getEStructuralFeatures().add(originalFeature);
		
		return replacingReference;
	}

	private void relocateAndReplaceOppositeReferences(EReference reference, EClass temporalAssociationEClass, EReference replacingReferenceToTemporalAssociation) {
		EReference oppositeFeatureToConvert = reference.getEOpposite();
		String replacingOppositeReferenceToProxyName = "opposite" + firstLetterToUpper(replacingReferenceToTemporalAssociation.getName());
		EReference replacingOppositeReferenceToProxy = createEReference(replacingOppositeReferenceToProxyName, temporalAssociationEClass, oppositeFeatureToConvert.getLowerBound(), -1);
		replacingOppositeReferenceToProxy.setEOpposite(reference);
		reference.getEReferenceType().getEStructuralFeatures().add(replacingOppositeReferenceToProxy);
		
		if (oppositeFeatureToConvert.getUpperBound() != -1) {
			createTemporalUpperBoundOCLConstraint(reference.getEReferenceType(), replacingOppositeReferenceToProxyName, oppositeFeatureToConvert.getUpperBound());
		}
		
		oppositeFeatureToConvert.setLowerBound(1);
		oppositeFeatureToConvert.setUpperBound(1);
		oppositeFeatureToConvert.setEOpposite(replacingReferenceToTemporalAssociation);
		temporalAssociationEClass.getEStructuralFeatures().add(oppositeFeatureToConvert);
		
		reference.setEOpposite(replacingOppositeReferenceToProxy);
		replacingReferenceToTemporalAssociation.setEOpposite(oppositeFeatureToConvert);
	}

	private void handleContainment(EReference reference, String relocatedFeatureName, EClass containerClass) {
		EReference newReference = createEReference("relocated" + firstLetterToUpper(relocatedFeatureName), reference.getEReferenceType(), 0, -1);
		newReference.setContainment(true);
		containerClass.getEStructuralFeatures().add(newReference);
	}
	
}



















