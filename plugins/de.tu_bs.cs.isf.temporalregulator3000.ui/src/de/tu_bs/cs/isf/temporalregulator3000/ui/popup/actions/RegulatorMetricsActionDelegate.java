package de.tu_bs.cs.isf.temporalregulator3000.ui.popup.actions;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.PackageNotFoundException;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.internal.ObjectPluginAction;
import org.osgi.framework.namespace.PackageNamespace;
import org.xml.sax.SAXParseException;

public class RegulatorMetricsActionDelegate implements IObjectActionDelegate {

	private Shell shell;
	
	/**
	 * Constructor for Action1.
	 */
	public RegulatorMetricsActionDelegate() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		if(action instanceof ObjectPluginAction) {
			@SuppressWarnings("restriction")
			ISelection selection = ((ObjectPluginAction) action).getSelection();
			
			if(selection instanceof TreeSelection) {
				TreePath treePath = ((TreeSelection) selection).getPaths()[0];
				Object file = treePath.getSegment(treePath.getSegmentCount()-1);

				if(file instanceof IFile) {
					handleIFile((IFile) file);
				}
				else if(file instanceof IFolder) {
					handleIFolder((IFolder) file);
				}
			}
		}
	}

	private void handleIFolder(IFolder iFolder) {
		try {
			for(IResource member : iFolder.members()) {
				if(member instanceof IFile) {
					// TODO
					MessageDialog dialog = new MessageDialog(shell, "Regulator Metrics", null, "Folders cannot be analysed yet...", MessageDialog.ERROR, new String [] { "Oh, okay... :(" }, 0);
					dialog.open();
					return;
				}
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	Map<String, Integer> elementCountMap;
	int referenceCount;
	
	private void handleIFile(IFile iFile) {
		elementCountMap = new HashMap<>();
		referenceCount = 0;
		
		try {
			getStatisticsForIFile(iFile);
		} catch (Exception e) {
			e.printStackTrace();
			String message = e.getMessage();
			if(e.getCause() instanceof PackageNotFoundException || e.getCause() instanceof SAXParseException)
				message = "Statistics are only avaible for files that contain registered Ecore Packages.";
			
			MessageDialog dialog = new MessageDialog(shell, "Regulator Metrics for \"" + iFile.getName() + "\"", null, message, MessageDialog.ERROR, new String [] { "Upsy Daisy..." }, 0);
			dialog.open();
			return;
		}
		
		int totalElementCount = 0;
		String message = "Following entities were found in \"" + iFile.getName() + "\":" + System.lineSeparator() + System.lineSeparator();
		for(String className : elementCountMap.keySet()) {
			int elementCount = elementCountMap.get(className);
			message += elementCount + "\t" + className + System.lineSeparator();
			
			totalElementCount += elementCount;
		}
		
		message += System.lineSeparator() + "Total number of entities: " + totalElementCount;
		
		message += System.lineSeparator() + "Total number of references: " + referenceCount;
		
		MessageDialog dialog = new MessageDialog(shell, "Regulator Metrics for \"" + iFile.getName() + "\"", null, message, MessageDialog.INFORMATION, new String [] { "Close", "Copy to Clipboard" }, 0);
		
		if(dialog.open() == 1) {
			StringSelection stringSelection = new StringSelection(message);
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipboard.setContents(stringSelection, null);
		}
	}
	
	

	private void getStatisticsForIFile(IFile iFile) throws Exception {
		Resource loadedRes = (new ResourceSetImpl()).getResource(URI.createURI("platform:/resource/" + iFile.getProject().getName() + "/" + iFile.getProjectRelativePath().toString()), true);
		loadedRes.load(Collections.emptyMap());

		for(EObject root : loadedRes.getContents()) {
			countContents(root);
		}
	}
	
	

	private void countContents(EObject object) {
//		referenceCount += object.eCrossReferences().size();
//		EcoreUtil.UsageCrossReferencer.find(object, object.eResource());
		
		String className = object.getClass().getName();
		Integer occurence = elementCountMap.get(className);
		if(occurence == null)
			elementCountMap.put(className, 1);
		else
			elementCountMap.put(className, occurence+1);
		
//		for (Field field : object.getClass().getDeclaredFields()) {
//			if(!field.getName().endsWith("_EDEFAULT")) {
//				boolean isField = field.getType().isPrimitive();
//				if(field.getType().getName().equals("java.lang.String"))
//					isField = true;
//				
//				System.out.println(isField + "\t- " + field.getName() + " [" + field.getType() + "]");
//			}
//		}
		
		for(EObject content : object.eContents()) {
			countContents(content);
		}
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

}
