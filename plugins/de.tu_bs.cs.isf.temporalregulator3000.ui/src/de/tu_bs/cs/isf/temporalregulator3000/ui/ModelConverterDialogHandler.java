package de.tu_bs.cs.isf.temporalregulator3000.ui;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.swt.widgets.Shell;

import de.tu_bs.cs.isf.temporalregulator3000.converter.ModelConverter;
import de.tu_bs.cs.isf.temporalregulator3000.core.DialogUtil;
import de.tu_bs.cs.isf.temporalregulator3000.ui.treeview.LabelProviderCustom;
import de.tu_bs.cs.isf.temporalregulator3000.ui.treeview.TreeContentProviderImplCustom;


public class ModelConverterDialogHandler {
	private Shell shell;
	private ModelConverter converter;
	private EObject resourceRootElement;
	
	private List<EObject> elementsToConvert;
	private String windowTitle;

	public ModelConverterDialogHandler(Shell shell) {
		DialogUtil.initialize(shell);
		this.elementsToConvert = new ArrayList<>();
	}

	public void setConverter(ModelConverter modelConverter) {
		this.converter = modelConverter;
	}

	public void addElement(EObject newElement) {
		if(newElement instanceof EClass || newElement instanceof EStructuralFeature)
			elementsToConvert.add(newElement);
	}

	public void openSelectionDialog(IFile file) {
	    this.windowTitle = "Creating temporal version of '" + file.getName() + "'";
	    
		Resource sourceResource;
		try {
			sourceResource = parseFile(file);
			this.resourceRootElement = sourceResource.getContents().get(0);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
	    // http://blog.eclipse-tips.com/2008/07/selection-dialogs-in-eclipse.html
	    EcoreModelTreeSelectionDialog dialog = new EcoreModelTreeSelectionDialog(shell, new LabelProviderCustom(), new TreeContentProviderImplCustom());
		dialog.setTitle(windowTitle);
		dialog.setMessage("Choose meta classes to become temporal entities:");
		dialog.setInput(this.resourceRootElement);
		dialog.setHandler(this);
		dialog.open();
	}

	public void convertAndSave() {
		converter.convert(elementsToConvert);
	}

	private Resource parseFile(IFile file) throws IOException {
//		String contentString = "";
//
//		try (BufferedReader br = new BufferedReader(new FileReader(file.getLocation().toString()))) {
//			StringBuilder sb = new StringBuilder();
//			String line = br.readLine();
//
//			while (line != null) {
//				sb.append(line);
//				sb.append(System.lineSeparator());
//				line = br.readLine();
//			}
//
//			contentString = sb.toString();
//			br.close();
//		}
//
//		ByteArrayInputStream byteStream = new ByteArrayInputStream(contentString.getBytes());

		URI resourceURI = URI.createURI("platform:/resource/" + file.getProject().getName() + "/" + file.getProjectRelativePath());
		
		ResourceSet resourceSet = new ResourceSetImpl();
		
		Resource resource = resourceSet.getResource(resourceURI, true);
		
		resource.load(Collections.EMPTY_MAP);
		
//		Resource resource = (new ResourceSetImpl()).createResource(resourceURI);
//		resource.load(byteStream, Collections.emptyMap());
//
//		byteStream.close();
		return resource;
	}
}
