package de.tu_bs.cs.isf.temporalregulator3000.ui.treeview;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IToolTipProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;

public class LabelProviderCustom extends LabelProvider implements IToolTipProvider {
	Map<String, Image> imageMap = new HashMap<>();

	public LabelProviderCustom() {
		loadImageToMap("EPackage", "icons/full/obj16/EPackage.gif");
		loadImageToMap("EClass", "icons/full/obj16/EClass.gif");
		loadImageToMap("EEnum", "icons/full/obj16/EEnum.gif");
		loadImageToMap("EAttribute", "icons/full/obj16/EAttribute.gif");
		loadImageToMap("EReference", "icons/full/obj16/EReference.gif");
	}

	private void loadImageToMap(String key, String path) {
		Bundle bundle = Platform.getBundle("org.eclipse.emf.ecore.edit");
		URL fullPathString = FileLocator.find(bundle, new Path(path), null);
		ImageDescriptor imageDesc = ImageDescriptor.createFromURL(fullPathString);

		imageMap.put(key, imageDesc.createImage());
	}

	@Override
	public String getText(Object element) {
		if (element instanceof EPackage) {
			return ((EPackage) element).getName();
		}
		if (element instanceof EClass) {
			String heritageString = "";

			if (((EClass) element).getESuperTypes().size() > 0) {
				heritageString = " -> ";
				for (EClass superType : ((EClass) element).getESuperTypes())
					heritageString += superType.getName() + ", ";
				heritageString = heritageString.substring(0, heritageString.length() - 2);
			}

			return ((EClass) element).getName() + heritageString;
		}
		if (element instanceof EClassifier) {
			return ((EClassifier) element).getName();
		}
		if (element instanceof EStructuralFeature) {
			EStructuralFeature feature = (EStructuralFeature) element;
			String label = feature.getName() + " : " + feature.getEType().getName();
			
			if(!feature.isChangeable() || feature.isDerived()) {
				label += " [";
				label += !feature.isChangeable() ? "UNCHANGEABLE, ": "";
				label += feature.isDerived() ? "DERIVED, ": "";
				label = label.substring(0, label.length()-2);
				label += "]";
			}
			return label;
		}
		return element.toString();
	}

	@Override
	public final Image getImage(Object element) {
		if (element instanceof EPackage)
			return imageMap.get("EPackage");
		else if (element instanceof EClass)
			return imageMap.get("EClass");
		else if (element instanceof EEnum)
			return imageMap.get("EEnum");
		else if (element instanceof EAttribute)
			return imageMap.get("EAttribute");
		else if (element instanceof EReference)
			return imageMap.get("EReference");
		else
			return null;
	}

	@Override
	public String getToolTipText(Object element) {
		return "My nice tooltip: " + element.toString();
	}

}
