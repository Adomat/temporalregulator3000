package org.eltaecore.feature.metrics;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.deltaecore.temporal.FeatureEvolutionGateway;
import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.PackageNotFoundException;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.internal.ObjectPluginAction;
import org.xml.sax.SAXParseException;

import de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporalElement;
import de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporallySortedList;
import de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporallySortedListAssociation;
import de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporallySortedListNode;

public class ActionDelegate implements IObjectActionDelegate {

	private Shell shell;
	
	/**
	 * Constructor for Action1.
	 */
	public ActionDelegate() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		if(action instanceof ObjectPluginAction) {
			@SuppressWarnings("restriction")
			ISelection selection = ((ObjectPluginAction) action).getSelection();
			
			if(selection instanceof TreeSelection) {
				TreePath treePath = ((TreeSelection) selection).getPaths()[0];
				Object file = treePath.getSegment(treePath.getSegmentCount()-1);

				if(file instanceof IFile) {
					handleIFile((IFile) file);
				}
			}
		}
	}

	Map<String, Integer> elementCountMap;
	int referenceCount, attributeCount;
	
	private void handleIFile(IFile iFile) {
		elementCountMap = new HashMap<>();
		referenceCount = 0;
		attributeCount = 0;
		
		try {
			getStatisticsForIFile(iFile);
		} catch (Exception e) {
			e.printStackTrace();
			String message = e.getMessage();
			if(e.getCause() instanceof PackageNotFoundException || e.getCause() instanceof SAXParseException)
				message = "Statistics are only avaible for files that contain registered Ecore Packages.";
			
			MessageDialog dialog = new MessageDialog(shell, "Regulator Metrics for \"" + iFile.getName() + "\"", null, message, MessageDialog.ERROR, new String [] { "Upsy Daisy..." }, 0);
			dialog.open();
			return;
		}
		
		int totalElementCount = 0;
		String message = "Following entities were found in \"" + iFile.getName() + "\":" + System.lineSeparator() + System.lineSeparator();
		for(String className : elementCountMap.keySet()) {
			int elementCount = elementCountMap.get(className);
			message += elementCount + "\t" + className + System.lineSeparator();
			
			totalElementCount += elementCount;
		}
		
		message += System.lineSeparator() + "Total number of entities: " + totalElementCount;

		message += System.lineSeparator() + "Total number of references: " + referenceCount;
		message += System.lineSeparator() + "Total number of attributes: " + attributeCount;
		
		MessageDialog dialog = new MessageDialog(shell, "Regulator Metrics for \"" + iFile.getName() + "\"", null, message, MessageDialog.INFORMATION, new String [] { "Close", "Copy to Clipboard" }, 0);
		
		if(dialog.open() == 1) {
			StringSelection stringSelection = new StringSelection(message);
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipboard.setContents(stringSelection, null);
		}
	}
	
	

	private void getStatisticsForIFile(IFile iFile) throws Exception {
		Resource loadedRes = (new ResourceSetImpl()).getResource(URI.createURI("platform:/resource/" + iFile.getProject().getName() + "/" + iFile.getProjectRelativePath().toString()), true);
		loadedRes.load(Collections.emptyMap());

		for(EObject root : loadedRes.getContents()) {
			countContents(root);
		}
	}
	
	

	private void countContents(EObject object) {
		object = FeatureEvolutionGateway.instance.getTemporalObjectFromCacheMap(object);
		
		// Non Temporal
		
		if(object instanceof org.deltaecore.feature.DEFeatureModel) {
			referenceCount += ((org.deltaecore.feature.DEFeatureModel) object).getRootFeature() != null ? 1 : 0;
		}
		if(object instanceof org.deltaecore.feature.DEFeature) {
			attributeCount += ((org.deltaecore.feature.DEFeature) object).getName() != null ? 1 : 0;
			referenceCount += ((org.deltaecore.feature.DEFeature) object).getGroups().size();
			referenceCount += ((org.deltaecore.feature.DEFeature) object).getParentOfFeature() != null ? 1 : 0;
		}
		if(object instanceof org.deltaecore.feature.DEGroup) {
			referenceCount += ((org.deltaecore.feature.DEGroup) object).getParentOfGroup() != null ? 1 : 0;
			referenceCount += ((org.deltaecore.feature.DEGroup) object).getFeatures().size();
		}
		if(object instanceof org.deltaecore.feature.DECardinalityBasedElement) {
			attributeCount += 2;
		}
		
		// Temporal
		
		if(object instanceof TemporalElement) {
			attributeCount += ((TemporalElement) object).getTemporalRegulatorID() != null ? 1 : 0;
			attributeCount += ((TemporalElement) object).getTemporalRegulatorValidSince() != null ? 1 : 0;
			attributeCount += ((TemporalElement) object).getTemporalRegulatorValidUntil() != null ? 1 : 0;
		}
		if(object instanceof org.deltaecore.temporal.feature.DEFeatureModel) {
			referenceCount += ((org.deltaecore.temporal.feature.DEFeatureModel) object).getRelocatedTemporalAssociationFeatureDEFeatureModelRootFeature().size();
			referenceCount += ((org.deltaecore.temporal.feature.DEFeatureModel) object).getTemporalRootFeatureReplacement().size();
		}
		if(object instanceof org.deltaecore.temporal.feature.DEFeature) {
			referenceCount += ((org.deltaecore.temporal.feature.DEFeature) object).getRelocatedTemporallySortedListContentFeatureDEFeatureGroups().size();
			referenceCount += ((org.deltaecore.temporal.feature.DEFeature) object).getTemporalGroupsReplacement() != null ? 1 : 0;
			referenceCount += ((org.deltaecore.temporal.feature.DEFeature) object).getTemporalNameReplacement().size();
			referenceCount += ((org.deltaecore.temporal.feature.DEFeature) object).getTemporalParentOfFeatureReplacement().size();
		}
		if(object instanceof org.deltaecore.temporal.feature.DEGroup) {
			referenceCount += ((org.deltaecore.temporal.feature.DEGroup) object).getRelocatedTemporallySortedListContentFeatureDEGroupFeatures().size();
			referenceCount += ((org.deltaecore.temporal.feature.DEGroup) object).getTemporalFeaturesReplacement() != null ? 1 : 0;
			referenceCount += ((org.deltaecore.temporal.feature.DEGroup) object).getTemporalParentOfGroupReplacement().size();
		}
		if(object instanceof org.deltaecore.temporal.feature.DECardinalityBasedElement) {
			referenceCount += ((org.deltaecore.temporal.feature.DECardinalityBasedElement) object).getTemporalMaxCardinalityReplacement().size();
			referenceCount += ((org.deltaecore.temporal.feature.DECardinalityBasedElement) object).getTemporalMinCardinalityReplacement().size();
		}
		if(object instanceof org.deltaecore.temporal.feature.TemporalAssociationFeatureDECardinalityBasedElementMaxCardinality) {
			attributeCount += 1;
		}
		if(object instanceof org.deltaecore.temporal.feature.TemporalAssociationFeatureDECardinalityBasedElementMinCardinality) {
			attributeCount += 1;
		}
		if(object instanceof org.deltaecore.temporal.feature.TemporalAssociationFeatureDEFeatureModelRootFeature) {
			referenceCount += ((org.deltaecore.temporal.feature.TemporalAssociationFeatureDEFeatureModelRootFeature) object).getRootFeature() != null ? 1 : 0;
		}
		if(object instanceof org.deltaecore.temporal.feature.TemporalAssociationFeatureDEFeatureName) {
			attributeCount += ((org.deltaecore.temporal.feature.TemporalAssociationFeatureDEFeatureName) object).getName() != null ? 1 : 0;
		}
		if(object instanceof org.deltaecore.temporal.feature.TemporalAssociationFeatureDEFeatureParentOfFeature) {
			referenceCount += ((org.deltaecore.temporal.feature.TemporalAssociationFeatureDEFeatureParentOfFeature) object).getParentOfFeature() != null ? 1 : 0;
		}
		if(object instanceof org.deltaecore.temporal.feature.TemporalAssociationFeatureDEGroupParentOfGroup) {
			referenceCount += ((org.deltaecore.temporal.feature.TemporalAssociationFeatureDEGroupParentOfGroup) object).getParentOfGroup() != null ? 1 : 0;
		}
		if(object instanceof TemporallySortedList) {
			referenceCount += ((TemporallySortedList<?>) object).getAllNodes().size();
			referenceCount += ((TemporallySortedList<?>) object).getRoot().size();
		}
		if(object instanceof TemporallySortedListAssociation) {
			referenceCount += ((TemporallySortedListAssociation<?>) object).getTarget() != null ? 1 : 0;
		}
		if(object instanceof TemporallySortedListNode) {
			referenceCount += ((TemporallySortedListNode<?>) object).getElement() != null ? 1 : 0;
			referenceCount += ((TemporallySortedListNode<?>) object).getSuccessor().size();
		}
		
		String className = object.getClass().getName();
		Integer occurence = elementCountMap.get(className);
		if(occurence == null)
			elementCountMap.put(className, 1);
		else
			elementCountMap.put(className, occurence+1);
		
		for(EObject content : object.eContents()) {
			countContents(content);
		}
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

}
