package de.tu_bs.cs.isf.temporalregulator3000.ui.popup.actions;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.deltaecore.feature.DEFeature;
import org.deltaecore.feature.DEFeatureFactory;
import org.deltaecore.feature.DEFeatureModel;
import org.deltaecore.feature.DEGroup;
import org.deltaecore.feature.impl.DEFeatureFactoryImpl;
import org.deltaecore.temporal.FeatureAdapterElementDifferenceCalculator;
import org.deltaecore.temporal.FeatureAdapterElementInitializer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.internal.ObjectPluginAction;

import de.tu_bs.cs.isf.temporalregulator3000.core.ClockMechanism;
import de.tu_bs.cs.isf.temporalregulator3000.model.util.DateResolverUtil;

public class SnapshotActionDelegate implements IActionDelegate {

	private Shell shell;
	
	private Date currentTime;
	
	private IProject project;
	private ResourceSet resourceSet;
	private Resource[] resourcesToConvert;

	
	
	/**
	 * Constructor for Action1.
	 */
	public SnapshotActionDelegate() {
		super();
		currentTime = DateResolverUtil.resolveDate("2017-05-22T12:00:00");
		changeTime(0);
	}
	
	

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	
	
	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		if(action instanceof ObjectPluginAction) {
			@SuppressWarnings("restriction")
			ISelection selection = ((ObjectPluginAction) action).getSelection();
			
			if(selection instanceof TreeSelection) {
				TreePath[] allTreePaths = ((TreeSelection) selection).getPaths();
//				if(allTreePaths.length == 1 && allTreePaths[0].getSegment(allTreePaths[0].getSegmentCount()-1) instanceof IFolder) {
//					try {
//						changeFileExtensionOfAllXMIFiles((IFolder) allTreePaths[0].getSegment(allTreePaths[0].getSegmentCount()-1), "behavior");
//					} catch (CoreException e) {
//						e.printStackTrace();
//					}
//					return;
//				}
				
				String resultMessage;
				try {
					resultMessage = loadResources(allTreePaths);
					if(resultMessage != null) {
						MessageDialog dialog = new MessageDialog(shell, "Snapshot Converter - Loading Models failed", null, resultMessage, MessageDialog.ERROR, new String [] { "Oohhh... :(" }, 0);
						dialog.open();
					}
				} catch (CoreException e) {
					e.printStackTrace();
				}
				
				resultMessage = createTemporalDeltaEcoreFeatureResource();
				if(resultMessage != null) {
					MessageDialog dialog = new MessageDialog(shell, "Snapshot Converter - Creating Temporal Resource failed", null, resultMessage, MessageDialog.ERROR, new String [] { "Oohhh... :(" }, 0);
					dialog.open();
				}
			}
		}
	}
	
	
	
	private void changeFileExtensionOfAllXMIFiles(IFolder sourceFolder, String newFileExtension) throws CoreException {
		IFolder destinationFolder = sourceFolder.getParent().getFolder(new Path(sourceFolder.getName() + "_" + newFileExtension));
		if(destinationFolder.exists())
			destinationFolder.delete(true, null);
		destinationFolder.create(true, true, null);
		
		for(IResource member : sourceFolder.members()) {
			member.copy(destinationFolder.getFullPath().append(member.getName().replaceAll("\\.xmi", "." + newFileExtension)), true, null);
		}
	}



	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}
	
	
	
	private String loadResources(TreePath[] allTreePaths) throws CoreException {
		resourcesToConvert = new Resource[allTreePaths.length];
		resourceSet = new ResourceSetImpl();
		
		int index = 0;
		
		for(TreePath treePath : allTreePaths) {
			Object file = treePath.getSegment(treePath.getSegmentCount()-1);
			
			if(project == null)
				project = ((IResource) file).getProject();
			
			if(file instanceof IFile) {
				IFile iFile = (IFile) file;
				
				URI uri = URI.createURI("platform:/resource/" + iFile.getProject().getName() + "/" + iFile.getProjectRelativePath().toString());
				Resource loadedRes = resourceSet.getResource(uri, true);
				
//				index = Integer.parseInt(iFile.getName().substring(10, 11));
//				if(resourcesToConvert[index] != null)
//					return "Please name your input files correctly!";
				
				resourcesToConvert[index++] = loadedRes;
			}
			
			if(file instanceof IFolder) {
				IFolder iFolder = (IFolder) file;
				IResource[] members = iFolder.members();

				int[] order = new int[] {0, 17, 8, 1, 15, 14, 4};
				order = new int[] {0, 17, 8};
				resourcesToConvert = new Resource[order.length];
				
				for(int i=0; i<order.length; i++) {
					IFile iFile = (IFile) members[order[i]];
					
					URI uri = URI.createURI("platform:/resource/" + iFile.getProject().getName() + "/" + iFile.getProjectRelativePath().toString());
					Resource loadedRes = resourceSet.getResource(uri, true);
					
					resourcesToConvert[i] = loadedRes;
				}
				
				return null;
			}
		}
		
		return null;
	}
	
	
	
	private String createTemporalDeltaEcoreFeatureResource() {
		FeatureAdapterElementDifferenceCalculator.instance.setComparator(new FeatureAdapterElementDifferenceCalculator.Comparator() {
			@Override
			public boolean equal(Object e1, Object e2) {
				if (e1 == null || e2 == null)
					return false;
				
				if(e1 instanceof org.deltaecore.feature.DEFeature && e2 instanceof org.deltaecore.feature.DEFeature)
					return ((org.deltaecore.feature.DEFeature) e1).getName().equals(((org.deltaecore.feature.DEFeature) e2).getName());
				
				if(e1 instanceof org.deltaecore.feature.DEGroup && e2 instanceof org.deltaecore.feature.DEGroup) {
					for(org.deltaecore.feature.DEFeature e1Feature : ((org.deltaecore.feature.DEGroup) e1).getFeatures()) {
						for(org.deltaecore.feature.DEFeature e2Feature : ((org.deltaecore.feature.DEGroup) e2).getFeatures()) {
							if(this.equal(e1Feature, e2Feature))
								return true;
						}
					}
					return false;
				}
				
				return e1.equals(e2);
			}
		});
		
		// Create a combined temporal model from selected original models

		int[] hoursBetweenSnapshots = new int[] { 0, 24*129, 24*22, 24*31, 24*32, 24*32, 24*28, 24*34, 24*28, 24*16 };
		
		currentTime = DateResolverUtil.resolveDate("2017-05-22T12:00:00");
		changeTime(0);
		System.out.println("Starting to combine to one temporal model at " + currentTime.toString());
		
		DEFeatureModel combinedFeatureModel = null;
		
		for(int index=0; index<resourcesToConvert.length; index++) {
			Resource res = resourcesToConvert[index];
			if(res.getContents().size() != 1 || !(res.getContents().get(0) instanceof DEFeatureModel))
				return "\"" + res.getURI() + "\" does not contain exactly one Feature Model...";

			if(combinedFeatureModel == null) {
				combinedFeatureModel = FeatureAdapterElementInitializer.instance.createAdapterElementFor((DEFeatureModel) res.getContents().get(0));
			}
			else {
				changeTime(hoursBetweenSnapshots[index]);
				FeatureAdapterElementDifferenceCalculator.instance.applyDifferences(combinedFeatureModel, (DEFeatureModel) res.getContents().get(0));
			}
		}
		
		Resource temporalResource = resourceSet.createResource(URI.createURI(resourcesToConvert[0].getURI().toString().substring(0, 26) + "Combined.defeaturemodel"));
		temporalResource.getContents().add(combinedFeatureModel);
		
		try {
			temporalResource.save(null);
		} catch (IOException e) {
			return "Storing of the temporal resource failed: " + e.getMessage();
		}
		
		// Split temporal model into several original models again
		
		currentTime = DateResolverUtil.resolveDate("2017-05-22T12:00:00");
		changeTime(0);
		System.out.println("Starting to split into several non-temporal models at " + currentTime.toString());
		
		String folderName = "retrievedOriginalModels";
		IFolder folder = project.getFolder(folderName);
		if(folder.exists())
			try {
				folder.delete(true, null);
			} catch (CoreException e) {
				return "Deleting the folder " + folder.getName() + "failed: " + e.getMessage();
			}
		
		for(int timeOffset : hoursBetweenSnapshots) {
			changeTime(timeOffset);
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String fileName = format.format(this.currentTime);
			Resource nonTemporalResource = resourceSet.createResource(URI.createURI(resourcesToConvert[0].getURI().toString().substring(0, 26) + "/" + folderName + "/" + fileName + ".defeaturemodel"));
			
			nonTemporalResource.getContents().add(createNonTemporalFeatureModel(combinedFeatureModel));
			
			try {
				nonTemporalResource.save(null);
			} catch (IOException e) {
				return "Storing of an original resource (" + nonTemporalResource.getURI().toString() + ") failed: " + e.getMessage();
			}
		}
		
		System.out.println("Printing statistics...");
		
//		org.deltaecore.temporal.SimpleCounter.printStatistics();
		
		System.out.println("Finished at " + currentTime.toString());
		
		MessageDialog dialog = new MessageDialog(shell, "Conversion to temporal Model finished!", null, "DONE !!!!!!!!!!!!!!!!!!!!!!!!!1111111einseinself", MessageDialog.INFORMATION, new String [] { "So nice *.*" }, 0);
		dialog.open();
		return null;
	}
	
	
	
	private DEFeatureFactory originalFactory = new DEFeatureFactoryImpl();
	
	private DEFeatureModel createNonTemporalFeatureModel(DEFeatureModel temporalFeatureModel) {
		DEFeatureModel derivedOriginalFeaturemodel = originalFactory.createDEFeatureModel();
		
		derivedOriginalFeaturemodel.setRootFeature(createNonTemporalFeature(temporalFeatureModel.getRootFeature()));
		
		return derivedOriginalFeaturemodel;
	}
	
	private DEFeature createNonTemporalFeature(DEFeature temporalFeature) {
		DEFeature derivedOriginalFeature = originalFactory.createDEFeature();
		
		derivedOriginalFeature.setMinCardinality(temporalFeature.getMinCardinality());
		derivedOriginalFeature.setMaxCardinality(temporalFeature.getMaxCardinality());
		
		derivedOriginalFeature.setName(temporalFeature.getName());
		
		for(DEGroup temporalGroup : temporalFeature.getGroups())
			derivedOriginalFeature.getGroups().add(createNonTemporalGroup(temporalGroup));
		
		return derivedOriginalFeature;
	}
	
	private DEGroup createNonTemporalGroup(DEGroup temporalGroup) {
		DEGroup derivedOriginalGroup = originalFactory.createDEGroup();

		derivedOriginalGroup.setMinCardinality(temporalGroup.getMinCardinality());
		derivedOriginalGroup.setMaxCardinality(temporalGroup.getMaxCardinality());

		for(DEFeature temporalFeature : temporalGroup.getFeatures())
			derivedOriginalGroup.getFeatures().add(createNonTemporalFeature(temporalFeature));
		
		return derivedOriginalGroup;
	}



//	private String createTemporalBehaviorMachineResource() {
//		System.out.println("Starting at " + currentTime.toString());
//		MStateMachine combinedAdapterMachine = null;
//		
//		BehaviorAdapterElementDifferenceCalculator.instance.setComparator(new BehaviorAdapterElementDifferenceCalculator.Comparator() {
//			@Override
//			public boolean equal(Object e1, Object e2) {
//				if (e1 == null || e2 == null)
//					return false;
//				if (e1 instanceof de.imotep.core.datamodel.MNamedEntity && e1 instanceof de.imotep.core.datamodel.MNamedEntity) {
//					de.imotep.core.datamodel.MNamedEntity entity1 = (de.imotep.core.datamodel.MNamedEntity) e1;
//					de.imotep.core.datamodel.MNamedEntity entity2 = (de.imotep.core.datamodel.MNamedEntity) e2;
//					
//					if(entity1.getName().equals(entity2.getName())) {
//						return true;
//					}
//				}
//				return e1.equals(e2);
//			}
//		});
//		
//		for(Resource res : this.resourcesToConvert) {
//			if(res.getContents().size() != 1 || !(res.getContents().get(0) instanceof MStateMachine))
//				return "\"" + res.getURI() + "\" does not contain exactly one State Machine...";
//
//			if(combinedAdapterMachine == null) {
//				combinedAdapterMachine = BehaviorAdapterElementInitializer.instance.createAdapterElementFor((MStateMachine) res.getContents().get(0));
//			}
//			else {
//				// Fast forward one day...
//				changeTime(24);
//				
//				BehaviorAdapterElementDifferenceCalculator.instance.applyDifferences(combinedAdapterMachine, (MStateMachine) res.getContents().get(0));
//			}
//		}
//		
//		Resource temporalResource = resourceSet.createResource(URI.createURI(resourcesToConvert[0].getURI().toString().substring(0, resourcesToConvert[0].getURI().toString().length()-27) + "_temporal.behavior"));
//		temporalResource.getContents().add(combinedAdapterMachine);
//		
//		try {
//			temporalResource.save(null);
//		} catch (IOException e) {
//			return "Storing of the temporal resource failed: " + e.getMessage();
//		}
//		
//		System.out.println("Finished at " + currentTime.toString());
//		
//		MessageDialog dialog = new MessageDialog(shell, "Conversion to temporal Model finished!", null, "DONE !!!!!!!!!!!!!!!!!!!!!!!!!1111111einseinself", MessageDialog.INFORMATION, new String [] { "So nice *.*" }, 0);
//		dialog.open();
//		return null;
//	}



	private void changeTime(long deltaHours) {
		System.out.println("Forwarding " + deltaHours + " hours...");
		currentTime.setTime(currentTime.getTime() + deltaHours*1000*60*60);
		ClockMechanism.instance.setTime(currentTime);
	}

}

















