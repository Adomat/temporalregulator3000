package de.buttontest2.handlers;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import de.tu_bs.cs.isf.temporalregulator3000.core.ClockMechanism;
import de.tu_bs.cs.isf.temporalregulator3000.model.util.DateResolverUtil;
import eu.vicci.ecosystem.sft.SFTBasicFault;
import eu.vicci.ecosystem.sft.SFTFactory;
import eu.vicci.ecosystem.sft.SFTFault;
import eu.vicci.ecosystem.sft.SFTGate;
import eu.vicci.ecosystem.sft.SFTGateType;
import eu.vicci.ecosystem.sft.SFTIdentifiable;
import eu.vicci.ecosystem.sft.SFTIntermediateFault;
import eu.vicci.ecosystem.sft.SFTSoftwareFaultTree;
import eu.vicci.ecosystem.sft.accessor.SFTDirectWriteAccessor;
import eu.vicci.ecosystem.sft.accessor.SFTWriteAccessor;

public class SampleHandler extends AbstractHandler {

	

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Resource res = (new ResourceSetImpl()).createResource(URI.createURI("platform:/resource/SFTTest/Test.sft"));
		String uri = null;//res.getURI().toString();

		ClockMechanism.instance.setTime(uri, DateResolverUtil.resolveDate("2019-05-21T12:00:00"));
		
		SFTWriteAccessor writeAccessor = new SFTDirectWriteAccessor();

		SFTSoftwareFaultTree softwareFaultTree = SFTFactory.eINSTANCE.createSFTSoftwareFaultTree();
		res.getContents().add(softwareFaultTree);
		softwareFaultTree.setName("Fault Tree 1");
		
		SFTIntermediateFault rootFault = SFTFactory.eINSTANCE.createSFTIntermediateFault();
		writeAccessor.modifyIntermediateFault(rootFault, "RF1", "Root Fault 1", "The First Root Fault");
		writeAccessor.setRootFault(rootFault, softwareFaultTree);
		
		SFTGate gate = SFTFactory.eINSTANCE.createSFTGate();
		writeAccessor.modifyGate(gate, "G", SFTGateType.OR);
		writeAccessor.addGate(gate, rootFault);
		
		

		ClockMechanism.instance.setTime(uri, DateResolverUtil.resolveDate("2019-05-22T12:00:00"));

		SFTBasicFault basicFault1 = SFTFactory.eINSTANCE.createSFTBasicFault();
		writeAccessor.modifyBasicFault(basicFault1, "F1", "Basic Fault 1", "The First Basic Fault", 0.3);
		writeAccessor.addBasicFault(basicFault1, gate);
		
		
		
		ClockMechanism.instance.setTime(uri, DateResolverUtil.resolveDate("2019-05-23T12:00:00"));
		
		SFTBasicFault basicFault2 = SFTFactory.eINSTANCE.createSFTBasicFault();
		writeAccessor.modifyBasicFault(basicFault2, "F2", "Basic Fault 2", "The Second Basic Fault", 0.7);
		writeAccessor.addBasicFault(basicFault2, gate);
		
		
		
		ClockMechanism.instance.setTime(uri, DateResolverUtil.resolveDate("2019-05-24T12:00:00"));
		
		SFTBasicFault basicFault3 = SFTFactory.eINSTANCE.createSFTBasicFault();
		writeAccessor.modifyBasicFault(basicFault3, "F3", "Another Basic Fault (3)", "The Third Basic Fault", 0.95);
		writeAccessor.addBasicFault(basicFault3, gate);
		
		// And so on...
		
		System.out.println();
		System.out.println("Resource to be saved at [" + ClockMechanism.instance.getTime() + "]:");
		printRes(res);
		
		try {
			res.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		Resource loadedRes = (new ResourceSetImpl()).getResource(URI.createURI("platform:/resource/SFTTest/Test.sft"), true);
		try {
			loadedRes.load(Collections.emptyMap());
		} catch (IOException e) {
			throw new RuntimeException("An error occured while setting the original genmodel:\n" + e.getMessage());
		}
		
		System.out.println();
		System.out.println("Loaded Resource at [" + ClockMechanism.instance.getTime() + "]:");
		printRes(loadedRes);
		
		return null;
	}
	
	
	private void printRes(Resource res) {
		for(EObject eo : res.getContents()) {
			printEObject(eo, 1);
		}
	}
	
	private void printEObject(EObject e, int depth) {
		for(int i=0; i<depth; i++)
			System.out.print("\t");
		depth++;

		if(e == null) {
			System.out.println("null...");
			return;
		}
		
		System.out.print(e.getClass().getSimpleName() + " - ");

		if(e instanceof SFTSoftwareFaultTree) {
			System.out.print(" [name: " + ((SFTSoftwareFaultTree) e).getName() + "]");
		}
		if(e instanceof SFTIdentifiable) {
			System.out.print(" [id: " + ((SFTIdentifiable) e).getId() + "]");
		}
		if(e instanceof SFTFault) {
			System.out.print(" [name: " + ((SFTFault) e).getName() + "]");
			System.out.print(" [description: " + ((SFTFault) e).getDescription() + "]");
		}
		if(e instanceof SFTGate) {
			System.out.print(" [gate type: " + ((SFTGate) e).getGateType() + "]");
		}
		if(e instanceof SFTBasicFault) {
			System.out.print(" [gate type: " + ((SFTBasicFault) e).getProbability() + "]");
		}
		
		System.out.println();
		
		if(e instanceof SFTSoftwareFaultTree) {
			printEObject(((SFTSoftwareFaultTree) e).getRootFault(), depth);
		}
		if(e instanceof SFTIntermediateFault) {
			printEObject(((SFTIntermediateFault) e).getGate(), depth);
		}
		if(e instanceof SFTGate) {
			for(SFTFault child : ((SFTGate) e).getFaults())
				printEObject(child, depth);
		}
		
	}

}
