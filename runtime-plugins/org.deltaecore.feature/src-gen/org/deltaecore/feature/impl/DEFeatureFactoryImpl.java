/**
 */
package org.deltaecore.feature.impl;

import org.deltaecore.feature.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DEFeatureFactoryImpl extends EFactoryImpl implements DEFeatureFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DEFeatureFactory init() {
		try {
			DEFeatureFactory theDEFeatureFactory = (DEFeatureFactory)EPackage.Registry.INSTANCE.getEFactory(DEFeaturePackage.eNS_URI);
			if (theDEFeatureFactory != null) {
				return theDEFeatureFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DEFeatureFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DEFeatureFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DEFeaturePackage.DE_FEATURE_MODEL: return createDEFeatureModel();
			case DEFeaturePackage.DE_FEATURE: return createDEFeature();
			case DEFeaturePackage.DE_GROUP: return createDEGroup();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DEFeatureModel createDEFeatureModel() {
		DEFeatureModelImpl deFeatureModel = new DEFeatureModelImpl();
		return deFeatureModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DEFeature createDEFeature() {
		DEFeatureImpl deFeature = new DEFeatureImpl();
		return deFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DEGroup createDEGroup() {
		DEGroupImpl deGroup = new DEGroupImpl();
		return deGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DEFeaturePackage getDEFeaturePackage() {
		return (DEFeaturePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DEFeaturePackage getPackage() {
		return DEFeaturePackage.eINSTANCE;
	}

} //DEFeatureFactoryImpl
