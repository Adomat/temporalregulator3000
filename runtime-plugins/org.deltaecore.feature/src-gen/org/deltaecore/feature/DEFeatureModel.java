/**
 */
package org.deltaecore.feature;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.deltaecore.feature.DEFeatureModel#getRootFeature <em>Root Feature</em>}</li>
 * </ul>
 *
 * @see org.deltaecore.feature.DEFeaturePackage#getDEFeatureModel()
 * @model
 * @generated
 */
public interface DEFeatureModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Root Feature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Feature</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Feature</em>' containment reference.
	 * @see #setRootFeature(DEFeature)
	 * @see org.deltaecore.feature.DEFeaturePackage#getDEFeatureModel_RootFeature()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DEFeature getRootFeature();

	/**
	 * Sets the value of the '{@link org.deltaecore.feature.DEFeatureModel#getRootFeature <em>Root Feature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Feature</em>' containment reference.
	 * @see #getRootFeature()
	 * @generated
	 */
	void setRootFeature(DEFeature value);

} // DEFeatureModel
