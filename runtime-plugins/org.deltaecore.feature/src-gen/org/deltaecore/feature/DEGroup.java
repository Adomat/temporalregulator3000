/**
 */
package org.deltaecore.feature;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DE Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.deltaecore.feature.DEGroup#getFeatures <em>Features</em>}</li>
 *   <li>{@link org.deltaecore.feature.DEGroup#getParentOfGroup <em>Parent Of Group</em>}</li>
 * </ul>
 *
 * @see org.deltaecore.feature.DEFeaturePackage#getDEGroup()
 * @model
 * @generated
 */
public interface DEGroup extends DECardinalityBasedElement {
	/**
	 * Returns the value of the '<em><b>Features</b></em>' containment reference list.
	 * The list contents are of type {@link org.deltaecore.feature.DEFeature}.
	 * It is bidirectional and its opposite is '{@link org.deltaecore.feature.DEFeature#getParentOfFeature <em>Parent Of Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Features</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Features</em>' containment reference list.
	 * @see org.deltaecore.feature.DEFeaturePackage#getDEGroup_Features()
	 * @see org.deltaecore.feature.DEFeature#getParentOfFeature
	 * @model opposite="parentOfFeature" containment="true" required="true"
	 * @generated
	 */
	EList<DEFeature> getFeatures();

	/**
	 * Returns the value of the '<em><b>Parent Of Group</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.deltaecore.feature.DEFeature#getGroups <em>Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Of Group</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Of Group</em>' container reference.
	 * @see #setParentOfGroup(DEFeature)
	 * @see org.deltaecore.feature.DEFeaturePackage#getDEGroup_ParentOfGroup()
	 * @see org.deltaecore.feature.DEFeature#getGroups
	 * @model opposite="groups" transient="false"
	 * @generated
	 */
	DEFeature getParentOfGroup();

	/**
	 * Sets the value of the '{@link org.deltaecore.feature.DEGroup#getParentOfGroup <em>Parent Of Group</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Of Group</em>' container reference.
	 * @see #getParentOfGroup()
	 * @generated
	 */
	void setParentOfGroup(DEFeature value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return (getMinCardinality() == 1 &amp;&amp; getMaxCardinality() == 1);'"
	 * @generated
	 */
	boolean isAlternative();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='if (getMinCardinality() != 1 || getMaxCardinality() != getFeatures().size()) {\r\n\treturn false;\r\n}\r\n\r\nfor (DEFeature feature : getFeatures()) {\r\n\t//If a feature of the group is mandatory, then it is really\r\n\t//not a special group but the cardinality is coincidence.\r\n\tif (feature.isMandatory()) {\r\n\t\treturn false;\r\n\t}\r\n}\r\n\r\nreturn true;'"
	 * @generated
	 */
	boolean isOr();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='int optionalFeatures = 0;\r\nint mandatoryFeatures = 0;\r\n\t\t\r\nfor (DEFeature feature : getFeatures()) {\r\n\tif (feature.isOptional()) {\r\n\t\toptionalFeatures++;\r\n\t} else if (feature.isMandatory()) {\r\n\t\tmandatoryFeatures++;\r\n\t}\r\n}\r\n\t\t\r\nreturn (getMinCardinality() &lt;= mandatoryFeatures &amp;&amp; getMaxCardinality() &gt;= (mandatoryFeatures + optionalFeatures));'"
	 * @generated
	 */
	boolean isAnd();

} // DEGroup
